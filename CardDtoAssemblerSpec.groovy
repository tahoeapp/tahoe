package tahoe.cards

import spock.lang.Specification
import tahoe.api.assemblers.CardDtoAssembler
import tahoe.api.dto.response.CardDto
import tahoe.users.Geolocation
import tahoe.users.Ranking
import tahoe.users.User

class CardDtoAssemblerSpec extends Specification {

    CardDtoAssembler cardDtoAssembler = new CardDtoAssembler()

    def "Average ranking calculated correctly"(def rankings, int expectedAverage) {
        setup:
        def card = Mock(Card)
        def user = Mock(User)
        def location = Mock(Geolocation)

        user.getLocation() >> location
        user.getRankings() >> rankings.collect { int ranking -> new Ranking("", ranking) }
        card.getOwner() >> user

        CardDto cardDto = cardDtoAssembler.assemble(card)

        expect:
        cardDto.averageOwnerRanking == expectedAverage

        where:
        rankings  || expectedAverage
        [3, 3, 4] || 3
        [4, 4, 4] || 4
        [4, 5, 5] || 5
        []        || 0
    }

}