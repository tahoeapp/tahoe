### Welcome to Tahoe-App! ###

* To set up an environment, please follow the instructions on the wiki at [Development Environment]
* Issues and tasks are kept in this BitBucket's [Issues]



[Development Environment]: https://bitbucket.org/tahoeapp/tahoe-web/wiki/Development%20Environment
[Issues]: https://bitbucket.org/tahoeapp/tahoe-web/issues?status=new&status=open