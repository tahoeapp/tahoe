package tahoe.resetpassword;

import org.springframework.data.mongodb.repository.MongoRepository;
import tahoe.resetpassword.ResetPassword;

import java.util.List;

public interface ResetPasswordRepository extends MongoRepository<ResetPassword, String> {

    public ResetPassword findById(String id);

}
