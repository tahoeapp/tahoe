package tahoe.resetpassword;

import java.util.List;

public interface ResetPasswordService {

    public ResetPassword createNewEntry(String email);
    
    public Boolean checkResetPassword(String id);
    
    public Boolean updatePassword(String id, String password);
    
}
