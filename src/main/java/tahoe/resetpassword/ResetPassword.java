package tahoe.resetpassword;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import tahoe.users.User;
import java.util.Date;

@Document(collection = "resetpasswords")
public class ResetPassword {

    private @Id String id;
    private @DBRef User owner;
    private Date requestDate;

    public ResetPassword() {
    }

    public ResetPassword(User owner, Date requestDate) {
        this.owner = owner;
        this.requestDate = requestDate;
    }

    public String getId() {
        return id;
    }
    
    public User getOwner() {
    	return owner;
    }
    
    public Date getRequestDate() {
    	return requestDate;
    }
        
}
