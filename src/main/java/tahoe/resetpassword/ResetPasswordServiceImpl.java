package tahoe.resetpassword;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.security.crypto.password.PasswordEncoder;
import tahoe.users.User;
import tahoe.users.UserRepository;
import tahoe.resetpassword.ResetPassword;
import tahoe.resetpassword.ResetPasswordRepository;
import org.joda.time.DateTime;
import java.util.Date;

@Service
public class ResetPasswordServiceImpl implements ResetPasswordService {

    private @Autowired ResetPasswordRepository resetPasswordRepository;
    private @Autowired UserRepository userRepository;
    private @Autowired PasswordEncoder passwordEncoder;

    @Override
    public ResetPassword createNewEntry(String email) {
    	User user = userRepository.findByEmail(email);
    	if (user == null) {
    		return null;
    	}
        ResetPassword resetPW = new ResetPassword(user, new DateTime().toDate());
    	return resetPasswordRepository.save(resetPW);
    }
    
    @Override
    public Boolean checkResetPassword(String id) {
    	ResetPassword resetPw = resetPasswordRepository.findById(id);
    	if (resetPw == null) {
    		return false;
    	}
    	Date requestDate = resetPw.getRequestDate();
    	if (requestDate.before(new DateTime().minusHours(12).toDate())) {
	        resetPasswordRepository.delete(resetPw);
    		return false;
    	}
    	return true;
    }
    
    @Override
    public Boolean updatePassword(String id, String password) {
    	ResetPassword resetPw = resetPasswordRepository.findById(id);
    	if (resetPw == null) {
    		return false;
    	}
    	Date requestDate = resetPw.getRequestDate();
    	if (requestDate.before(new DateTime().minusHours(12).toDate())) {
	        resetPasswordRepository.delete(resetPw);
    		return false;
    	}
    	User user = resetPw.getOwner();
    	String encodedPassword = passwordEncoder.encode(password);
        user.setPassword(encodedPassword);
        userRepository.save(user);
        resetPasswordRepository.delete(resetPw);
        return true;
    }

}