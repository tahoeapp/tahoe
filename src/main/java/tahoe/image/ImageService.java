package tahoe.image;

public interface ImageService {

	public boolean isPortrait(byte[] imageData);
	
	public boolean isLandscape(byte[] imageData);
    
    public byte[] resizeByHeight(int height, byte[] imageData);
    
    public byte[] optimizeForWeb(byte[] imageData);
    
}
