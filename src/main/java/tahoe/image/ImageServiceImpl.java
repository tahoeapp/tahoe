package tahoe.image;

import org.springframework.stereotype.Service;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import javax.imageio.ImageIO;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.Image;
import java.awt.RenderingHints;
import javax.imageio.ImageWriter;
import javax.imageio.ImageWriteParam;
import javax.imageio.stream.ImageOutputStream;
import javax.imageio.IIOImage;
import java.util.Iterator;
import java.io.IOException;

@Service
public class ImageServiceImpl implements ImageService {

	@Override
	public boolean isPortrait(byte[] imageData) {
		try {
			InputStream in = new ByteArrayInputStream(imageData);
			BufferedImage image = ImageIO.read(in);
			//If the height is greater than or equal to width return true
			return image.getHeight() >= image.getWidth();
		}
		catch(IOException ex){
			System.out.println (ex.toString());
		}
		return true;
	}
	
	@Override
	public boolean isLandscape(byte[] imageData) {
		try {
			InputStream in = new ByteArrayInputStream(imageData);
			BufferedImage image = ImageIO.read(in);
			//If the width is greater than or equal to height return true
			return image.getWidth() >= image.getHeight();
		}
		catch(IOException ex){
			System.out.println (ex.toString());
		}
		return true;
	}
    
    @Override
    public byte[] resizeByHeight(int height, byte[] imageData) {
    	try {
			//Convert to a buffered image
			InputStream in = new ByteArrayInputStream(imageData);
			BufferedImage imageToResize = ImageIO.read(in);
			//Resize
			double scaleFactor = imageToResize.getHeight() / (double)height;
			int newW = (int)(imageToResize.getWidth() / scaleFactor);
			int newH = (int)(imageToResize.getHeight() / scaleFactor);
			BufferedImage tmp = new BufferedImage(newW, newH, BufferedImage.TYPE_INT_RGB);
            Graphics2D g2d = tmp.createGraphics();
			g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
								RenderingHints.VALUE_INTERPOLATION_BILINEAR);
			g2d.drawImage(imageToResize, 0, 0, newW, newH, null);
			//Convert back to a byte array
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(imageToResize, "jpg", baos);
			baos.flush();
			byte[] resizedImage = baos.toByteArray();
			baos.close();
			return resizedImage;
		}
		catch(IOException ex){
			System.out.println (ex.toString());
		}
		return imageData;
    }
    
    @Override
    public byte[] optimizeForWeb(byte[] imageData) {
    	try {
			//Convert to a buffered image
			InputStream in = new ByteArrayInputStream(imageData);
			BufferedImage imageToOptimize = ImageIO.read(in);
			//Reduce the quality and get the byte array
			Iterator iter = ImageIO.getImageWritersByFormatName("jpg");  
			ImageWriter writer = (ImageWriter)iter.next();  
			ImageWriteParam iwp = writer.getDefaultWriteParam();  
			iwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);  
			float quality = 0.2f;  
			iwp.setCompressionQuality(quality);  
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageOutputStream ios = ImageIO.createImageOutputStream(baos);
			writer.setOutput(ios);
			writer.write(null, new IIOImage(imageToOptimize, null, null), iwp);  
			writer.dispose();
			baos.flush();
			byte[] optimizedImage = baos.toByteArray();
			baos.close();
			return optimizedImage;
		}
		catch(IOException ex){
			System.out.println (ex.toString());
		}
		return imageData;
    }

}