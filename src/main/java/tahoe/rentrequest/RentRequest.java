package tahoe.rentrequest;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import tahoe.cards.Card;
import tahoe.users.User;

import java.util.Date;

@Document(collection = "rent_requests")
public class RentRequest {

    @Id
    private String id;

    @DBRef
    private Card requestedCard;

    @DBRef
    private User createdBy;

    private Date createDate;
    private Date rentStartDate;
    private Date rentEndDate;
    private double rentCost;
    private double tahoeFee;

    /**
     * Use this c'tor when constructed from controller
     */
    public RentRequest(Card requestedCard, Date createDate,
                       Date rentStartDate, Date rentEndDate) {
        this.requestedCard = requestedCard;
        this.createDate = createDate;
        this.rentStartDate = rentStartDate;
        this.rentEndDate = rentEndDate;
    }

    public RentRequest(Card requestedCard, Date createDate, User createdBy,
                       Date rentStartDate, Date rentEndDate, double rentCost,
                       double tahoeFee) {
        this.requestedCard = requestedCard;
        this.createDate = createDate;
        this.createdBy = createdBy;
        this.rentStartDate = rentStartDate;
        this.rentEndDate = rentEndDate;
        this.rentCost = rentCost;
        this.tahoeFee = tahoeFee;
    }

    public void setRentCost(double rentCost) {
        this.rentCost = rentCost;
    }

    public void setTahoeFee(double tahoeFee) {
        this.tahoeFee = tahoeFee;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public Card getRequestedCard() {
        return requestedCard;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public Date getRentStartDate() {
        return rentStartDate;
    }

    public Date getRentEndDate() {
        return rentEndDate;
    }

    public double getRentCost() {
        return rentCost;
    }

    public double getTahoeFee() {
        return tahoeFee;
    }
}
