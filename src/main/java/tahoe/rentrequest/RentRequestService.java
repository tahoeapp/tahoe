package tahoe.rentrequest;

public interface RentRequestService {

    public RentRequest createRentRequest(RentRequest rentRequest);

}
