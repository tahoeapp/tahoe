package tahoe.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import tahoe.api.assemblers.UserDtoAssembler;
import tahoe.api.dto.response.UserDto;
import tahoe.api.dto.request.UserRequestDto;
import tahoe.users.User;
import tahoe.users.UserService;
import tahoe.errors.ErrorCodes;
import tahoe.errors.ErrorMessages;

import javax.validation.Valid;
import java.security.Principal;

@RestController
@RequestMapping("/rest/me/user")
public class MyUserController {

    private @Autowired UserService userService;
    private @Autowired UserDtoAssembler userDtoAssembler; 

    private Logger logger = LoggerFactory.getLogger(getClass());

    @RequestMapping(method = RequestMethod.GET)
    public UserDto readMyUserInfo(Principal principal) {
        User user = userService.readUserByEmail(principal.getName());
        return userDtoAssembler.toDto(user);
    }
    
    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public UserDto updateUser(@RequestBody @Valid UserRequestDto userRequestDto,
    						  Principal principal) {
    	User updatedUser = userDtoAssembler.toUser(userRequestDto);
        
        String newUserEmailAddress = updatedUser.getEmail();
        //Check the email address is valid
        if (!userService.checkValidEmailAddress(newUserEmailAddress)) {
        	//Return invalid email error
        	return new UserDto(ErrorCodes.INVALID_EMAIL, ErrorMessages.INVALID_EMAIL);
        }
        String newUserPassword = updatedUser.getPassword();
        if (newUserPassword != null && newUserPassword.length() > 0) {
			if (!newUserPassword.matches("(?=.*\\d)(?=.*[a-z]).{6,}") || newUserPassword.length() > 20) {
				//Return invalid password error
				return new UserDto(ErrorCodes.INVALID_PASSWORD, ErrorMessages.INVALID_PASSWORD);
			}
        }
        User user = userService.updateUser(principal.getName(), updatedUser);
        return userDtoAssembler.toDto(user);
    }

}
