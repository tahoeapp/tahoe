package tahoe.api;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tahoe.api.assemblers.PaymentDtoAssembler;
import tahoe.api.dto.response.PaymentDto;
import tahoe.payments.Payment;
import tahoe.payments.PaymentService;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("rest/me/payments")
public class MyPaymentsController {

    private @Autowired PaymentService paymentService;
    private @Autowired PaymentDtoAssembler paymentDtoAssembler;

    @RequestMapping(method = RequestMethod.GET)
    public List<PaymentDto> readMyPayments(Principal loggedInUser) {
        List<Payment> payments = paymentService.readPaymentsByPayer(loggedInUser.getName());
        return Lists.transform(payments, new Function<Payment, PaymentDto>() {
            @Override
            public PaymentDto apply(Payment payment) {
                return paymentDtoAssembler.toDto(payment);
            }
        });
    }

    @RequestMapping("{id}")
    public PaymentDto readPayment(@PathVariable("id") String id, Principal loggedInUser) {
        Payment payment = paymentService.readPaymentsByPayer(id, loggedInUser.getName());
        return paymentDtoAssembler.toDto(payment);
    }


}
