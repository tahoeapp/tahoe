package tahoe.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpHeaders;
import tahoe.api.assemblers.ItemTypeDtoAssembler;
import tahoe.api.dto.response.ItemTypeDto;
import tahoe.api.dto.request.ItemTypeRequestDto;
import tahoe.itemtypes.ItemType;
import tahoe.itemtypes.Type;
import tahoe.itemtypes.ItemTypesService;

import javax.validation.Valid;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/rest/itemtypes")
public class ItemTypesController {

    private @Autowired ItemTypesService itemTypesService;
    private @Autowired ItemTypeDtoAssembler itemTypeDtoAssembler;

    @RequestMapping(method = RequestMethod.GET)
    public List<ItemTypeDto> getItemTypes() {
    	List<ItemType>  itemTypes = itemTypesService.readAllItems();
    	List<ItemTypeDto> itemTypesDto = new ArrayList<>();
    	for (ItemType itemType : itemTypes) {
    		itemTypesDto.add(itemTypeDtoAssembler.toDto(itemType));
    	}
    	return itemTypesDto;
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public String writeItemType(@RequestBody @Valid ItemTypeRequestDto itemTypeRequestDto) {
    	ItemType itemType = itemTypeDtoAssembler.toItemType(itemTypeRequestDto);
    	String item = itemType.getItem();
    	if (item == null || item.isEmpty()) {
    		return "An item must have an item name";
    	}
    	Type[] types = itemType.getTypes();
    	if (types.length < 1) {
    		return "An item type needs at least 1 type";
    	}
    	for (int i = 0; i != types.length; i++) {
    		String type = types[i].getType();
    		String typeDisplay = types[i].getTypeDisplay();
    		int sizeCount = types[i].getSizeCount();
    		if (sizeCount < 1 || sizeCount > 2) {
    			return "Type " + type + " must have a size count of 1 or 2";
    		}
    		if (type == null || type.isEmpty()) {
    			return "All types must have a type name";
    		}
    		if (typeDisplay == null || typeDisplay.isEmpty()) {
    			return "All types much have a type display name";
    		}
    	}
    	String serviceSport = itemType.getServiceSport();
    	if (itemType.getUseSport() && (serviceSport == null || serviceSport.isEmpty())) {
    		return "If the item is to use the sport then the service sport name must be provided";
    	}
    	itemTypesService.writeItem(itemType);
    	return "Added Item " + item;
    }
    
    // @RequestMapping(method = RequestMethod.PUT)
//     @ResponseStatus(value = HttpStatus.OK)
//     public void writeItemTypes() {
//     	//Windsurf boards
//     	String[] windsurfTypeSizeLabels = {};
//     	String[] windsurfTypeSizePlaceholders = {"Size (Liters)"};
//     	Type[] windsurfTypes = {new Type("Beginner", "Windsurf Board", 1, windsurfTypeSizeLabels, windsurfTypeSizePlaceholders),
//     							new Type("Intermediate", "Windsurf Board", 1, windsurfTypeSizeLabels, windsurfTypeSizePlaceholders),
//     							new Type("Advanced", "Windsurf Board", 1, windsurfTypeSizeLabels, windsurfTypeSizePlaceholders)};
//     	ItemType windsurf = new ItemType("Windsurf Board", windsurfTypes, false, "Windsurfing", true);
//     	itemTypesService.writeItem(windsurf);
//     	//Windsurf sails
//     	String[] windsurfSailTypeSizeLabels = {};
//     	String[] windsurfSailTypeSizePlaceholders = {"Size (M²)"};
//     	Type[] windsurfSailTypes = {new Type("Default", "Sail", 1, windsurfSailTypeSizeLabels, windsurfSailTypeSizePlaceholders)};
//     	ItemType windsurfSail = new ItemType("Windsurf Sail", windsurfSailTypes, true, "", false);
//     	itemTypesService.writeItem(windsurfSail);
//     	//Kite boards
//     	String[] kiteBoardTypeSizeLabelsDefault = {};
//     	String[] kiteBoardTypeSizePlaceholdersDefault = {"Size (Inches)"};
//     	String[] kiteBoardTypeSizeLabelsTwinTip = {"Width (cm):", "Height (cm):"};
//     	String[] kiteBoardTypeSizePlaceholdersTwinTip = {"Width", "Height"};
//     	Type[] kiteBoardTypes = {new Type("Twin Tip", "Twin Tip", 2, kiteBoardTypeSizeLabelsTwinTip, kiteBoardTypeSizePlaceholdersTwinTip),
//     							new Type("Strapless Surfboard", "Strapless Surfboard", 1, kiteBoardTypeSizeLabelsDefault, kiteBoardTypeSizePlaceholdersDefault),
//     							new Type("Straps Surfboard", "Straps Surfboard", 1, kiteBoardTypeSizeLabelsDefault, kiteBoardTypeSizePlaceholdersDefault),
//     							new Type("Skim Board", "Skim Board", 1, kiteBoardTypeSizeLabelsDefault, kiteBoardTypeSizePlaceholdersDefault)};
//     	ItemType kiteBoard = new ItemType("Kite Board", kiteBoardTypes, false, "Kiteboarding", true);
//     	itemTypesService.writeItem(kiteBoard);
//     	//Surfboards
//     	String[] surfoardTypeSizeLabels = {};
//     	String[] surfboardTypeSizePlaceholders = {"Size (Feet and Inches)"};
//     	Type[] surfboardTypes = {new Type("Short Board", "Short Board", 1, surfoardTypeSizeLabels, surfboardTypeSizePlaceholders),
//     							new Type("Long Board", "Long Board", 1, surfoardTypeSizeLabels, surfboardTypeSizePlaceholders),
//     							new Type("Fishtail", "Fishtail", 1, surfoardTypeSizeLabels, surfboardTypeSizePlaceholders),
//     							new Type("Hybrid", "Hybrid", 1, surfoardTypeSizeLabels, surfboardTypeSizePlaceholders)};
//     	ItemType surfboard = new ItemType("Surfboard", surfboardTypes, false, "Surfing", true);
//     	itemTypesService.writeItem(surfboard);
//     }	

}
