package tahoe.api;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.support.PagedListHolder;
import tahoe.api.assemblers.CardDtoAssembler;
import tahoe.api.dto.response.CardDto;
import tahoe.cards.Card;
import tahoe.cards.CardService;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
@RequestMapping("/rest/cards")
public class CardsController {

    private @Autowired CardService cardService;
    private @Autowired CardDtoAssembler cardDtoAssembler;
    
    private Logger logger = LoggerFactory.getLogger(getClass());
    
    @RequestMapping(value = "/nearby", method = RequestMethod.GET)
    public List<CardDto> readPagedNearbyCards(@RequestParam(defaultValue = "-121.886329") double longitude,
                                        	  @RequestParam(defaultValue = "37.338208") double latitude,
                                        	  @RequestParam(defaultValue = "0") int page) {
        PagedListHolder pagedHolder = new PagedListHolder(cardService.readNearbyCards(longitude, latitude));
        pagedHolder.setPageSize(5);
        List<CardDto> cardDtos = new ArrayList<>();
        if (page <= pagedHolder.getPageCount() - 1) {
        	pagedHolder.setPage(page);
        	List<Card> cards = pagedHolder.getPageList();
        	for (Card card : cards) {
        		try {
					cardDtos.add(cardDtoAssembler.assemble(card));
				} catch (Exception e){
					logger.info("Item with no valid user found");
				}
        	}
        }
        return cardDtos;
    }

    @RequestMapping("{id}")
    public CardDto readCard(@PathVariable("id") String id) {
        Card card = cardService.readCard(id);
        return cardDtoAssembler.assemble(card);
    }

    @RequestMapping("{id}/banner")
    public byte[] readCardBanner(@PathVariable("id") String id) {
        Card card = cardService.readCard(id);
        return card.getBanner();
    }

    @RequestMapping("{id}/backbanner")
    public byte[] readCardBackBanner(@PathVariable("id") String id) {
        Card card = cardService.readCard(id);
        return card.getBackBanner();
    }

    @RequestMapping("{id}/sidebanner")
    public byte[] readCardSideBanner(@PathVariable("id") String id) {
        Card card = cardService.readCard(id);
        return card.getSideBanner();
    }

    @RequestMapping("{id}/front")
    public byte[] readCardFrontPhoto(@PathVariable("id") String id) {
        Card card = cardService.readCard(id);
        return card.getFront();
    }

    @RequestMapping("{id}/back")
    public byte[] readCardBackPhoto(@PathVariable("id") String id) {
        Card card = cardService.readCard(id);
        return card.getBack();
    }

    @RequestMapping("{id}/side")
    public byte[] readCardSidePhoto(@PathVariable("id") String id) {
        Card card = cardService.readCard(id);
        return card.getSide();
    }

}
