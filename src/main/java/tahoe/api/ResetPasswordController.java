package tahoe.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.mail.javamail.JavaMailSender;
import javax.mail.internet.MimeMessage;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpHeaders;
import org.springframework.security.crypto.codec.Base64;
import tahoe.resetpassword.ResetPassword;
import tahoe.resetpassword.ResetPasswordService;
import tahoe.errors.ErrorCodes;
import tahoe.errors.ErrorMessages;
import javax.validation.Valid;

@RestController
@RequestMapping("/rest/resetpassword")
public class ResetPasswordController {

	private @Autowired JavaMailSender javaMailSender;
	private @Autowired ResetPasswordService resetPasswordService;

    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity<Void> resetPasswordRequest(@RequestParam String email) {
    	//Create the request for the user
		ResetPassword resetPW = resetPasswordService.createNewEntry(email);
		if (resetPW == null) {
			//there is no password to reset return error
        	return new ResponseEntity<>(new HttpHeaders(), HttpStatus.BAD_REQUEST);
		}
		//Determine the url to be added to the mail message
		String resetUrl = "http://www.tahoe-app.com/reset-password.php?c=" + resetPW.getId();
    	//Create email to the user
		try {
			MimeMessage mimeMessage = javaMailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
			String htmlMsg = "To reset your password please <a href=\"" + resetUrl + "\">go here</a>";
			mimeMessage.setContent(htmlMsg, "text/html");
			helper.setTo(email);
			helper.setSubject("Reset Password");
			helper.setFrom("no-reply@tahoe-app.com");
			javaMailSender.send(mimeMessage);
		}
		catch (Exception e) {
        	return new ResponseEntity<>(new HttpHeaders(), HttpStatus.BAD_REQUEST);
		}
        return new ResponseEntity<>(new HttpHeaders(), HttpStatus.CREATED);
    }
    
    @RequestMapping("check")
    public String  checkResetPassword(@RequestParam String id) {
    	if (resetPasswordService.checkResetPassword(id)) {
    		return "1";
    	}
    	return "0";
    }

    @RequestMapping("{id}/reset")
    public String  resetPassword(@PathVariable("id") String id,
    							@RequestParam String password) {
    	byte[] unencyptedPassword = Base64.decode(password.getBytes());
    	String newPassword = new String(unencyptedPassword);
    	if (!newPassword.matches("(?=.*\\d)(?=.*[a-z]).{6,}") || newPassword.length() > 20) {
        	//Return invalid password error
        	return "0";
        }
    	if (resetPasswordService.updatePassword(id, newPassword)) {
    		return "1";
    	}
    	return "0";
    }

}
