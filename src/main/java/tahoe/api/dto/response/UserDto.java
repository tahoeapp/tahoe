package tahoe.api.dto.response;

public class UserDto {

    private String id;
    private String firstName;
    private String lastName;
    private String email;
    private double latitude;
    private double longitude;
    private double totalRanking;
    private String payAcc;
    private long errorCode;
    private String errorMessage;

    public UserDto() {
    }

    public UserDto(String id, String firstName, String lastName, String email, double latitude, double longitude, String payAcc) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.latitude = latitude;
        this.longitude = longitude;
        this.payAcc = payAcc;
        this.totalRanking = 0;
    }
    
    public UserDto(long errorCode, String errorMessage) {
    	this.errorCode = errorCode;
    	this.errorMessage = errorMessage;
    }

    public String getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
    
    public String getEmail() {
    	return email;
    }
    
    public double getLatitude() {
    	return latitude;
    }
    
    public double getLongitude() {
    	return longitude;
    }
    
    public double getTotalRanking() {
    	return totalRanking;
    }
    
    public String getPayAcc() {
    	return payAcc;
    }
        
    public long getErrorCode() {
    	return errorCode;
    }
    
    public String getErrorMessage() {
    	return errorMessage;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    public void setEmail(String email) {
    	this.email = email;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public void setLatitude(double latitude) {
    	this.latitude = latitude;
    }
    
    public void setLongitude(double longitude) {
    	this.longitude = longitude;
    }
    
    public void setTotalRanking(double totalRanking) {
    	this.totalRanking = totalRanking;
    }
    
    public void setPayAcc(String payAcc) {
    	this.payAcc = payAcc;
    }
    
    public void setErrorCode(long errorCode) {
    	this.errorCode = errorCode;
    }
    
    public void setErrorMessage(String errorMessage) {
    	this.errorMessage = errorMessage;
    }
}
