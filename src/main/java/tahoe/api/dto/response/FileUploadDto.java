package tahoe.api.dto.response;

public class FileUploadDto {

    private String id;
    private String type;
    private long errorCode;
    private String errorMessage;

    public FileUploadDto() {
    }

    public FileUploadDto(String id, String type, long errorCode, String errorMessage) {
    	this.id = id;
    	this.type = type;
    	this.errorCode = errorCode;
    	this.errorMessage = errorMessage;
    }

	public String getId() {
		return id;
	}

    public String getType() {
        return type;
    }

    public long getErrorCode() {
    	return errorCode;
    }
    
    public String getErrorMessage() {
    	return errorMessage;
    }
    
    public String setId() {
    	return id;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    public void setErrorCode(long errorCode) {
    	this.errorCode = errorCode;
    }
    
    public void setErrorMessage(String errorMessage) {
    	this.errorMessage = errorMessage;
    }
}
