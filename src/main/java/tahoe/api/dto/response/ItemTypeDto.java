package tahoe.api.dto.response;

public class ItemTypeDto {

    private String id;
    private String item;
    private TypeDto[] types;
    private Boolean hideType;
    private String serviceSport;
    private Boolean useSport;

    public ItemTypeDto() {
    }

    public ItemTypeDto(String id, String item, TypeDto[] types, Boolean hideType, String serviceSport, Boolean useSport) {
    	this.id = id;
    	this.item = item;
    	this.types = types;
    	this.hideType = hideType;
    	this.serviceSport = serviceSport;
    	this.useSport = useSport;
    }

    public String getId() {
        return id;
    }

    public String getItem() {
        return item;
    }

    public TypeDto[] getTypes() {
    	return types;
    }
    
    public Boolean getHideType() {
    	return hideType;
    }
    
    public String getServiceSport() {
    	return serviceSport;
    }
    
    public Boolean getUseSport() {
    	return useSport;
    }
        
    public void setId(String id) {
        this.id = id;
    }

    public void setItem(String item) {
    	this.item = item;
    }
    
    public void setTypes(TypeDto[] types) {
    	this.types = types;
    }
    
    public void setHideType(Boolean hideType) {
    	this.hideType = hideType;
    }
    
    public void setServiceSport(String serviceSport) {
    	this.serviceSport = serviceSport;
    }
    
    public void setUseSport(Boolean useSport) {
    	this.useSport = useSport;
    }
}
