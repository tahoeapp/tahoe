package tahoe.api.dto.response;

import java.util.Date;

public class MessageDto {

    private UserDto sender;
    private String text;
    private Date createDate;

    public MessageDto() {
    }

    public MessageDto(UserDto sender, String text, Date createDate) {
        this.sender = sender;
        this.text = text;
        this.createDate = createDate;
    }

    public UserDto getSender() {
        return sender;
    }

    public String getText() {
        return text;
    }

    public Date getCreateDate() {
        return createDate;
    }
}
