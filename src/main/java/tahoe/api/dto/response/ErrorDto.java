package tahoe.api.dto.response;

public class ErrorDto {

    private long errorCode;
    private String errorMessage;

    public ErrorDto() {
    }

    public ErrorDto(long errorCode, String errorMessage) {
    	this.errorCode = errorCode;
    	this.errorMessage = errorMessage;
    }

	public long getErrorCode() {
    	return errorCode;
    }
    
    public String getErrorMessage() {
    	return errorMessage;
    }
    
    public void setErrorCode(long errorCode) {
    	this.errorCode = errorCode;
    }
    
    public void setErrorMessage(String errorMessage) {
    	this.errorMessage = errorMessage;
    }
}
