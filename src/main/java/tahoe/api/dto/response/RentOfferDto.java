package tahoe.api.dto.response;

import java.util.Date;

public class RentOfferDto {

    private Date rentStartDate;
    private Date rentEndDate;
    private Date createDate;
    private double rentCost;
    private double tahoeFee;

    public RentOfferDto(Date rentStartDate, Date rentEndDate, Date createDate,
                        double rentCost, double tahoeFee) {
        this.rentStartDate = rentStartDate;
        this.rentEndDate = rentEndDate;
        this.createDate = createDate;
        this.rentCost = rentCost;
        this.tahoeFee = tahoeFee;
    }

    public Date getRentStartDate() {
        return rentStartDate;
    }

    public Date getRentEndDate() {
        return rentEndDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public double getRentCost() {
        return rentCost;
    }

    public double getTahoeFee() {
        return tahoeFee;
    }
}
