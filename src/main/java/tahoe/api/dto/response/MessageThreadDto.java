package tahoe.api.dto.response;

import java.util.Date;
import java.util.List;

public class MessageThreadDto {

    private String id;
    private String title;
    private List<MessageDto> messages;

    public MessageThreadDto(String id, String title, List<MessageDto> messages) {
        this.id = id;
        this.title = title;
        this.messages = messages;
    }

    public String getTitle() {
        return title;
    }

    public List<MessageDto> getMessages() {
        return messages;
    }

    public String getId() {
        return id;
    }
}
