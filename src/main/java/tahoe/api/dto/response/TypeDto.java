package tahoe.api.dto.response;

public class TypeDto {

    private String type;
    private String typeDisplay;
    private int sizeCount;
    private SizeLabelDto[] sizeLabels;
    private SizePlaceholderDto[] sizePlaceholders;

    public TypeDto() {
    }

    public TypeDto(String type, String typeDisplay, int sizeCount, SizeLabelDto[] sizeLabels, SizePlaceholderDto[] sizePlaceholders) {
    	this.type = type;
    	this.typeDisplay = typeDisplay;
    	this.sizeCount = sizeCount;
    	this.sizeLabels = sizeLabels;
    	this.sizePlaceholders = sizePlaceholders;
    }
    
    public String getType() {
        return type;
    }
    
    public String getTypeDisplay() {
        return typeDisplay;
    }

	public int getSizeCount() {
		return sizeCount;
	}
	
	public SizeLabelDto[] getSizeLabels() {
		return sizeLabels;
	}
	
	public SizePlaceholderDto[] getSizePlaceholders() {
		return sizePlaceholders;
	}
    
    public void setType(String type) {
        this.type = type;
    }
    
    public void setTypeDisplay(String typeDisplay) {
        this.typeDisplay = typeDisplay;
    }

	public void setSizeCount(int sizeCount) {
		this.sizeCount = sizeCount;
	}
	
	public void setSizeLabels(SizeLabelDto[] sizeLabels) {
		this.sizeLabels = sizeLabels;
	}
	
	public void setSizePlaceholders(SizePlaceholderDto[] sizePlaceholders) {
		this.sizePlaceholders = sizePlaceholders;
	}    
}
