package tahoe.api.dto.response;

import tahoe.payments.PaymentMethod;

import java.util.Date;

public class PaymentDto {

    private String id;
    private String paymentDescription;
    private UserDto paidByUser;
    private UserDto paidToUser;
    private String productTitle;
    private String productDescription;
    private Date fromDate;
    private Date toDate;
    private double totalTransaction;
    private String transactionId;
    private PaymentMethod paymentMethod;

    public PaymentDto() {

    }

    public PaymentDto(String id, String paymentDescription, UserDto paidByUser,
                      UserDto paidToUser, String productTitle, String productDescription,
                      Date fromDate, Date toDate, double totalTransaction, String transactionId,
                      PaymentMethod paymentMethod) {
        this.id = id;
        this.paymentDescription = paymentDescription;
        this.paidByUser = paidByUser;
        this.paidToUser = paidToUser;
        this.productTitle = productTitle;
        this.productDescription = productDescription;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.totalTransaction = totalTransaction;
        this.transactionId = transactionId;
        this.paymentMethod = paymentMethod;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPaymentDescription() {
        return paymentDescription;
    }

    public void setPaymentDescription(String paymentDescription) {
        this.paymentDescription = paymentDescription;
    }

    public UserDto getPaidByUser() {
        return paidByUser;
    }

    public void setPaidByUser(UserDto paidByUser) {
        this.paidByUser = paidByUser;
    }

    public UserDto getPaidToUser() {
        return paidToUser;
    }

    public void setPaidToUser(UserDto paidToUser) {
        this.paidToUser = paidToUser;
    }

    public String getProductTitle() {
        return productTitle;
    }

    public void setProductTitle(String productTitle) {
        this.productTitle = productTitle;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public double getTotalTransaction() {
        return totalTransaction;
    }

    public void setTotalTransaction(double totalTransaction) {
        this.totalTransaction = totalTransaction;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
}
