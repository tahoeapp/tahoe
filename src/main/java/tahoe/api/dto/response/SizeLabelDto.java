package tahoe.api.dto.response;

public class SizeLabelDto {

    private String sizeLabel;
    
    public SizeLabelDto() {
    }

    public SizeLabelDto(String sizeLabel) {
        this.sizeLabel = sizeLabel;
    }
    
    public String getSizeLabel() {
        return sizeLabel;
    }

    public void setSizeLabel(String sizeLabel) {
        this.sizeLabel = sizeLabel;
    }
}