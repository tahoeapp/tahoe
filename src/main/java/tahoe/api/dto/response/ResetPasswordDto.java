// package tahoe.api.dto.response;
// 
// public class UserDto {
// 
//     private String id;
//     private String firstName;
//     private String lastName;
//     private long errorCode;
//     private String errorMessage;
// 
//     public UserDto() {
//     }
// 
//     public UserDto(String id, String firstName, String lastName) {
//         this.firstName = firstName;
//         this.lastName = lastName;
//     }
//     
//     public UserDto(long errorCode, String errorMessage) {
//     	this.errorCode = errorCode;
//     	this.errorMessage = errorMessage;
//     }
// 
//     public String getId() {
//         return id;
//     }
// 
//     public String getFirstName() {
//         return firstName;
//     }
// 
//     public String getLastName() {
//         return lastName;
//     }
//     
//     public long getErrorCode() {
//     	return errorCode;
//     }
//     
//     public String getErrorMessage() {
//     	return errorMessage;
//     }
// 
//     public void setFirstName(String firstName) {
//         this.firstName = firstName;
//     }
// 
//     public void setLastName(String lastName) {
//         this.lastName = lastName;
//     }
// 
//     public void setId(String id) {
//         this.id = id;
//     }
//     
//     public void setErrorCode(long errorCode) {
//     	this.errorCode = errorCode;
//     }
//     
//     public void setErrorMessage(String errorMessage) {
//     	this.errorMessage = errorMessage;
//     }
// }
