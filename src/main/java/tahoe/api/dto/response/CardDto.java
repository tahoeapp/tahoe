package tahoe.api.dto.response;

public class CardDto {

//Add location
//Do we want to add make and model

    private String ownerId;
    private double pricePerDay;
    private double pricePerWeek;
    private double authorizedAmount;
    private String item;
    private String type;
    private String makeAndModel;
    private String year;
    private String description;
    private String size;
    private String height;
    private String width;
    private String defects;
    private String sport;
    private double longitude;
    private double latitude;
    private String cardId;
    private int likeCount;
    private int viewCount;
    private Boolean swappable;
    private Boolean isFrontLandscape;

    public CardDto(String ownerId, double pricePerDay, double pricePerWeek,
                   double authorizedAmount, String item, String type, String makeAndModel, 
                   String year, String description, String size, String height, 
                   String width, String defects, String sport, double longitude, double latitude, 
                   String cardId, int likeCount, int viewCount, Boolean swappable, Boolean isFrontLandscape) {
        this.ownerId = ownerId;
        this.pricePerDay = pricePerDay;
        this.pricePerWeek = pricePerWeek;
        this.authorizedAmount = authorizedAmount;
        this.item = item;
        this.type = type;
        this.makeAndModel = makeAndModel;
        this.year = year;
        this.description = description;
        this.size = size;
        this.height = height;
        this.width = width;
        this.defects = defects;
        this.sport = sport;
        this.longitude = longitude;
        this.latitude = latitude;
        this.cardId = cardId;
        this.likeCount = likeCount;
        this.viewCount = viewCount;
        this.swappable = swappable;
        this.isFrontLandscape = isFrontLandscape;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public double getPricePerDay() {
        return pricePerDay;
    }

    public double getPricePerWeek() {
        return pricePerWeek;
    }
    
    public String getItem() {
    	return item;
    }
        
    public String getType() {
    	return type;
    }
    
    public String getMakeAndModel() {
    	return makeAndModel;
    }
    
    public String getYear() {
    	return year;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public String getCardId() {
        return cardId;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public int getViewCount() {
        return viewCount;
    }

    public String getDescription() {
        return description;
    }

    public String getDefects() {
        return defects;
    }
    
    public String getSport() {
    	return sport;
    }

    public double getAuthorizedAmount() {
        return authorizedAmount;
    }

    public String getSize() {
        return size;
    }
    
    public String getHeight() {
    	return height;
    }
    
    public String getWidth() {
    	return width;
    }
    
    public Boolean getSwappable() {
    	return swappable;
    }
    
    public Boolean getIsFrontLandscape() {
    	return isFrontLandscape;
    }
}
