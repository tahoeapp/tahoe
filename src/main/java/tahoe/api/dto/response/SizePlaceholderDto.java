package tahoe.api.dto.response;

public class SizePlaceholderDto {

    private String sizePlaceholder;
    
    public SizePlaceholderDto() {
    }

    public SizePlaceholderDto(String sizePlaceholder) {
        this.sizePlaceholder = sizePlaceholder;
    }
    
    public String getSizePlaceholder() {
        return sizePlaceholder;
    }

    public void setSizePlaceholder(String sizePlaceholder) {
        this.sizePlaceholder = sizePlaceholder;
    }
}
