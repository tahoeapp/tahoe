package tahoe.api.dto.response;

public class RateDto {

    private double feeRate;
    private double minimumFee;

    public RateDto() {
    }

    public RateDto(double feeRate, double minimumFee) {
        this.feeRate = feeRate;
        this.minimumFee = minimumFee;
    }

    public double getFeeRate() {
        return feeRate;
    }

    public double getMinimumFee() {
        return minimumFee;
    }
}
