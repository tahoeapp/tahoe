package tahoe.api.dto.request;

public class ItemTypeRequestDto {

    private String item;
    private TypeRequestDto[] types;
    private Boolean hideType;
    private String serviceSport;
    private Boolean useSport;

    public ItemTypeRequestDto() {
    }

    public ItemTypeRequestDto(String item, TypeRequestDto[] types, Boolean hideType, String serviceSport, Boolean useSport) {
    	this.item = item;
    	this.types = types;
    	this.hideType = hideType;
    	this.serviceSport = serviceSport;
    	this.useSport = useSport;
    }

    public String getItem() {
        return item;
    }

    public TypeRequestDto[] getTypes() {
    	return types;
    }
    
    public Boolean getHideType() {
    	return hideType;
    }
    
    public String getServiceSport() {
    	return serviceSport;
    }
    
    public Boolean getUseSport() {
    	return useSport;
    }
}
