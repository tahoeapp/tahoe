package tahoe.api.dto.request;

public class UserRequestDto {

    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private double latitude;
    private double longitude;
    private String signupDevice;
    private String payAcc;
    
    public UserRequestDto() {
    }

    public UserRequestDto(String firstName, String lastName, String email, String password, double latitude, double longitude, String signupDevice, String payAcc) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.latitude = latitude;
        this.longitude = longitude;
        this.signupDevice = signupDevice;
        this.payAcc = payAcc;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
    
    public double getLatitude() {
    	return latitude;
    }
    
    public double getLongitude() {
    	return longitude;
    }	
    
    public String getSignupDevice() {
    	return signupDevice;
    }
    
    public String getPayAcc() {
    	return payAcc;
    }
}
