package tahoe.api.dto.request;

public class MessageRequestDto {

    private String cardId;
    private String text;

    public MessageRequestDto() {
    }

    public MessageRequestDto(String cardId, String text) {
        this.cardId = cardId;
        this.text = text;
    }

    public String getCardId() {
        return cardId;
    }

    public String getText() {
        return text;
    }

}
