package tahoe.api.dto.request;

import java.util.Date;

public class RentOfferRequestDto {

    private Date rentStartDate;
    private Date rentEndDate;
    private String requestedCardId;

    public RentOfferRequestDto() {
    }

    public RentOfferRequestDto(Date rentStartDate, Date rentEndDate, String requestedCardId) {
        this.rentStartDate = rentStartDate;
        this.rentEndDate = rentEndDate;
        this.requestedCardId = requestedCardId;
    }

    public Date getRentStartDate() {
        return rentStartDate;
    }

    public Date getRentEndDate() {
        return rentEndDate;
    }

    public String getRequestedCardId() {
        return requestedCardId;
    }
}
