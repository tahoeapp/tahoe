package tahoe.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpHeaders;
import tahoe.api.assemblers.UserDtoAssembler;
import tahoe.api.dto.request.UserRequestDto;
import tahoe.api.dto.response.UserDto;
import tahoe.users.User;
import tahoe.users.Ranking;
import tahoe.users.UserService;
import tahoe.errors.ErrorCodes;
import tahoe.errors.ErrorMessages;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;

@RestController
@RequestMapping("/rest/users")
public class UsersController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    private @Autowired UserService userService;
    private @Autowired UserDtoAssembler userDtoAssembler;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public UserDto createUser(@RequestBody @Valid UserRequestDto userRequestDto) {
    	User newUser = userDtoAssembler.toUser(userRequestDto);
        
        String newUserEmailAddress = newUser.getEmail();
        String newUserPassword = newUser.getPassword();
        //Check the email address is valid
        if (!userService.checkValidEmailAddress(newUserEmailAddress)) {
        	//Return invalid email error
        	return new UserDto(ErrorCodes.INVALID_EMAIL, ErrorMessages.INVALID_EMAIL);
        }
        if (!newUserPassword.matches("(?=.*\\d)(?=.*[a-z]).{6,}") || newUserPassword.length() > 20) {
        	//Return invalid password error
        	return new UserDto(ErrorCodes.INVALID_PASSWORD, ErrorMessages.INVALID_PASSWORD);
        }
        //Check that the email address does not already exist in the db
        if (userService.readUserByEmail(newUserEmailAddress) != null) {
        	return new UserDto(ErrorCodes.EMAIL_ALREADY_REGISTERED, ErrorMessages.EMAIL_ALREADY_REGISTERED);
        }
        User createdUser = userService.createUser(newUser);
        return userDtoAssembler.toDto(createdUser);
    }

    @RequestMapping(value="{id}", method=RequestMethod.GET)
    public UserDto readUser(@PathVariable("id") String id) {
        User user = userService.readUserById(id);
        //Determine the total ranking, this is updated each time the user is gotten and not stored
        double totalRanking = 0;
        List<Ranking> rankings = user.getRankings();
        for (Ranking ranking : rankings) {
        	totalRanking = totalRanking + ranking.getRanking();
        }
        totalRanking = totalRanking / rankings.size();
        UserDto userDto = userDtoAssembler.toDto(user);
        userDto.setTotalRanking(totalRanking);
        return userDto;
    }

    @RequestMapping("{id}/avatar")
    public byte[] readUserAvatar(@PathVariable("id") String id) {
        User user = userService.readUserById(id);
        return user.getAvatar();
    }
    
    @RequestMapping(value="{id}/avatarupload", method=RequestMethod.POST)
    public ResponseEntity<Void> handleFileUpload(@PathVariable("id") String id,
    		@RequestParam("file") MultipartFile file) {
        try {
        	byte[] imageBytes = Base64.decode(file.getBytes());
			if (!userService.setUserAvatar(id, imageBytes)) {
				return new ResponseEntity<>(new HttpHeaders(), HttpStatus.BAD_REQUEST);
			}
			return new ResponseEntity<>(new HttpHeaders(), HttpStatus.OK);
		}
		catch (Exception e) {
			return new ResponseEntity<>(new HttpHeaders(), HttpStatus.BAD_REQUEST);
		}
    }

}
