package tahoe.api.assemblers;

import org.springframework.stereotype.Component;
import tahoe.api.dto.request.UserRequestDto;
import tahoe.api.dto.response.UserDto;
import tahoe.users.User;
import tahoe.users.Geolocation;

@Component
public class UserDtoAssembler {

    public User toUser(UserRequestDto sui) {
        User user = new User();
        user.setEmail(sui.getEmail());
        user.setPassword(sui.getPassword());
        user.setFirstName(sui.getFirstName());
        user.setLastName(sui.getLastName());
        user.setLocation(new Geolocation(sui.getLongitude(), sui.getLatitude()));
        user.setSignupDevice(sui.getSignupDevice());
        user.setPayAcc(sui.getPayAcc());
        return user;
    }

    public UserDto toDto(User user) {
        UserDto dto = new UserDto();
        dto.setId(user.getId());
        dto.setFirstName(user.getFirstName());
        dto.setLastName(user.getLastName());
        dto.setEmail(user.getEmail());
        dto.setLatitude(user.getLocation().getLatitude());
        dto.setLongitude(user.getLocation().getLongitude());
        dto.setPayAcc(user.getPayAcc());
        return dto;
    }

}
