package tahoe.api.assemblers;

import org.springframework.stereotype.Component;
import tahoe.api.dto.response.ItemTypeDto;
import tahoe.api.dto.response.TypeDto;
import tahoe.api.dto.response.SizeLabelDto;
import tahoe.api.dto.response.SizePlaceholderDto;
import tahoe.api.dto.request.ItemTypeRequestDto;
import tahoe.api.dto.request.TypeRequestDto;
import tahoe.itemtypes.ItemType;
import tahoe.itemtypes.Type;

@Component
public class ItemTypeDtoAssembler {

    public ItemTypeDto toDto(ItemType itemType) {
        ItemTypeDto dto = new ItemTypeDto();
        dto.setId(itemType.getId());
        dto.setItem(itemType.getItem());
        dto.setHideType(itemType.getHideType());
        dto.setServiceSport(itemType.getServiceSport());
        dto.setUseSport(itemType.getUseSport());
        Type[] types = itemType.getTypes();
        TypeDto[] typesDto = new TypeDto[types.length];
        for (int i = 0; i != types.length; i++) {
        	TypeDto type = new TypeDto();
        	type.setType(types[i].getType());
        	type.setTypeDisplay(types[i].getTypeDisplay());
        	type.setSizeCount(types[i].getSizeCount());
        	String[] sizeLabels = types[i].getSizeLabels();
        	SizeLabelDto[] sizeLabelsDto = new SizeLabelDto[sizeLabels.length];
        	for (int j = 0; j != sizeLabels.length; j++) {
        		SizeLabelDto sizeDto = new SizeLabelDto();
        		sizeDto.setSizeLabel(sizeLabels[j]);
        		sizeLabelsDto[j] = sizeDto;
        	}
        	type.setSizeLabels(sizeLabelsDto);
        	String[] sizePlaceholders = types[i].getSizePlaceholders();
        	SizePlaceholderDto[] sizePlaceholdersDto = new SizePlaceholderDto[sizePlaceholders.length];
        	for (int j = 0; j != sizePlaceholders.length; j++) {
        		SizePlaceholderDto sizeDto = new SizePlaceholderDto();
        		sizeDto.setSizePlaceholder(sizePlaceholders[j]);
        		sizePlaceholdersDto[j] = sizeDto;
        	}
        	type.setSizePlaceholders(sizePlaceholdersDto);
        	typesDto[i] = type;
        }
        dto.setTypes(typesDto);
        return dto;
    }
    
    public ItemType toItemType(ItemTypeRequestDto itemTypeRequest) {
    	TypeRequestDto[] typeRequestDto = itemTypeRequest.getTypes();
    	Type[] types = new Type[typeRequestDto.length];
    	for (int i = 0; i != typeRequestDto.length; i++) {
    		types[i] = new Type(typeRequestDto[i].getType(), typeRequestDto[i].getTypeDisplay(), typeRequestDto[i].getSizeCount(), typeRequestDto[i].getSizeLabels(), typeRequestDto[i].getSizePlaceholders());
    	}
    	return new ItemType(itemTypeRequest.getItem(), types, itemTypeRequest.getHideType(), itemTypeRequest.getServiceSport(), itemTypeRequest.getUseSport());
    }

}
