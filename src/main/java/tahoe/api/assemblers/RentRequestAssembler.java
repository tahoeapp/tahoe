package tahoe.api.assemblers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tahoe.api.dto.request.RentOfferRequestDto;
import tahoe.api.dto.response.RentOfferDto;
import tahoe.cards.Card;
import tahoe.cards.CardService;
import tahoe.rentrequest.RentRequest;

import java.util.Date;

@Component
public class RentRequestAssembler {

    private @Autowired CardService cardService;

    public RentOfferDto assemble(RentRequest rentRequest) {
        return null;
    }

    public RentRequest assemble(RentOfferRequestDto rentOfferRequestDto) {
        Card requestedCard = cardService.readCard(rentOfferRequestDto.getRequestedCardId());
        Date startDate = rentOfferRequestDto.getRentStartDate();
        Date endDate = rentOfferRequestDto.getRentEndDate();
        return new RentRequest(
                requestedCard,
                new Date(),
                startDate,
                endDate
        );
    }

}
