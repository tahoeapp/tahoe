package tahoe.api.assemblers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tahoe.api.dto.response.PaymentDto;
import tahoe.payments.Payment;

@Component
public class PaymentDtoAssembler {

    private @Autowired UserDtoAssembler userDtoAssembler;

    public PaymentDto toDto(Payment payment) {
        PaymentDto paymentDto = new PaymentDto();
        paymentDto.setId(payment.getId());
        paymentDto.setPaymentDescription(payment.getPaymentDescription());
        paymentDto.setPaidByUser(userDtoAssembler.toDto(payment.getPaidBy()));
        paymentDto.setPaidToUser(userDtoAssembler.toDto(payment.getPaidTo()));
        paymentDto.setProductTitle(payment.getProductTitle());
        paymentDto.setProductDescription(payment.getProductDescription());
        paymentDto.setFromDate(payment.getFromDate());
        paymentDto.setToDate(payment.getToDate());
        paymentDto.setTotalTransaction(payment.getTotalTransaction());
        paymentDto.setTransactionId(payment.getTransactionId());
        paymentDto.setPaymentMethod(payment.getPaymentMethod());
        return paymentDto;
    }

}

