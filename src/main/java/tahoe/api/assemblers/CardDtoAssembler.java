package tahoe.api.assemblers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tahoe.api.dto.request.CardRequestDto;
import tahoe.api.dto.response.CardDto;
import tahoe.cards.Card;
import tahoe.users.Ranking;
import tahoe.users.UserService;
import tahoe.users.Geolocation;
import java.util.Date;
import org.joda.time.DateTime;

import java.util.List;

@Component
public class CardDtoAssembler {

    private @Autowired UserService userService;

    public CardDto assemble(Card card) {
        return new CardDto(
                card.getOwner().getId(),
                card.getPricePerDay(),
                card.getPricePerWeek(),
                card.getAuthorizedAmount(),
                card.getItem(),
                card.getType(),
                card.getMakeAndModel(),
                card.getYear(),
                card.getDescription(),
                card.getSize(),
                card.getHeight(),
                card.getWidth(),
                card.getDefects(),
                card.getSport(),
                card.getLocation().getLongitude(),
                card.getLocation().getLatitude(),
                card.getId(),
                card.getLikeCount(),
                card.getViewCount(),
                card.getSwappable(),
                card.getIsFrontLandscape()
        );
    }

    public Card assemble(CardRequestDto cardRequest, String email) {
        return new Card(
        		cardRequest.getItem(),
        		cardRequest.getType(),
        		cardRequest.getMakeAndModel(),
        		cardRequest.getYear(),
                cardRequest.getDescription(),
                cardRequest.getSize(),
                cardRequest.getHeight(),
                cardRequest.getWidth(),
                cardRequest.getDefects(),
                cardRequest.getSport(),
                new Geolocation(cardRequest.getLongitude(), cardRequest.getLatitude()),
                cardRequest.getPricePerDay(),
                cardRequest.getPricePerWeek(),
                cardRequest.getAuthorizedAmount(),
                0,
                0,
                userService.readUserByEmail(email),
                cardRequest.getSwappable(),
                new DateTime().toDate()
        );
    }

}
