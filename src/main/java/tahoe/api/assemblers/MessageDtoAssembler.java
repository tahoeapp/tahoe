package tahoe.api.assemblers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tahoe.api.dto.request.MessageRequestDto;
import tahoe.api.dto.response.MessageDto;
import tahoe.api.dto.response.UserDto;
import tahoe.threads.Message;

@Component
public class MessageDtoAssembler {

    private @Autowired UserDtoAssembler userDtoAssembler;

    public Message toMessage(MessageRequestDto messageRequestDto, String creatingUserEmail) {
        // commented out. We will create/update threads based on cardId instead of userId.
        return null;
    }

    public MessageDto toMessageDto(Message message) {
        UserDto creatingUserDto = userDtoAssembler.toDto(message.getSender());
        return new MessageDto(creatingUserDto, message.getText(), message.getCreateDate());
    }
}
