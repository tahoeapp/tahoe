package tahoe.api.assemblers;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tahoe.api.dto.response.MessageDto;
import tahoe.api.dto.response.MessageThreadDto;
import tahoe.api.dto.response.UserDto;
import tahoe.threads.Message;
import tahoe.threads.MessageThread;

import java.util.List;

@Component
public class MessageThreadDtoAssembler {

    private @Autowired UserDtoAssembler userDtoAssembler;

    public MessageThreadDto toDto(MessageThread messageThread) {
        List<MessageDto> messageDtos = Lists.newArrayList();
        for (Message msg : messageThread.getMessages()) {
            UserDto userDto = userDtoAssembler.toDto(msg.getSender());
            MessageDto msgDto = new MessageDto(userDto, msg.getText(), msg.getCreateDate());
            messageDtos.add(msgDto);
        }

        return new MessageThreadDto(messageThread.getId(), messageThread.getTitle(), messageDtos);
    }
}
