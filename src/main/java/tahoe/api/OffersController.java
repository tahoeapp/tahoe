package tahoe.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tahoe.api.assemblers.RentRequestAssembler;
import tahoe.api.dto.request.RentOfferRequestDto;
import tahoe.api.dto.response.RentOfferDto;
import tahoe.rentrequest.RentRequest;
import tahoe.rentrequest.RentRequestService;

import javax.validation.Valid;

@RestController
@RequestMapping("/rest/offers")
public class OffersController {

    private @Autowired RentRequestService rentRequestService;
    private @Autowired RentRequestAssembler rentRequestAssembler;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public RentOfferDto createRentRequest(@RequestBody @Valid RentOfferRequestDto rentOfferRequestDto) {
        RentRequest partialRentRequest = rentRequestAssembler.assemble(rentOfferRequestDto);
        RentRequest rentRequest = rentRequestService.createRentRequest(partialRentRequest);
        return rentRequestAssembler.assemble(rentRequest);
    }


}
