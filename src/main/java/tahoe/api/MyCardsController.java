package tahoe.api;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.security.crypto.codec.Base64;
import tahoe.api.assemblers.CardDtoAssembler;
import tahoe.api.dto.request.CardRequestDto;
import tahoe.api.dto.response.CardDto;
import tahoe.api.dto.response.FileUploadDto;
import tahoe.api.dto.response.ErrorDto;
import tahoe.cards.Card;
import tahoe.cards.CardService;
import tahoe.errors.ErrorCodes;
import tahoe.errors.ErrorMessages;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/rest/me/cards")
public class MyCardsController {

    private @Autowired CardService cardService;
    private @Autowired CardDtoAssembler cardDtoAssembler;

    @RequestMapping(method = RequestMethod.GET)
    public List<CardDto> readMyCards(Principal loggedInUser) {
        List<Card> cards = cardService.readCardsByOwner(loggedInUser.getName());
        List<CardDto> result = Lists.newArrayList();
        for (Card card : cards) {
            result.add(cardDtoAssembler.assemble(card));
        }
        return result;
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public CardDto createCard(@RequestBody @Valid CardRequestDto cardRequestDto, Principal loggedInUser) {
    	Card card = cardDtoAssembler.assemble(cardRequestDto, loggedInUser.getName());
        Card newCard = cardService.createCard(card);
        return cardDtoAssembler.assemble(newCard);
    }
    
    @RequestMapping(value="{id}", method = RequestMethod.POST)
    public CardDto updateCard(@PathVariable("id") String id, 
    						  @RequestBody @Valid CardRequestDto cardRequestDto, 
    						  Principal loggedInUser) {
    	Card card = cardDtoAssembler.assemble(cardRequestDto, loggedInUser.getName());
        Card updatedCard = cardService.updateCard(id, card);
        return cardDtoAssembler.assemble(updatedCard);
    }
    
    @RequestMapping(value="{id}/remove", method = RequestMethod.POST)
    public ErrorDto updateCard(@PathVariable("id") String id, 
    						  Principal loggedInUser) {
    	//Check to see if this card is owned by the logged in user
    	Card card = cardService.readCard(id);
    	if (!card.getOwner().getEmail().equals(loggedInUser.getName())) {
        	return new ErrorDto(ErrorCodes.CARD_IS_NOT_THIS_USERS, ErrorMessages.CARD_IS_NOT_THIS_USERS);
    	}
        if (!cardService.removeCard(id)) {
        	return new ErrorDto(ErrorCodes.CARD_CAN_NOT_BE_REMOVED, ErrorMessages.CARD_CAN_NOT_BE_REMOVED);
        }
			return new ErrorDto(ErrorCodes.OP_SUCCESSFUL, ErrorMessages.OP_SUCCESSFUL);
    }

	@RequestMapping(value="{id}/{type}", method=RequestMethod.POST)
    public FileUploadDto handleFileUpload(@PathVariable("id") String id,
    		@PathVariable("type") String type,
            @RequestParam("file") MultipartFile file, 
            Principal loggedInUser) {
        try {
        	byte[] imageBytes = Base64.decode(file.getBytes());
			if (!cardService.setCardPicture(id, type, imageBytes)) {
				return new FileUploadDto(id, type, ErrorCodes.NO_CARD_FILE_TYPE_FOUND, ErrorMessages.NO_CARD_FILE_TYPE_FOUND);
			}
			return new FileUploadDto(id, type, ErrorCodes.OP_SUCCESSFUL, ErrorMessages.OP_SUCCESSFUL);
		}
		catch (Exception e) {
			return new FileUploadDto(id, type, ErrorCodes.NO_FILE_RECEIVED, ErrorMessages.NO_FILE_RECEIVED);
		}
    }

}
