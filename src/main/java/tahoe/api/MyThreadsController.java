package tahoe.api;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tahoe.api.assemblers.MessageThreadDtoAssembler;
import tahoe.api.assemblers.UserDtoAssembler;
import tahoe.api.dto.response.MessageThreadDto;
import tahoe.threads.MessageThread;
import tahoe.threads.MessageThreadService;
import tahoe.users.UserService;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("rest/me/threads")
public class MyThreadsController {

    private @Autowired UserService userService;
    private @Autowired UserDtoAssembler userDtoAssembler;
    private @Autowired MessageThreadService messageThreadService;
    private @Autowired MessageThreadDtoAssembler messageThreadDtoAssembler;

    @RequestMapping(method = RequestMethod.GET)
    public List<MessageThreadDto> readMyThreads(Principal loggedInUser) {
        List<MessageThread> messageThreads = messageThreadService.readThreadsWithUser(loggedInUser.getName());
        return Lists.transform(messageThreads, new Function<MessageThread, MessageThreadDto>() {
            @Override
            public MessageThreadDto apply(MessageThread input) {
                return messageThreadDtoAssembler.toDto(input);
            }
        });
    }

    @RequestMapping("{threadId}")
    public MessageThreadDto readThread(@PathVariable("threadId") String threadId) {
        MessageThread messageThread = messageThreadService.readThread(threadId);
        return messageThreadDtoAssembler.toDto(messageThread);
    }

}
