package tahoe.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tahoe.api.assemblers.UserDtoAssembler;
import tahoe.api.dto.response.UserDto;
import tahoe.users.User;
import tahoe.users.UserService;

import java.security.Principal;

@RestController
@RequestMapping("/rest/me/session")
public class MySessionController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    private @Autowired UserService userService;
    private @Autowired UserDtoAssembler userDtoAssembler;

    @RequestMapping(method = RequestMethod.POST)
    public UserDto startSession(Principal user) {
        logger.info("Username {} has just successfully logged in", user.getName());

        User me = userService.readUserByEmail(user.getName());
        return userDtoAssembler.toDto(me);
    }

}
