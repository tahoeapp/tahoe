package tahoe.itemtypes;

import java.util.List;

public interface ItemTypesService {

    public List<ItemType> readAllItems();
    
    public void writeItem(ItemType itemType);
}
