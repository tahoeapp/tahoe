package tahoe.itemtypes;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import tahoe.itemtypes.Type;

@Document(collection = "itemtypes")
public class ItemType {

	private @Id String id;
	private String item;
	private Type[] types;
	private Boolean hideType;
	private String serviceSport;
	private Boolean useSport;

    public ItemType() {
    }
    
    public ItemType(String item, Type[] types, Boolean hideType, String serviceSport, Boolean useSport) {
    	this.item = item;
    	this.types = types;
    	this.hideType = hideType;
    	this.serviceSport = serviceSport;
    	this.useSport = useSport;
    }

    public String getId() {
        return id;
    }
    
    public String getItem() {
    	return item;
    }
    
    public Type[] getTypes() {
    	return types;
    }
    
    public Boolean getHideType() {
    	return hideType;
    }
    
    public String getServiceSport() {
    	return serviceSport;
    }
    
    public Boolean getUseSport() {
    	return useSport;
    }
    
    public void setId(String id) {
    	this.id = id;
    }
        
}
