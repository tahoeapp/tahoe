package tahoe.itemtypes;

import org.springframework.data.annotation.PersistenceConstructor;

public class Type {
	private String type;
	private String typeDisplay;
	private int sizeCount;
	private String[] sizeLabels;
	private String[] sizePlaceholders;

    @PersistenceConstructor
    public Type(String type, String typeDisplay, int sizeCount, String[] sizeLabels, String[] sizePlaceholders) {
        this.type = type;
        this.typeDisplay = typeDisplay;
        this.sizeCount = sizeCount;
        this.sizeLabels = sizeLabels;
        this.sizePlaceholders = sizePlaceholders;
    }

    public String getType() {
        return type;
    }
    
    public String getTypeDisplay() {
    	return typeDisplay;
    }

    public int getSizeCount() {
        return sizeCount;
    }
    
    public String[] getSizeLabels() {
    	return sizeLabels;
    }
    
    public String[] getSizePlaceholders() {
    	return sizePlaceholders;
    }
}
