package tahoe.itemtypes;

import org.springframework.data.mongodb.repository.MongoRepository;
import tahoe.itemtypes.ItemType;

import java.util.List;

public interface ItemTypesRepository extends MongoRepository<ItemType, String> {

}
