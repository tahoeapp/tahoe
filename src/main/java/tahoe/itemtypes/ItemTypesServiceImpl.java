package tahoe.itemtypes;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tahoe.itemtypes.ItemType;
import tahoe.itemtypes.ItemTypesRepository;

import java.util.List;

@Service
public class ItemTypesServiceImpl implements ItemTypesService {

    private @Autowired ItemTypesRepository itemTypesRepository;
    
    @Override
    public List<ItemType> readAllItems() {
        return itemTypesRepository.findAll();
    }
    
    @Override
    public void writeItem(ItemType itemType) {
    	itemTypesRepository.save(itemType);
    }

}