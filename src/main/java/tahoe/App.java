package tahoe;

import org.springframework.boot.SpringApplication;
import tahoe.bootstrap.BootstrapConfig;
import tahoe.bootstrap.MailConfig;

public class App {

    public static void main(String[] args) {
        SpringApplication springApp = new SpringApplication(
                TahoeConfig.class,
                BootstrapConfig.class,
                MailConfig.class
        );
        springApp.run(args);
    }
}
