package tahoe.cards;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tahoe.users.User;
import tahoe.users.UserRepository;
import tahoe.image.ImageService;

import java.util.List;

@Service
public class CardServiceImpl implements CardService {

    private @Autowired CardRepository cardRepository;
    private @Autowired UserRepository userRepository;
	private @Autowired ImageService imageService;
    
    @Override
    public Card readCard(String id) {
        Card card = cardRepository.findOne(id);
        return card;
    }
    
    @Override
    public Card readCardAndIncrementViewCount(String id) {
    	Card card = cardRepository.findOne(id);
    	incrementViewCount(Lists.newArrayList(card));
    	return card;
    }

    @Override
    public Card createCard(Card card) {
        return cardRepository.save(card);
    }

    @Override
    public Card updateCard(String id, Card updatedCard) {
    	Card card = cardRepository.findOne(id);
    	if (card == null) {
    		return null;
    	}
    	updatedCard.setId(card.getId());
    	updatedCard.setLikeCount(card.getLikeCount());
    	updatedCard.setViewCount(card.getViewCount());
    	updatedCard.setFront(card.getFront());
    	updatedCard.setBanner(card.getBanner());
    	updatedCard.setBack(card.getBack());
    	updatedCard.setSide(card.getSide());
    	updatedCard.setOwner(card.getOwner());
    	updatedCard.setIsFrontLandscape(card.getIsFrontLandscape());
        return cardRepository.save(updatedCard);
    }
    
    @Override
    public Boolean removeCard(String id) {
    	Card card = cardRepository.findOne(id);
    	if (card == null) {
    		return false;
    	}
    	cardRepository.delete(card);
    	return true;
    }

    @Override
    public List<Card> readNearbyCards(double longitude, double latitude) {
        // List<User> usersNearGeolocation = userRepository.findUsersNearGeolocation(longitude, latitude, 20000);
//         List<Card> nearbyCards = Lists.newArrayList();
//         for (User user : usersNearGeolocation) {
//             List<Card> ownerCards = cardRepository.findByOwner(user);
//             nearbyCards.addAll(ownerCards);
//         }
		List<Card> nearbyCards = cardRepository.findAll();
        return nearbyCards;
    }

    @Override
    public List<Card> readCardsByOwner(String userName) {
        User user = userRepository.findByEmail(userName);
        return cardRepository.findByOwner(user);
    }
    
    @Override
    public Boolean setCardPicture(String id, String type, byte[] bytes) {
    	Card card = cardRepository.findOne(id);
		if (type.equals("front")) {
			//Create the banner for the image
			byte[] banner = imageService.resizeByHeight(150, bytes);
			card.setBanner(banner);
			card.setFront(bytes);
			card.setIsFrontLandscape(imageService.isLandscape(bytes));
		}
		else if (type.equals("back")) {
			//Create the banner for the image
			byte[] banner = imageService.resizeByHeight(150, bytes);
			card.setBackBanner(banner);
			card.setBack(bytes);
		}
		else if (type.equals("side")) {
			//Create the banner for the image
			byte[] banner = imageService.resizeByHeight(150, bytes);
			card.setSideBanner(banner);
			card.setSide(bytes);
		}
		else {
			return false;
		}
		cardRepository.save(card);
		return true;
    }

    private void incrementViewCount(List<Card> cards) {
        for (Card card : cards) {
            card.incrementViewCount();
            cardRepository.save(card);
        }
    }

}