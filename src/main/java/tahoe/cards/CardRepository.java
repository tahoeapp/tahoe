package tahoe.cards;

import org.springframework.data.mongodb.repository.MongoRepository;
import tahoe.users.User;

import java.util.List;

public interface CardRepository extends MongoRepository<Card, String> {

    public List<Card> findByOwner(User owner);

}
