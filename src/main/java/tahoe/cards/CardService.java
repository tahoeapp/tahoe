package tahoe.cards;

import java.util.List;

public interface CardService {

    public Card readCard(String id);
    
    public Card readCardAndIncrementViewCount(String id);

    public Card createCard(Card card);

    public Card updateCard(String id, Card updatedCard);
    
    public Boolean removeCard(String id);

    public List<Card> readNearbyCards(double longitude, double latitude);

    public List<Card> readCardsByOwner(String userName);
    
    public Boolean setCardPicture(String id, String type, byte[] bytes);
    
}
