package tahoe.cards;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import tahoe.users.Geolocation;
import tahoe.users.User;
import java.util.Date;

@Document(collection = "cards")
public class Card {

    private @Id String id;
    private String item;
    private String type;
    private String makeAndModel;
    private String year;
    private String description;
    private String size;
    private String height;
    private String width;
    private String defects;
    private String sport;
    private Geolocation location;
    private double pricePerDay;
    private double pricePerWeek;
    private double authorizedAmount;
    private int likeCount;
    private int viewCount;
    private Boolean swappable;
    private Date addedDate;
    private byte[] banner;
    private byte[] backBanner;
    private byte[] sideBanner;
    private byte[] front;
    private byte[] back;
    private byte[] side;
    private Boolean isFrontLandscape;

    private @DBRef User owner;

    public Card() {
    }

    public Card(String item, String type, String makeAndModel, String year, 
    			String description, String size, String height, 
    			String width, String defects, String sport, Geolocation location,
    			double pricePerDay, double pricePerWeek, double authorizedAmount, 
    			int likeCount, int viewCount, User owner, Boolean swappable, 
    			Date addedDate) {
    	this.item = item;
        this.type = type;
        this.makeAndModel = makeAndModel;
        this.year = year;
        this.description = description;
        this.size = size;
        this.height = height;
        this.width = width;
        this.defects = defects;
        this.sport = sport;
        this.location = location;
        this.pricePerDay = pricePerDay;
        this.pricePerWeek = pricePerWeek;
        this.authorizedAmount = authorizedAmount;
        this.likeCount = likeCount;
        this.viewCount = viewCount;
        this.owner = owner;
        this.swappable = swappable;
        this.addedDate = addedDate;
        this.isFrontLandscape = isFrontLandscape;
    }

    public String getId() {
        return id;
    }
    
    public String getItem() {
    	return item;
    }
    
    public String getType() {
    	return type;
    }
    
    public String getMakeAndModel() {
    	return makeAndModel;
    }
    
    public String getYear() {
    	return year;
    }

    public User getOwner() {
        return owner;
    }

    public String getDescription() {
        return description;
    }

    public String getDefects() {
        return defects;
    }
    
    public String getSport() {
    	return sport;
    }

    public Geolocation getLocation() {
        return location;
    }

    public double getPricePerDay() {
        return pricePerDay;
    }

    public double getPricePerWeek() {
        return pricePerWeek;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public int getViewCount() {
        return viewCount;
    }

    public double getAuthorizedAmount() {
        return authorizedAmount;
    }

    public String getSize() {
        return size;
    }
    
    public String getHeight() {
    	return height;
    }
    
    public String getWidth() {
    	return width;
    }

    public void incrementViewCount() {
        viewCount++;
    }
    
    public Boolean getSwappable() {
    	return swappable;
    }
    
    public Date getAddedDate() {
    	return addedDate;
    }
    
    public byte[] getBanner() {
    	return banner;
    }
    
    public byte[] getBackBanner() {
    	return backBanner;
    }
    
    public byte[] getSideBanner() {
    	return sideBanner;
    }

    public byte[] getFront() {
        return front;
    }

    public byte[] getBack() {
        return back;
    }

    public byte[] getSide() {
        return side;
    }
    
    public Boolean getIsFrontLandscape() {
    	return isFrontLandscape;
    }
    
    public void setId(String id) {
    	this.id = id;
    }
    
    public void setLikeCount(int likeCount) {
    	this.likeCount = likeCount;
    }
    
    public void setViewCount(int viewCount) {
    	this.viewCount = viewCount;
    }
    
    public void setBanner(byte[] banner) {
    	this.banner = banner;
    }
    
    public void setBackBanner(byte[] backBanner) {
    	this.backBanner = backBanner;
    }
    
    public void setSideBanner(byte[] sideBanner) {
    	this.sideBanner = sideBanner;
    }
    
    public void setFront(byte[] front) {
        this.front = front;
    }

    public void setBack(byte[] back) {
        this.back = back;
    }

    public void setSide(byte[] side) {
        this.side = side;
    }
    
    public void setOwner(User owner) {
    	this.owner = owner;
    }
    
    public void setIsFrontLandscape(Boolean isFrontLandscape) {
    	this.isFrontLandscape = isFrontLandscape;
    }
        
}
