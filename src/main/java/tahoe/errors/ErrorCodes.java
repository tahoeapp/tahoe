package tahoe.errors;

public class ErrorCodes {

	public static long OP_SUCCESSFUL = 0;
	public static long INVALID_EMAIL = 1001;
	public static long INVALID_PASSWORD = 1002;
	public static long EMAIL_ALREADY_REGISTERED = 1003;
	public static long NO_FILE_RECEIVED = 1004;
	public static long NO_CARD_FILE_TYPE_FOUND = 1005;
	public static long EMAIL_NOT_FOUND = 1006;
	public static long CARD_IS_NOT_THIS_USERS = 1007;
	public static long CARD_CAN_NOT_BE_REMOVED = 1008;
	
}
