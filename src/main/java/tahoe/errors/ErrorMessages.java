package tahoe.errors;

public class ErrorMessages {

	public static String OP_SUCCESSFUL = new String("Operation Successful");
	public static String INVALID_EMAIL = new String("Not a valid email address");
	public static String INVALID_PASSWORD = new String("Password must be between 6 and 20 and contain at least 1 character and 1 number");
	public static String EMAIL_ALREADY_REGISTERED = new String("Email address is already registered");
	public static String NO_FILE_RECEIVED = new String("No file information was received");
	public static String NO_CARD_FILE_TYPE_FOUND = new String("Unable to find file type for card");
	public static String EMAIL_NOT_FOUND = new String("Unable to find user");
	public static String CARD_IS_NOT_THIS_USERS = new String("This item does not belong to this user");
	public static String CARD_CAN_NOT_BE_REMOVED = new String("Unable to remove item");

}
