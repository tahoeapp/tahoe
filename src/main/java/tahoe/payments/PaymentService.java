package tahoe.payments;

import tahoe.cards.Card;
import tahoe.users.User;

import java.util.Date;
import java.util.List;

public interface PaymentService {

    public List<Payment> readPaymentsByPayer(String payerUsername);

    public Payment createPayment(Card card, User payingUser, Date fromDate, Date dateTo, double totalPaid,
                                 String transactionId, PaymentMethod paymentMethod);

    public Payment readPaymentsByPayer(String paymentId, String payerUsername);
}
