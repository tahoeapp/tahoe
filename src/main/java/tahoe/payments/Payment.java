package tahoe.payments;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import tahoe.users.User;

import java.util.Date;

@Document(collection = "payments")
public class Payment {

    private @Id String id;
    private @DBRef User paidBy;
    private @DBRef User paidTo;
    private String paymentDescription;
    private String productTitle;
    private String productDescription;
    private Date fromDate;
    private Date toDate;
    private double totalTransaction;
    private String transactionId;
    private PaymentMethod paymentMethod;

    public Payment() {
    }

    public Payment(User paidBy, User paidTo, String paymentDescription, String productTitle,
                   String productDescription, Date fromDate, Date toDate, double totalTransaction,
                   String transactionId, PaymentMethod paymentMethod) {
        this.paidBy = paidBy;
        this.paidTo = paidTo;
        this.paymentDescription = paymentDescription;
        this.productTitle = productTitle;
        this.productDescription = productDescription;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.totalTransaction = totalTransaction;
        this.transactionId = transactionId;
        this.paymentMethod = paymentMethod;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPaymentDescription() {
        return paymentDescription;
    }

    public void setPaymentDescription(String paymentDescription) {
        this.paymentDescription = paymentDescription;
    }

    public String getProductTitle() {
        return productTitle;
    }

    public void setProductTitle(String productTitle) {
        this.productTitle = productTitle;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public double getTotalTransaction() {
        return totalTransaction;
    }

    public void setTotalTransaction(double totalTransaction) {
        this.totalTransaction = totalTransaction;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public User getPaidBy() {
        return paidBy;
    }

    public void setPaidBy(User paidBy) {
        this.paidBy = paidBy;
    }

    public User getPaidTo() {
        return paidTo;
    }

    public void setPaidTo(User paidTo) {
        this.paidTo = paidTo;
    }
}
