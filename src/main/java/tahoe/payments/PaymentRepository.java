package tahoe.payments;

import org.springframework.data.mongodb.repository.MongoRepository;
import tahoe.users.User;

import java.util.List;

public interface PaymentRepository extends MongoRepository<Payment, String> {

    public List<Payment> findByPaidBy(User user);

}
