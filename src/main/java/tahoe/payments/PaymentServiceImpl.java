package tahoe.payments;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tahoe.cards.Card;
import tahoe.users.User;
import tahoe.users.UserService;

import java.util.Date;
import java.util.List;

@Service
public class PaymentServiceImpl implements PaymentService {

    private @Autowired PaymentRepository paymentRepository;
    private @Autowired UserService userService;

    @Override
    public List<Payment> readPaymentsByPayer(String payerUsername) {
        User user = userService.readUserByEmail(payerUsername);
        return paymentRepository.findByPaidBy(user);
    }

    @Override
    public Payment createPayment(Card card, User payingUser, Date fromDate, Date dateTo, double totalPaid,
                                 String transactionId, PaymentMethod paymentMethod) {
        Payment payment = new Payment();
        payment.setPaidBy(payingUser);
        payment.setPaidTo(card.getOwner());
        payment.setFromDate(fromDate); // TODO check if dates are already taken!!
        payment.setToDate(dateTo);
        payment.setPaymentMethod(paymentMethod);
        payment.setProductDescription(card.getDescription());
        payment.setPaymentDescription("Payment"); // TODO change to something more meaningful
        payment.setTotalTransaction(totalPaid);
        payment.setTransactionId(transactionId);
        return paymentRepository.save(payment);
    }

    @Override
    public Payment readPaymentsByPayer(String paymentId, String payerEmail) {
        Payment payment = paymentRepository.findOne(paymentId);
        if (payment.getPaidBy().getEmail().equals(payerEmail) ||
                payment.getPaidTo().getEmail().equals(payerEmail)) {
            return payment;
        }

        throw new RuntimeException("Access denied");
    }

}
