package tahoe.bootstrap;

import com.google.common.collect.Lists;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import tahoe.cards.Card;
import tahoe.cards.CardService;
import tahoe.payments.Payment;
import tahoe.payments.PaymentMethod;
import tahoe.payments.PaymentService;
import tahoe.threads.Message;
import tahoe.threads.MessageThread;
import tahoe.threads.MessageThreadService;
import tahoe.users.Geolocation;
import tahoe.users.Ranking;
import tahoe.users.User;
import tahoe.users.UserService;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

@SuppressWarnings("SpringJavaAutowiringInspection")
public class DemoCardPopulator {

    private @Autowired CardService cardService;
    private @Autowired UserService userService;
    private @Autowired MessageThreadService messageThreadService;
    private @Autowired PaymentService paymentService;

    @PostConstruct
    public void populate() throws IOException {
        User nadav = createUser("n@c.com", "Nadav", "Cohen", "snowboarder_small.png", Locations.MISSION, true);
        User eli = createUser("eli@tahoe-app.com", "Eli", "Nir", "grumpy-cat-small.png", Locations.BERNAL_HEIGHTS, true);
        User josh = createUser("josh@whitehouse.gov", "Josh", "Lyman", "josh-lyman_small.jpg", Locations.NOE_VALLEY, false);
        User donna = createUser("donna@whitehouse.gov", "Donna", "Moss", "donna_small.jpg", Locations.NEW_YORK, false);

        Card burton360 = createCard("Burton 360", "Burton 360", "10", 11, "No defects", 12d, 60d, 400d, eli, "banner1.png");
        Card burton720 = createCard("Burton 720", "Burton 720", "12", 10, "No defects", 12d, 50d, 400d, nadav, "banner2.png");
        Card rome1000 = createCard("Rome 1k", "Rome 1k", "9", 9, "No defects", 12d, 50d, 400d, donna, "banner3.png");
        Card rome4000 = createCard("Rome 4k", "Rome 1k", "9", 9, "No defects", 12d, 50d, 400d, eli, "banner3.png");
        Card rome5000 = createCard("Rome 5k", "Rome 1k", "9", 9, "No defects", 12d, 50d, 400d, eli, "banner3.png");
        Card rome6000 = createCard("Rome 6k", "Rome 1k", "9", 9, "No defects", 12d, 50d, 400d, eli, "banner3.png");
        Card rome7000 = createCard("Rome 7k", "Rome 1k", "9", 9, "No defects", 12d, 50d, 400d, eli, "banner3.png");
        Card rome8000 = createCard("Rome 8k", "Rome 1k", "9", 9, "No defects", 12d, 50d, 400d, eli, "banner3.png");
        Card rome9000 = createCard("Rome 9k", "Rome 1k", "9", 9, "No defects", 12d, 50d, 400d, eli, "banner3.png");
        Card rome10000 = createCard("Rome 10k", "Rome 1k", "9", 9, "No defects", 12d, 50d, 400d, eli, "banner3.png");
        Card rome11000 = createCard("Rome 11k", "Rome 1k", "9", 9, "No defects", 12d, 50d, 400d, eli, "banner3.png");
        Card rome12000 = createCard("Rome 12k", "Rome 1k", "9", 9, "No defects", 12d, 50d, 400d, eli, "banner3.png");
        Card rome13000 = createCard("Rome 13k", "Rome 1k", "9", 9, "No defects", 12d, 50d, 400d, eli, "banner3.png");
        Card rome14000 = createCard("Rome 14k", "Rome 1k", "9", 9, "No defects", 12d, 50d, 400d, eli, "banner3.png");
        Card rome15000 = createCard("Rome 15k", "Rome 1k", "9", 9, "No defects", 12d, 50d, 400d, eli, "banner3.png");
        Card rome16000 = createCard("Rome 16k", "Rome 1k", "9", 9, "No defects", 12d, 50d, 400d, eli, "banner3.png");
        Card rome17000 = createCard("Rome 17k", "Rome 1k", "9", 9, "No defects", 12d, 50d, 400d, eli, "banner3.png");
        Card rome18000 = createCard("Rome 18k", "Rome 1k", "9", 9, "No defects", 12d, 50d, 400d, eli, "banner3.png");

        createThread(nadav, eli, "Burton 360");
        createThread(eli, donna, "Rome 1k");
        createThread(eli, nadav, "Burton 720");

        createPayment(burton360, new DateTime().minusDays(5).toDate(), 3, 36, nadav);
        createPayment(burton720, new DateTime().minusDays(7).toDate(), 5, 40, donna);
        createPayment(rome1000, new DateTime().minusDays(10).toDate(), 2, 10, josh);
    }

    private User createUser(String email, String firstName, String lastName, String imgFileName, Geolocation location,
                            Boolean isAdmin) throws IOException {
        Resource photoResource = new ClassPathResource("bootstrap/" + imgFileName);
        byte[] photo = IOUtils.toByteArray(photoResource.getInputStream());
        User user = new User(email, "pass", firstName, lastName, photo, location, new ArrayList<Ranking>(), new DateTime().toDate(), "t", "", isAdmin);
        return userService.createUser(user);
    }

    private Card createCard(String title, String desc, String size, double bootSize, String defects, double pricePerDay,
                            double pricePerWeek, double authorizedAmount, User owner, String bannerImgFileName) throws IOException {
        Resource photoResource = new ClassPathResource("bootstrap/" + bannerImgFileName);
        byte[] photo = IOUtils.toByteArray(photoResource.getInputStream());
        Card card = new Card("Snowboard","Snowboard", "", "2014", desc, size, "", "", defects, "", owner.getLocation(), pricePerDay, pricePerWeek, authorizedAmount, 0, 0, owner, false, new DateTime().toDate());
        card.setFront(photo);
        return cardService.createCard(card);
    }
    
    private MessageThread createThread(User user1, User user2, String title) {
        Random rand = new Random();

        User[] users = {user1, user2};
        String[] msgPool = {
                "Hello",
                "Hola!",
                "I was wondering if I could ask you a few questions about your board",
                "How about you tell me a little about your board?",
                "Would you like to go snowboard sometime?"
        };
        List<Message> messages = Lists.newArrayList();
        int numOfMessages = 2 + rand.nextInt(5);
        DateTime startDate = new DateTime().minusHours(10);

        for (int i = 0; i < numOfMessages; i++) {
            Message msg = new Message(users[i % 2], msgPool[rand.nextInt(msgPool.length)], startDate.plusHours(1).toDate());
            messages.add(msg);
            startDate = startDate.plusHours(1);
        }

        MessageThread thread = new MessageThread(title, messages);
        return messageThreadService.createThread(thread);
    }

    private Payment createPayment(Card card, Date fromDate, int numOfDays, double totalTrans, User fromUser) {
        Date toDate = new DateTime(fromDate).plusDays(numOfDays).toDate();
        String transId = RandomStringUtils.randomAlphabetic(10);
        return paymentService.createPayment(card, fromUser, fromDate, toDate, totalTrans, transId, PaymentMethod.credit_card);
    }

}
