package tahoe.bootstrap;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("demo")
public class DemoConfig {

    @Bean
    public DemoCardPopulator demoCardPopulator() {
        return new DemoCardPopulator();
    }

}
