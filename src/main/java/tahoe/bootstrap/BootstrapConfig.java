package tahoe.bootstrap;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("bootstrap")
public class BootstrapConfig {

    @Bean
    public DatabaseInitializer databaseInitializer() {
        return new DatabaseInitializer();
    }

}