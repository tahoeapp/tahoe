package tahoe.bootstrap;

import tahoe.users.Geolocation;

public class Locations {

    public static Geolocation MISSION = new Geolocation(-122.4200, 37.7600);

    public static Geolocation NOE_VALLEY = new Geolocation(-122.4319, 37.7514);

    public static Geolocation BERNAL_HEIGHTS = new Geolocation(-122.4316, 37.7186);

    public static Geolocation NEW_YORK = new Geolocation(-75.1890, 42.3482);

}
