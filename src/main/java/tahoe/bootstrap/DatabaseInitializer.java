package tahoe.bootstrap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexType;
import org.springframework.data.mongodb.core.index.GeospatialIndex;

import javax.annotation.PostConstruct;

@SuppressWarnings("SpringJavaAutowiringInspection")
public class DatabaseInitializer {

    private @Autowired MongoTemplate mongoTemplate;

    @PostConstruct
    public void init() {
        // add 2dsphere index to location field in user to allow approximate search
        GeospatialIndex index = new GeospatialIndex("location").typed(GeoSpatialIndexType.GEO_2DSPHERE);
        mongoTemplate.indexOps("users").ensureIndex(index);
    }

}
