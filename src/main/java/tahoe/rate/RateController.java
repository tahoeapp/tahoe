package tahoe.version;

import tahoe.api.dto.response.RateDto;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/rates")
public class RateController {

    @RequestMapping(method = RequestMethod.GET)
    public RateDto rates() {
        RateDto rate = new RateDto(10.0, 0.0);
        return rate;
    }
}
