package tahoe.version;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/version")
public class VersionController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @RequestMapping("/ping")
    @ResponseStatus(value = HttpStatus.OK)
    public void ping() {
        logger.info("Version requested.");
    }
}
