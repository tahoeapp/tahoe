package tahoe.threads;

import org.springframework.data.mongodb.core.mapping.DBRef;
import tahoe.users.User;

import java.util.Date;

public class Message {

    private @DBRef User sender;
    private String text;
    private Date createDate;


    public Message(User sender, String text, Date createDate) {
        this.sender = sender;
        this.text = text;
        this.createDate = createDate;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}
