package tahoe.threads;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface MessageThreadRepository extends MongoRepository<MessageThread, String>, MessageThreadRepositoryCustom {

}
