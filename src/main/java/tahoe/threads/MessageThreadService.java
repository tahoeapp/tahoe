package tahoe.threads;

import java.util.List;

public interface MessageThreadService {

    public MessageThread createThread(MessageThread messageThread);

    public List<MessageThread> readThreadsWithUser(String email);

    public MessageThread readThread(String threadId);
}
