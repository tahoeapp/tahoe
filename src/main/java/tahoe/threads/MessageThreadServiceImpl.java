package tahoe.threads;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tahoe.users.User;
import tahoe.users.UserService;

import java.util.List;

@Service
public class MessageThreadServiceImpl implements MessageThreadService {

    private @Autowired MessageThreadRepository messageThreadRepository;
    private @Autowired UserService userService;

    @Override
    public MessageThread createThread(MessageThread messageThread) {
        return messageThreadRepository.save(messageThread);
    }

    @Override
    public List<MessageThread> readThreadsWithUser(String email) {
        User user = userService.readUserByEmail(email);
        return messageThreadRepository.findMessagesWithUserId(user.getId());
    }

    @Override
    public MessageThread readThread(String threadId) {
        return messageThreadRepository.findOne(threadId);
    }

}
