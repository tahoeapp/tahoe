package tahoe.threads;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "threads")
public class MessageThread {

    private @Id String id;
    private String title;
    private List<Message> messages;

    public MessageThread(String title, List<Message> messages) {
        this.title = title;
        this.messages = messages;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }
}
