package tahoe.threads;

import java.util.List;

public interface MessageThreadRepositoryCustom {

    public List<MessageThread> findMessagesWithUserId(String userId);

}
