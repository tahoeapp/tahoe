package tahoe.threads;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MessageThreadRepositoryImpl implements MessageThreadRepositoryCustom {

    private @Autowired MongoTemplate mongoTemplate;

    @Override
    public List<MessageThread> findMessagesWithUserId(String userId) {
        Query query = new Query(Criteria.where("messages.sender.$id").is(new ObjectId(userId)));
        return mongoTemplate.find(query, MessageThread.class);
    }
}
