package tahoe.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import tahoe.image.ImageService;
import org.joda.time.DateTime;
import java.util.Date;

@Service
public class UserServiceImpl implements UserService {

    private @Autowired UserRepository userRepository;
    private @Autowired PasswordEncoder passwordEncoder;
    private @Autowired ImageService imageService;

    @Override
    public User createUser(User user) {
        String encodedPassword = passwordEncoder.encode(user.getPassword());
        user.setPassword(encodedPassword);
        user.setSignupDate(new DateTime().toDate());
        return userRepository.save(user);
    }
    
    @Override
    public User updateUser(String originalEmail, User updatedUser) {
    	User user = userRepository.findByEmail(originalEmail);
    	if (user != null) {
			user.setEmail(updatedUser.getEmail());
			user.setFirstName(updatedUser.getFirstName());
			user.setLastName(updatedUser.getLastName());
			user.setLocation(updatedUser.getLocation());
			String newPassword = updatedUser.getPassword();
			if (newPassword != null && newPassword.length() > 0) {
				String encodedPassword = passwordEncoder.encode(newPassword);
       	 		user.setPassword(encodedPassword);
			}
			user = userRepository.save(user);
    	}
    	return user;
    }

    @Override
    public User readUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public User readUserById(String userId) {
        return userRepository.findOne(userId);
    }
    
    @Override
    public Boolean checkValidEmailAddress(String email) {
    	return email.matches("([a-zA-z0-9_-]+)(\\.[a-zA-Z0-9_-]+)*@([a-zA-Z0-9_-]+\\.+)+([A-Za-z]+)");
    }
    
    @Override
    public Boolean setUserAvatar(String id, byte[] bytes) {
    	User user = userRepository.findOne(id);
    	if (user == null) {
    		return false;
    	}
    	user.setAvatar(bytes);
    	userRepository.save(user);
		return true;
    }

}
