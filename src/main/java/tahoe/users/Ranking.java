package tahoe.users;

public class Ranking {

    private String note;
    private int ranking;

    public Ranking(String note, int ranking) {
        this.note = note;
        this.ranking = ranking;
    }

    public String getNote() {
        return note;
    }

    public int getRanking() {
        return ranking;
    }
}
