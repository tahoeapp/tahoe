package tahoe.users;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface UserRepository extends MongoRepository<User, String> {

    @Query("{ 'location': {" +
            "     $near: {" +
            "       $geometry: { type: 'Point', coordinates: [?0, ?1] }," +
            "       $maxDistance: ?2" +
            "     }" +
            "} }")
    public List<User> findUsersNearGeolocation(double longitude, double latitude, double distanceInMeters);

    public User findByEmail(String email);

}
