package tahoe.users;

public interface UserService {

    public User createUser(User user);
    
    public User updateUser(String originalEmail, User updatedUser);

    public User readUserByEmail(String email);

    public User readUserById(String userId);
    
    public Boolean checkValidEmailAddress(String email);
    
    public Boolean setUserAvatar(String id, byte[] bytes);

}
