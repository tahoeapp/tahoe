package tahoe.users;

import com.google.common.collect.Lists;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import tahoe.bootstrap.Locations;
import java.util.Date;

import java.util.List;

@Document(collection = "users")
public class User {

    private @Id String id;
    private @Indexed(unique = true) String email;
    private String password;
    private String firstName;
    private String lastName;
    private byte[] avatar;
    private Geolocation location = Locations.MISSION; // default to Mission!
    private List<Ranking> rankings = Lists.newArrayList();
    private Date signupDate;
    private String signupDevice;
    private String payAcc;
    private boolean isAdmin = false;

    public User() {
    }

    public User(String email, String password, String firstName, String lastName,
                byte[] avatar, Geolocation location, List<Ranking> rankings, Date signupDate, String signupDevice, String payAcc, boolean isAdmin) {
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.avatar = avatar;
        this.location = location;
        this.rankings = rankings;
        this.signupDate = signupDate;
        this.signupDevice = signupDevice;
        this.payAcc = payAcc;
        this.isAdmin = isAdmin;
    }

    public String getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public byte[] getAvatar() {
        return avatar;
    }

    public String getEmail() {
        return email;
    }

    public Geolocation getLocation() {
        return location;
    }

    public List<Ranking> getRankings() {
        return rankings;
    }
    
    public Date getSignupDate() {
    	return signupDate;
    }
    
    public String getSignupDevice() {
    	return signupDevice;
    }

    public String getPassword() {
        return password;
    }
    
    public String getPayAcc() {
		return payAcc;
	}
    
    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAvatar(byte[] photo) {
        this.avatar = photo;
    }

    public void setLocation(Geolocation location) {
        this.location = location;
    }
    
    public void setSignupDate(Date signupDate) {
    	this.signupDate = signupDate;
    }
    
    public void setSignupDevice(String signupDevice) {
    	this.signupDevice = signupDevice;
    }
    
    public void setPayAcc(String payAcc) {
    	this.payAcc = payAcc;
    }
}
