package tahoe.users;

import org.springframework.data.annotation.PersistenceConstructor;

public class Geolocation {
    private String type = "Point";

    // [0] is longitude. Ranges from -180 (w) to 180 (e)
    // [1] is latitude. Ranges from 90 (n) to -90 (s)
    private double[] coordinates = {};

    @PersistenceConstructor
    public Geolocation(String type, double[] coordinates) {
        this.type = type;
        this.coordinates = coordinates;
    }

    public Geolocation(double longitude, double latitude) {
        this.coordinates = new double[] { longitude, latitude };
    }

    public double getLongitude() {
        return coordinates[0];
    }

    public double getLatitude() {
        return coordinates[1];
    }
}
