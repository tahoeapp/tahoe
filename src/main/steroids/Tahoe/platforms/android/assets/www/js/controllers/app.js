'use-strict';

angular.module('tahoeApp', ['ionic', 'google.places', 'angularMoment', 'tahoeApp.controllers'])

    .run(function ($ionicPlatform, $ionicPopup) {
        $ionicPlatform.ready(function () {
            if (window.StatusBar) {
                StatusBar.styleDefault();
            }
            //Check for network
            if(window.Connection) {
                if(navigator.connection.type == Connection.NONE) {
                    var alertPopup = $ionicPopup.alert({
						template: 'No internet connection, please connect to your local Wifi.'
					});
					alertPopup.then();
                }
            }
            
            PayPalApp.initPaymentUI();
        });
    })

    // uncomment for production use
    //.constant('baseUrl', 'http://apps.tahoe-app.com:8080/rest')
    .constant('baseUrl', 'http://10.0.0.8:8080/rest')

    // uncomment for device deployments
    .constant('appHomeUrl', 'http://localhost/index.html')

    .config(function (RestangularProvider, baseUrl) {
        RestangularProvider.setDefaultHeaders({ "Content-Type": "application/json" });
        RestangularProvider.setBaseUrl(baseUrl);
	})
	
	// Restangular service that uses Bing
	// .factory('PaypalRestangular', function(Restangular) {
// 		return Restangular.withConfig(function(RestangularConfigurer) {
// 			RestangularConfigurer.setDefaultHeaders({ "Content-Type": "application/x-www-form-urlencoded" });
// 			RestangularConfigurer.setBaseUrl('https://api.sandbox.paypal.com/v1');
// 		});
// 	});

