﻿// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register controller
    angular.module('tahoeApp.controllers').controller("HomeCtrl", HomeCtrl);

    // 2. Inject dependencies
    HomeCtrl.inject = [];

    // 3. Define public interface and private implementation
    function HomeCtrl($scope, cardService, baseUrl) {

        // #region 3.1 Public Interface
        
        // Hide menu icon in the header
        $scope.$root.hideMenuIcon = false;
        
        $scope.buildOwnerPictureUrl = buildOwnerPictureUrl;

        $scope.buildBannerUrl = buildBannerUrl;

        // #endregion

        // #region  3.2. Private implementations
		$scope.cards = [];
		var end = false;
		var currentPage = -1;

		$scope.fetchMore = function() {
			if (end) return;

			currentPage++;		  
			cardService.readNearbyCards(currentPage, function (result) {
				if (result.length)
					$scope.cards = $scope.cards.concat(result);
				else
					end = true;
				$scope.$broadcast("scroll.infiniteScrollComplete");
			});
		};

        function buildOwnerPictureUrl(card) {
            return baseUrl + '/users/' + card.ownerId + '/avatar';
        };

        function buildBannerUrl(card) {
            return baseUrl + '/cards/' + card.cardId + '/banner';
        };
        // #endregion
    }
}());