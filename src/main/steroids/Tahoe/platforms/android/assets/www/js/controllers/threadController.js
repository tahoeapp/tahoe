﻿// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register controller
    angular.module('tahoeApp.controllers').controller("ThreadCtrl", ThreadCtrl);

    // 2. Inject dependencies
    ThreadCtrl.inject = [];

    // 3. Define public interface and private implementation
    function ThreadCtrl($scope, $stateParams, threadService, localStorageService, baseUrl) {

        // #region 3.1 Public Interface
        $scope.buildSenderPictureUrl = buildSenderPictureUrl;
        $scope.isMe = isMe;

        // #endregion

        // #region  3.2. Private implementations
        threadService.readMyThread($stateParams.id, function (result) {
            $scope.thread = result;
        });

        function isMe(sender) {
            var me = localStorageService.readLoginInfo().id;
            return sender.id === me;
        };

        function buildSenderPictureUrl(sender) {
            return baseUrl + '/users/' + sender.id + '/avatar';
        };
        // #endregion
    }
}());