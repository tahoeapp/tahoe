﻿// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register controller
    angular.module('tahoeApp.controllers').controller("CardDetailsCtrl", CardDetailsCtrl);

    // 2. Inject dependencies
    CardDetailsCtrl.inject = [];

    // 3. Define public interface and private implementation
    function CardDetailsCtrl($scope, $state, $stateParams, cardService, $ionicSlideBoxDelegate) {

        // #region 3.1 Public Interface

        $scope.$root.hideMenuIcon = true;

        // #endregion

        //cardService.readCard(0, function (result) {
        //    $scope.card = result;
        //});

        // #region  3.2. Private implementations
        // TODO comment out for fake data
        cardService.readCard($stateParams.cardId, function (result) {
            $scope.card = result;
        });
        // #endregion

        // TODO: Load card image
        var photos = [
            { slideThumbnail: "../../images/product1_1.jpg" },
            { slideThumbnail: "../../images/product1.jpg" },
            { slideThumbnail: "../../images/product1_1.jpg" }
        ];

        $scope.cardsSlide = photos;

        $scope.slideHasChanged = function (index) {
            cardService.readCard(index, function (result) {
                $scope.card = result;
            });
        };

        $scope.previousSlide = function (index)
        {
            cardService.readCard(index - 1, function (result) {
                $scope.card = result;
            });
        }

        $scope.nextSlide = function (index) {
            cardService.readCard(index + 1, function (result) {
                $scope.card = result;
            });
        }
        
        $scope.rentClicked = function() {
        	$state.go('app.renting');
        }

  $scope.nextSlide = function() {
    $ionicSlideBoxDelegate.next();
  }
  $scope.previousSlide = function() {
    $ionicSlideBoxDelegate.previous();
  }
    }
}());