﻿// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register service
    angular.module('tahoeApp.services').factory("paymentHistoryService", paymentHistoryService);

    // 2. Inject dependencies
    paymentHistoryService.inject = [];

    // 3. Define public interface and private implementation
    function paymentHistoryService(Restangular, localStorageService, basicAuthService) {

        // #region 3.1 Public Interface
        var that = this;
        // #endregion

        // #endregion

        // #region  3.2. Private implementations
        var createAuth = function () {
            var loginInfo = localStorageService.readLoginInfo();
            return basicAuthService.createHeader(loginInfo);
        };

        var getPaymentHistoryList = function (callback) {
            Restangular.all("me/payments").getList({}, createAuth()).then(function (result) {
                if (typeof (callback) === 'function') {
                    callback(null, result);
                }
            });
        };

        var getPaymentHistoryById = function (id, callback) {
            Restangular.one("me/payments", id).get({}, createAuth()).then(function (result) {
                if (typeof (callback) === 'function') {
                    callback(null, result);
                }
            });
        };


        // #region 3.2.1. Return service object

        var service = {
            findPaymentHistoryById: getPaymentHistoryById,
            getPaymentHistoryList: getPaymentHistoryList
        };

        return service;
        // #endregion
        // #endregion
    }
}());