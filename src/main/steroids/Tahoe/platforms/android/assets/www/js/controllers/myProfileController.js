// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register controller
    angular.module('tahoeApp.controllers').controller("MyProfileCtrl", MyProfileCtrl);

    // 2. Inject dependencies
    MyProfileCtrl.inject = [];

    // 3. Define public interface and private implementation
    function MyProfileCtrl($scope, $state, localStorageService, myProfileService, locationService, appHomeUrl, baseUrl) {

        angular.element('.col').on('click', function() {hprlnk(angular.element(this).attr('id'));});
        		
        // #region 3.1 Public Interface
        $scope.$root.hideMenuIcon = false;
        $scope.$root.hideBackIcon = true;
        
        $scope.buildProfilePictureUrl = buildProfilePictureUrl;
        $scope.buildOwnerPictureUrl = buildOwnerPictureUrl;
        $scope.buildBannerUrl = buildBannerUrl;
        $scope.rotateBanner = rotateBanner;
        $scope.logOut = logOut;
        $scope.hprlnk = hprlnk;
        
        $scope.noCards = true;
        
        $scope.itemTypes = localStorageService.readItemTypes();
        // #endregion

        // #region  3.2. Private implementations
        myProfileService.readMyCards(function (result) {
            $scope.cards = result;
            $scope.noCards = result.length == 0;
            
            if (!$scope.noCards) {
            	for (var i = 0; i < $scope.cards.length; i++) {
            	//Find the item and type information
            	var sizeCount = 1;
					var found = false;
					var typeDisplay = $scope.cards[i].type;
					for (var j = 0; j < $scope.itemTypes.length && !found; j++) {
						if ($scope.itemTypes[j].item == $scope.cards[i].item) {
							//If this item has no type then use the default type
							if ($scope.itemTypes[j].hideType == "true") {
								typeDisplay = $scope.itemTypes[j].types[0].typeDisplay;
								sizeCount = $scope.itemTypes[j].types[0].sizeCount;
							}
							else {
								for (var k = 0; k < $scope.itemTypes[j].types.length && !found; k++) {
									if ($scope.itemTypes[j].types[k].type == $scope.cards[i].type) {
										typeDisplay = $scope.itemTypes[j].types[k].typeDisplay;
										sizeCount = $scope.itemTypes[j].types[k].sizeCount;
										found = true;
									}
								}
							}
							found = true;
						}
					}
            		if ($scope.cards[i].item == "Service") {
            			$scope.cards[i].typeDisplay = $scope.cards[i].sport + " " + typeDisplay;
            			//For a service use the size display to display the first 3 words of the description
            			var words = $scope.cards[i].description.split(" ");
            			if (words.length <= 3) {
            				$scope.cards[i].sizeDisplay = $scope.cards[i].description;
            			}
            			else {
            				var description = "";
            				for (var j = 0; j < 3; j++) {
            					description = description.concat(words[j]) + " ";
            				}
            				$scope.cards[i].sizeDisplay = description + "...";
            			}
            			//Display the number of hours in the price
            			$scope.cards[i].priceDisplay = "$ " + $scope.cards[i].pricePerDay + " | " + $scope.cards[i].size + " hours";
            		}
            		else {
            			$scope.cards[i].typeDisplay = typeDisplay;
            			$scope.cards[i].sizeDisplay = "Size: " + $scope.cards[i].size;
            			if (sizeCount == 2) {
            				$scope.cards[i].sizeDisplay = "Size: " + $scope.cards[i].width + " x " + $scope.cards[i].height;
            			}
            			$scope.cards[i].yearDisplay = $scope.cards[i].year;
            			$scope.cards[i].priceDisplay = "$ " + $scope.cards[i].pricePerDay + "/day | $ " + $scope.cards[i].pricePerWeek + "/week";
					}
            	}
            }
        });

        myProfileService.readMyInfo(function (result) {
            $scope.myInfo = result;
            locationService.getCityByLatLong(result.latitude, result.longitude, function(result) {
            	if (location != "") {
            		$scope.myInfo.location = result;
            	}
            });
        });

		function hprlnk(lnk) {
			$state.go('app.'+lnk);
		}

		function buildProfilePictureUrl(id) {
			if (id) {
            	return baseUrl + '/users/' + id + '/avatar';
            }
        };
        
        function buildOwnerPictureUrl(card) {
        	if (card.ownerId) {
            	return baseUrl + '/users/' + card.ownerId + '/avatar';
            }
        };

        function buildBannerUrl(card) {
        	if (card.cardId) {
            	return baseUrl + '/cards/' + card.cardId + '/front';
            }
        };
        
        function rotateBanner(card) {
        	var prfImg = angular.element(".prof-img");
        	if (card.isFrontLandscape) {
        		prfImg.attr("class","profile-banner");
        	}
        	else {
        		prfImg.attr("class","profile-banner-rotated");
        	}
        };

        function logOut() {
            localStorageService.clearLocalStorage();
            document.location.replace(appHomeUrl);
        }
        // #endregion
    }
}());