﻿// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register controller
    angular.module('tahoeApp.controllers').controller("paymentHistoryDetailsCtrl", paymentHistoryDetailsCtrl);

    // 2. Inject dependencies
    paymentHistoryDetailsCtrl.inject = [];

    // 3. Define public interface and private implementation
    function paymentHistoryDetailsCtrl($scope, $stateParams, paymentHistoryService) {
        // #region 3.1 Public Interface
        $scope.payment = null;
        // #endregion

        // #region  3.2. Private implementations
        paymentHistoryService.findPaymentHistoryById($stateParams.id, function (err, data) {
            if (!err) {
                $scope.payment = data;
            }
        });
        // #endregion
    }
}());