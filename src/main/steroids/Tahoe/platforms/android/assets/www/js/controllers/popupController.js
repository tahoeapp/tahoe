﻿// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register controller
    angular.module('tahoeApp.controllers').controller("PopupCtrl", PopupCtrl);

    // 2. Inject dependencies
    PopupCtrl.inject = [];

    // 3. Define public interface and private implementation
    function PopupCtrl($scope, $ionicPopup, $timeout) {

        // #region 3.1 Public Interface
        $scope.isAutoScroll = isAutoScroll;
        $scope.showConfirm = showConfirm;
        $scope.showPopupOffer = showPopupOffer;
        $scope.showPopupCounter = showPopupCounter;
//        $scope.showPopup = showPopup;

        // #endregion

        // #region  3.2. Private implementations
        // A confirm dialog
        function showConfirm() {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Do You Realy Want to Accept Offer ?',
                buttons: [
                    {
                        text: '<b>Yes</b>',
                        type: 'button-balanced',
                        url: '#/app/login'
                    },
                    {
                        text: '<b>Cancel</b>',
                        type: 'button-dark'
                    }
                ]
            });
            confirmPopup.then(function (res) {
                if (res) {
                    console.log('You are sure');
                } else {
                    console.log('You are not sure');
                }
            });
        };
        function showPopupOffer() {
            $scope.data = {};
            var myPopup = $ionicPopup.confirm({
                template: '<div class="offer-alert"><div class="stars-offers"><i class="ion-android-star"></i><i class="ion-android-star"></i><i class="ion-android-star"></i><i class="ion-android-star"></i><i class="ion-android-star-outline"></i></div><p class="write-note">Write a note :</p><textarea></textarea></div>',
                title: '<h1 class="ttl-offer-popup">Give A Review and Rate for <span class="green">Lorem Ipsum</span> Deal</h1>',
                cssClass: 'popup-offer',
                scope: $scope,
                buttons: [
                    {
                        text: 'SUBMIT',
                        type: 'button-balanced'
                    }
                ]
            });
            
         };
         
        function showPopupCounter() {
            $scope.data = {};

            // An elaborate, custom popup
            var myPopup = $ionicPopup.confirm({
                template: '<div class="boost-wrap alert"><span class="eg-box">e.g $ 15</span><span class="boost-box"><i>$</i> BOOST</span></div>',
                title: 'Counter Offer',
                scope: $scope,
                buttons: [
                    {
                        text: 'SEND COUNTER OFFER',
                        type: 'button-balanced'
                    }
                ]
            });
        };
        function isAutoScroll() {
            $scope.autoScroll = ($scope.autoScroll) ? false : true;
            return $scope.autoScroll;
        }
        // #endregion
    }
}());