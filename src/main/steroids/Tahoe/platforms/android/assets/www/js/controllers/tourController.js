﻿// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register controller
    angular.module('tahoeApp.controllers').controller("TourCtrl", TourCtrl);

    // 2. Inject dependencies
    TourCtrl.inject = [];

    // 3. Define public interface and private implementation
    function TourCtrl($scope, $state, $ionicSideMenuDelegate, $ionicPopup, $ionicHistory) {

        // #region 3.1 Public Interface
        
        $ionicSideMenuDelegate.canDragContent(false);

		//Changes to frameheight needed
		var innerHeight = window.innerHeight;
		var innerWidth = window.innerWidth;
		
		//Calculate the button positions
		var buttonHeight = innerHeight / 13.62;
		$scope.bottomButtonTop = innerHeight - buttonHeight;
		
		$scope.middleButtonTop = (innerHeight / 1.705);
		$scope.middleButtonBottom = $scope.middleButtonTop + buttonHeight;
		
		var middleButtonWidth = innerWidth / 1.205;
		$scope.middleButtonLeft = innerWidth / 13.39;
		$scope.middleButtonRight = $scope.middleButtonLeft + middleButtonWidth;

        $scope.frameheight = innerHeight;
        $scope.framewidth = innerWidth;
		
		$scope.endTour = endTour;
        // #endregion
		
        function endTour() {
        	var messagePopup = $ionicPopup.alert({
				template: 'This version of Tahoe is for inventory only! Start listing your items today to show up first in users search. Tahoe full version will come out very soon!'
			});
			messagePopup.then();
        	$ionicSideMenuDelegate.canDragContent(true);
        	$state.go('app.my_profile');
        	$ionicHistory.nextViewOptions({
  				historyRoot: true
  			});
        }
    }
}());