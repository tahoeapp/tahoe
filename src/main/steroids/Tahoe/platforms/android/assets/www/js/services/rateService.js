// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register service
    angular.module('tahoeApp.services').factory("rateService", rateService);

    // 2. Inject dependencies
    rateService.inject = [];

    // 3. Define public interface and private implementation
    function rateService(Restangular, baseUrl) {

        // #region 3.1 Public Interface
       
        // #endregion

        // #region  3.2. Private implementations
        var getRates = function (callback) {
            Restangular.one("rates").get().then(function (result) {
                if (typeof (callback) === 'function') {
                    callback(result);
                }
            });
        };

        var service = {
            getRates: getRates
        }

        return service;
    }
    // #endregion
}());