'use-strict';

var app = angular.module('tahoeApp');

app.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider

        .state('app', {
            url: "/app",
            abstract: true,
            templateUrl: "templates/menu.html",
            controller: 'AppCtrl'
        })

        .state('app.home', {
            url: "/home",
            views: {
                'menuContent': {
                    templateUrl: "templates/home.html",
                    controller: 'HomeCtrl'
                }
            }
        })

        .state('app.card_details', {
            url: "/card_details/:cardId",
            views: {
                'menuContent': {
                    templateUrl: "templates/card_details.html",
                    controller: 'CardDetailsCtrl'
                }
            }
        })

        .state('app.my_profile', {
            url: "/my_profile",
            views: {
                'menuContent': {
                    templateUrl: "templates/my_profile.html",
                    controller: 'MyProfileCtrl'
                }
            }
        })

        .state('app.sign_up', {
            url: "/sign_up",
            views: {
                'menuContent': {
                    templateUrl: "templates/sign_up.html",
                    controller: 'SignUpCtrl'
                }
            }
        })

        .state('app.login', {
            url: "/login",
            views: {
                'menuContent': {
                    templateUrl: "templates/login.html",
                    controller: 'LoginCtrl'
                }
            }
        })

        .state('app.search', {
            url: "/search",
            views: {
                'menuContent': {
                    templateUrl: "templates/search.html"
                }
            }
        })

        .state('app.renting', {
            url: "/renting",
            views: {
                'menuContent': {
                    templateUrl: "templates/renting.html",
                    controller: 'RentingCtrl'
                }
            }
        })

        .state('app.threads', {
            url: "/threads",
            views: {
                'menuContent': {
                    templateUrl: "templates/threads.html",
                    controller: 'ThreadsCtrl'

                }
            }
        })

        .state('app.thread', {
            url: "/threads/:id",
            views: {
                'menuContent': {
                    templateUrl: "templates/thread.html",
                    controller: 'ThreadCtrl'

                }
            }
        })

        .state('app.offer_list', {
            url: "/offer_list",
            views: {
                'menuContent': {
                    templateUrl: "templates/offer_list.html"
                }
            }
        })
        
        .state('app.payment_history_list', {
            url: "/payment_history_list",
            views: {
                'menuContent': {
                    templateUrl: "templates/payment_history_list.html",
                    controller: 'paymentHistoryListCtrl'
                }
            }
        })
        
        .state('app.payment_history_details', {
            url: "/payment_history_details/:id",
            views: {
                'menuContent': {
                    templateUrl: "templates/payment_history_details.html",
                    controller: 'paymentHistoryDetailsCtrl'
                }
            }
        })
        
        .state('app.rent_offer_sent', {
            url: "/rent_offer_sent",
            views: {
                'menuContent': {
                    templateUrl: "templates/rent_offer_sent.html"
                }
            }
        })
        
        .state('app.setting', {
            url: "/setting",
            views: {
                'menuContent': {
                    templateUrl: "templates/setting.html"
                }
            }
        })
                
        .state('app.swap_offer_new', {
            url: "/swap_offer_new",
            views: {
                'menuContent': {
                    templateUrl: "templates/swap_offer_new.html"
                }
            }
        })
        
        .state('app.swap_offer_sent', {
            url: "/swap_offer_sent",
            views: {
                'menuContent': {
                    templateUrl: "templates/swap_offer_sent.html"
                }
            }
        })
        
        .state('app.swapping', {
            url: "/swapping",
            views: {
                'menuContent': {
                    templateUrl: "templates/swapping.html"
                }
            }
        })
        .state('app.message_list', {
            url: "/message_list",
            views: {
                'menuContent': {
                    templateUrl: "templates/message_list.html"
                }
            }
        })
        
        .state('app.message', {
            url: "/message",
            views: {
                'menuContent': {
                    templateUrl: "templates/message.html"
                }
            }
        })
        
        .state('app.counter_received', {
            url: "/counter_received",
            views: {
                'menuContent': {
                    templateUrl: "templates/counter_received.html"
                }
            }
        })

		.state('app.edit_profile', {
            url: "/edit_profile",
            views: {
                'menuContent': {
                    templateUrl: "templates/edit_profile.html",
                    controller: 'EditProfileCtrl'
                }
            }
        })
         
        .state('app.add_item', {
            url: "/add_item",
            views: {
                'menuContent': {
                    templateUrl: "templates/add_item.html",
                    controller: 'AddItemCtrl'
                }
            }
        })
        
        .state('app.edit_item', {
            url: "/edit_item/:cardId",
            views: {
                'menuContent': {
                    templateUrl: "templates/add_item.html",
                    controller: 'AddItemCtrl'
                }
            }
        })
        
        .state('app.about', {
            url: "/about",
            views: {
                'menuContent': {
                    templateUrl: "templates/about.html",
                    controller: 'AboutCtrl'
                }
            }
        })
        
        .state('app.tour', {
            url: "/tour",
            views: {
                'menuContent': {
                    templateUrl: "templates/tour.html",
                    controller: 'TourCtrl'
                }
            }
        });

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/login');
});
