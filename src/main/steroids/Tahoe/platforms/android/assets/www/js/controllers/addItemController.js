﻿// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register controller
    angular.module('tahoeApp.controllers').controller("AddItemCtrl", AddItemCtrl);

    // 2. Inject dependencies
    AddItemCtrl.inject = [];

    // 3. Define public interface and private implementation
    function AddItemCtrl($scope, $state, $stateParams, $ionicActionSheet, $ionicPopup, cardService, localStorageService, locationService, validationService, baseUrl) {

        // #region 3.1 Public Interface
        
        angular.element('select').change(function() {
        	angular.element(this).css('color','black');
        });
        
        $scope.$root.hideMenuIcon = true;
        $scope.$root.hideBackIcon = false;
        
        $scope.isEditing = false;
        
        $scope.card = {
        	swappable: false
        };
        $scope.address = {};
        
        $scope.getFrontPhoto = getFrontPhoto;
        $scope.getBackPhoto = getBackPhoto;
        $scope.getSidePhoto = getSidePhoto;
        $scope.submitCard = submitCard;
        $scope.saveCard = saveCard;
        $scope.itemChanged = itemChanged;
        $scope.typeChanged = typeChanged;
        $scope.removeItem = removeItem;

        $scope.hideFront = true;
        $scope.hideBack = true;
        $scope.hideSide = true;
        
        $scope.hideMakeModel = false;
		$scope.hideYear = false;
		$scope.hideDefects = false;
		$scope.hideRates = false;
		$scope.hideRateService = true;
		$scope.hideSwap = false;
        $scope.hideAuthAmount = false;
        $scope.hideSport = true;
        
        $scope.hideType = false;
        $scope.hideOneSize = false;
        $scope.hideTwoSizes = true;
        $scope.hideSizeLabel1 = true;
        $scope.hideSizeLabel2 = true;

		$scope.sizeLabel1 = "";
		$scope.sizeLabel2 = "";
		$scope.sizePlaceholder1 = "Size";
		$scope.sizePlaceholder2 = ""
		
		$scope.descriptionPlaceholder = "Product Description";
		
		$scope.hideSubmit = false;
		$scope.hideSave = true;
		$scope.hideSpinner = true;
		
		$scope.hideRemove = true;
        
        $scope.disableInput = true;
        
        $scope.items = ["Service"];
        
        $scope.types = [{item: "Service", name: "Class"},
        				{item: "Service", name: "Supervision"},
        				{item: "Service", name: "Gear setup"}];
        				
		$scope.sports = [];
        
        $scope.years = [];
        
        $scope.selectedFrontPhoto = false;
        $scope.selectedBackPhoto = false;
        $scope.selectedSidePhoto = false;
        
        $scope.imageError = true;
		$scope.serviceImageError = true;
        $scope.makeModelError = true;
        
        $scope.photoHeaderLabel = "You Have to Upload 3 Product Photos (front, back & side)";
        $scope.frontPhotoLabel = "Front";
        $scope.backPhotoLabel = "Back";
        $scope.sidePhotoLabel = "Side";
        
        $scope.autocompleteOptions = {types: ['(cities)']};
        
        var year = new Date().getFullYear();
        for (var i = 0; i < 11; i++) {
        	$scope.years.push(year - i);
        }
        
        $scope.itemTypes = localStorageService.readItemTypes();
        var itemArr = [];
        var typeArr = [];
        var serviceSportArr = [];
        for (var i = 0; i < $scope.itemTypes.length; i++) {
        	var item = $scope.itemTypes[i].item;
        	var hideType = $scope.itemTypes[i].hideType == "true";
        	var useSport = $scope.itemTypes[i].useSport == "true";
        	if (useSport) {
        		serviceSportArr.push($scope.itemTypes[i].serviceSport);
        	}
        	itemArr.push(item);
        	for (var j = 0; j < $scope.itemTypes[i].types.length && !hideType; j++) {
        		typeArr.push({item: item, name: $scope.itemTypes[i].types[j].type});
        	}
        }
        $scope.items = itemArr.concat($scope.items);
        $scope.types = typeArr.concat($scope.types);
        $scope.sports = serviceSportArr;
        
        if ($stateParams.cardId) {
        	//If we are in editing mode then show the correct button and get the data
        	$scope.isEditing = true;
        	$scope.hideRemove = false;
        	$scope.hideSubmit = true;
        	$scope.hideSave = false;
        	$scope.hideFront = false;
        	$scope.hideBack = false;
        	$scope.hideSide = false;
        	$scope.frontImage = baseUrl + '/cards/' + $stateParams.cardId + '/front';
            $scope.backImage = baseUrl + '/cards/' + $stateParams.cardId + '/back';
        	$scope.sideImage = baseUrl + '/cards/' + $stateParams.cardId + '/side';
        	cardService.readCard($stateParams.cardId, function (result) {
            	$scope.card = result;
            	for (var i = 0; i < $scope.types.length; i++) {
            		if ($scope.types[i].name === result.type) {
            			$scope.card.type = $scope.types[i];
            		}
            	}
            	//$scope.card.type = {name: result.type};
            	$scope.card.year = parseInt(result.year);
            	itemChanged();
            	typeChanged();
            	locationService.getCityByLatLong(result.latitude, result.longitude, function(result) {
					if (result != "") {
						$scope.address = result;
					}
				});
        	});
        }
        
        // #endregion

        // #region  3.2. Private implementations
        
        function roundToTwo(num) {    
			return +(Math.round(num + "e+2")  + "e-2");
		}
        
        function validateCommonCardItems(card) {
        	var itemField = angular.element(document.getElementById("item-field"));
            var typeField = angular.element(document.getElementById("type-field"));
            var sportField = angular.element(document.getElementById("sport-field"));
            var makeModelField = angular.element(document.getElementById("make-model-field"));
            var yearField = angular.element(document.getElementById("year-field"));
            var sizeField = angular.element(document.getElementById("size-field"));
            var widthField = angular.element(document.getElementById("width-field"));
            var heightField = angular.element(document.getElementById("height-field"));
            var descriptionField = angular.element(document.getElementById("descripition-field"));
            var pricePerDayField = angular.element(document.getElementById("price-per-day-field"));
            var pricePerWeekField = angular.element(document.getElementById("price-per-week-field"));
            var priceService = angular.element(document.getElementById("price-service-field"));
            var authAmountField = angular.element(document.getElementById("auth-amount-field"));
        	var isValid = true;
			
			if (!card.description) {
        		descriptionField.addClass("red-border");
        		isValid = false;
        	}
        	else {
        		descriptionField.removeClass("red-border");
        	}
			
			if (card.item == "Service") {
        		if (!card.type || !card.type.name || card.type.name === "Type") {
					typeField.addClass("red-border");
					isValid = false;
				}
				else {
					typeField.removeClass("red-border");
				}
				
				if (!card.sport || card.sport === "Sport") {
					sportField.addClass("red-border");
					isValid = false;
				}
				else {
					sportField.removeClass("red-border");
				}
				
				if (!card.size) {
					sizeField.addClass("red-border");
					isValid = false;
				}
				else {
					//round the size
					card.size = roundToTwo(parseFloat(card.size));
					sizeField.removeClass("red-border");
				}
				
				if (!card.pricePerDay) {
        			priceService.addClass("red-border");
        			isValid = false;
				}
				else {
					priceService.removeClass("red-border");
				}
			}
			else {
				if (!card.item || card.item === "Item") {
					itemField.addClass("red-border");
					isValid = false;
				}
				else {
					itemField.removeClass("red-border");
				}
				
				if (!card.makeAndModel) {
					makeModelField.addClass("red-border");
					$scope.makeModelError = true;
					isValid = false;
				}
				else if (!validationService.validateMakeAndModel(card.makeAndModel)) {
					makeModelField.addClass("red-border");
					$scope.makeModelError = false;
					isValid = false;
				}
				else {
					makeModelField.removeClass("red-border");
					$scope.makeModelError = true;
				}
			
				if (!card.year || card.year === "Year") {
					yearField.addClass("red-border");
					isValid = false;
				}
				else {
					yearField.removeClass("red-border");
				}
				
				if (!card.pricePerDay) {
					pricePerDayField.addClass("red-border");
					isValid = false;
				}
				else {
					pricePerDayField.removeClass("red-border");
				}
			
				if (!card.pricePerWeek) {
					pricePerWeekField.addClass("red-border");
					isValid = false;
				}
				else {
					pricePerWeekField.removeClass("red-border");
				}
        	
				if (!card.authorizedAmount) {
					authAmountField.addClass("red-border");
					isValid = false;
				}
				else {
					authAmountField.removeClass("red-border");
				}
				
				//Get the details for type and size
				var hideType = false;
				var sizeCount = 1;
				var found = false;
				var item;
				for (var i = 0; i < $scope.itemTypes.length && !found; i++) {
					if ($scope.itemTypes[i].item == card.item) {
						item = $scope.itemTypes[i];
						hideType = item.hideType == "true";
						found = true;
					}
				}
			
				if (!hideType) {
					if (!card.type || !card.type.name || card.type.name === "Type") {
						typeField.addClass("red-border");
						isValid = false;
					}
					else {
						typeField.removeClass("red-border");
					}
					
					if (card.type) {
						found = false;
						for (var i = 0; i < item.types.length && !found; i++) {
							if (item.types[i].type == card.type.name) {
								sizeCount = item.types[i].sizeCount;
								found = true;
							}
						}
					}
				}
				else {
					//If this is a hide type item then there has to be 1 type to determine the size parameters
					sizeCount = item.types[0].sizeCount;
				}
			
				if (sizeCount == 1) {
					if (!card.size) {
						sizeField.addClass("red-border");
						isValid = false;
					}
					else {
						//round the size
						card.size = roundToTwo(parseFloat(card.size));
						sizeField.removeClass("red-border");
					}
				}
				else if (sizeCount == 2) {
					if (!card.width) {
						widthField.addClass("red-border");
						isValid = false;
					}
					else {
						//round the size
						card.width = roundToTwo(parseFloat(card.width));
						widthField.removeClass("red-border");
					}
			
					if (!card.height) {
						heightField.addClass("red-border");
						isValid = false;
					}
					else {
						//round the size
						card.height = roundToTwo(parseFloat(card.height));
						heightField.removeClass("red-border");
					}
				}
				else {
					sizeField.removeClass("red-border");
					widthField.removeClass("red-border");
					heightField.removeClass("red-border");
				}
			}  
			
			return isValid
        }
        
        
        function submitCard() {
			//Validate entries
			var card = $scope.card;
			
			var locationField = angular.element(document.getElementById("location-field"));
            var isValid = validateCommonCardItems(card);
			
			if (!this.address.geometry) {
        		locationField.addClass("red-border");
        		isValid = false;
        	}
        	else {
        		locationField.removeClass("red-border");
        	}
			
			if (card.item == "Service") {
				if (!$scope.selectedFrontPhoto) {
					$scope.serviceImageError = false;
					isValid = false;
				}
				else {
					$scope.imageError = true;
					$scope.serviceImageError = true;
				}  
			} 
			else {	 
				if (!$scope.selectedFrontPhoto || !$scope.selectedBackPhoto || !$scope.selectedSidePhoto) {
					$scope.imageError = false;
					isValid = false;
				}
				else {
					$scope.serviceImageError = true;
					$scope.imageError = true;
				} 	
			}
			
			if (isValid) {
				//Hide the button and show the spinner
				$scope.hideSubmit = true;
				$scope.hideSpinner = false;
				//Sort out the type
				if (card.item !== "Windsurf Sail") {
					var typeName = card.type.name;
					delete card.type;
					card.type = typeName;
				}
				//Obtain the users longitude and latitude
				card.latitude = this.address.geometry.location.lat();
				card.longitude = this.address.geometry.location.lng();
				cardService.addCard($scope.card, function (result) {
					if (result) {
						if (result.errorCode) {
						//Handle error
							var alertPopup = $ionicPopup.alert({
								template: result.errorMessage
							});
							alertPopup.then();							
							$scope.hideSubmit = false;
							$scope.hideSpinner = true;
							return;
						}
						//Upload the pictures
						var cardId = result.cardId;
						cardService.uploadPictureToCard(cardId, "front", $scope.frontImageData, function (result) {
							if (result) {
								if (result.responseCode == 200) {
									onUpdateFrontSuccess(cardId);
								}
								else {
									$scope.hideSave = false;
									$scope.hideSpinner = true;
								}
							}
							else {
								$scope.hideSave = false;
								$scope.hideSpinner = true;
							}
						});
					}
				});
			}
        };
        
        function saveCard() {
			//Validate entries
			var card = $scope.card;
			
			var locationField = angular.element(document.getElementById("location-field"));
            var isValid = validateCommonCardItems(card);
        	
        	if (!this.address) {
        		locationField.addClass("red-border");
        		isValid = false;
        	}
        	else {
        		locationField.removeClass("red-border");
        	}
			
			if (isValid) {
				$scope.hideSave = true;
				$scope.hideSpinner = false;
				//Sort out the type
				if (card.item !== "Windsurf Sail") {
					var typeName = card.type.name;
					delete card.type;
					card.type = typeName;
				}
				if (this.address.geometry) {
					card.latitude = this.address.geometry.location.lat();
					card.longitude = this.address.geometry.location.lng();
            	}
				card.year = "" + card.year
			
				var cardId = card.cardId;
				delete card.cardId;
				delete card.ownerId;
				delete card.averageOwnerRanking;
				delete card.likeCount;
				delete card.viewCount;
				delete card.isFrontLandscape;
			
				cardService.updateCard(cardId, card, function (result) {
					if (result) {
						if (result.errorCode) {
							//Handle error
							var alertPopup = $ionicPopup.alert({
								template: result.errorMessage
							});
							alertPopup.then();
							$scope.hideSave = false;
							$scope.hideSpinner = true;
							return;
						}
						//Upload the pictures
						if ($scope.selectedFrontPhoto) {
							cardService.uploadPictureToCard(cardId, "front", $scope.frontImageData, function (result) {
								if (result) {
									if (result.responseCode == 200) {
										onUpdateFrontSuccess(cardId);
									}
									else {
										$scope.hideSave = false;
										$scope.hideSpinner = true;
									}
								}
								else {
									$scope.hideSave = false;
									$scope.hideSpinner = true;
								}
							});
						}
						else {
							onUpdateFrontSuccess(cardId);
						}
					}
					else {
						$scope.hideSave = false;
						$scope.hideSpinner = true;
					}
				});
			}
        };
        
        function onUpdateFrontSuccess(cardId) {
        	if ($scope.selectedBackPhoto) {
				cardService.uploadPictureToCard(cardId, "back", $scope.backImageData, function (result) {
					if (result) {
						if (result.responseCode == 200) {
							onUpdateBackSuccess(cardId);
						}
						else {
							$scope.hideSave = false;
							$scope.hideSpinner = true;
						}
					}
					else {
						$scope.hideSave = false;
						$scope.hideSpinner = true;
					}
				});
			}
			else {
				onUpdateBackSuccess(cardId);
			}
        }
        
        function onUpdateBackSuccess(cardId) {
        	if ($scope.selectedSidePhoto) {
        		cardService.uploadPictureToCard(cardId, "side", $scope.sideImageData, function (result) {
					if (result.responseCode == 200) {
						$state.go('app.my_profile');
					}
					else {
						$scope.hideSave = false;
						$scope.hideSpinner = true;
					}
				});
			}
			else {
				$scope.hideSave = false;
				$scope.hideSpinner = true;
				$state.go('app.my_profile');
			}
        }
        
        function getFrontPhoto() {
        	if (!$scope.disableInput) {
				$ionicActionSheet.show({buttons: [{text: 'Photo Library'},
												  {text: 'Take Photo'}],
												  cancelText: 'Cancel',
												  buttonClicked: function(index) {
						navigator.camera.getPicture(onFrontSuccess, onFail, {
							quality: 90,
							targetWidth: 1000,
							targetHeight: 1000,
							sourceType: index,
							destinationType: Camera.DestinationType.DATA_URL,
							correctOrientation : true
						});
						return true;
					}
				});
            }
        };

        function onFrontSuccess(imageData) {
        	$scope.selectedFrontPhoto = true;
        	$scope.frontImage = "data:image/jpeg;base64," + imageData;
        	$scope.frontImageData = imageData;
            $scope.hideFront = false;
            $scope.$apply();
        };
        
        function getBackPhoto() {
            if (!$scope.disableInput) {
            	$ionicActionSheet.show({buttons: [{text: 'Photo Library'},
												  {text: 'Take Photo'}],
												  cancelText: 'Cancel',
												  buttonClicked: function(index) {
						navigator.camera.getPicture(onBackSuccess, onFail, {
							quality: 90,
							targetWidth: 1000,
							targetHeight: 1000,
							sourceType: index,
							destinationType: Camera.DestinationType.DATA_URL,
							correctOrientation : true
						});
						return true;
					}
				});
			}
        };

        function onBackSuccess(imageData) {
       		$scope.selectedBackPhoto = true;
            $scope.backImage = "data:image/jpeg;base64," + imageData;
            $scope.backImageData = imageData;
            $scope.hideBack = false;
            $scope.$apply();
        }
        
        function getSidePhoto() {
            if (!$scope.disableInput) {
            	$ionicActionSheet.show({buttons: [{text: 'Photo Library'},
												  {text: 'Take Photo'}],
												  cancelText: 'Cancel',
												  buttonClicked: function(index) {
						navigator.camera.getPicture(onSideSuccess, onFail, {
							quality: 90,
							targetWidth: 1000,
							targetHeight: 1000,
							sourceType: index,
							destinationType: Camera.DestinationType.DATA_URL,
							correctOrientation : true
						});
						return true;
					}
				});
			}
        };

        function onSideSuccess(imageData) {
        	$scope.selectedSidePhoto = true;
            $scope.sideImage = "data:image/jpeg;base64," + imageData;
            $scope.sideImageData = imageData;
            $scope.hideSide = false;
            $scope.$apply();
        }

        function onFail(message) {
        }
        
        function itemChanged() {
        	$scope.disableInput = false;
        	if ($scope.card.item == "Service") {
        		$scope.hideSport = false;
        		$scope.hideMakeModel = true;
				$scope.hideYear = true;
				$scope.hideDefects = true;
				$scope.hideRates = true;
				$scope.hideRateService = false;
				$scope.hideSwap = true;
       			$scope.hideAuthAmount = true;
				$scope.hideOneSize = false;
				$scope.hideTwoSizes = true;
				$scope.hideSizeLabel1 = true;
				$scope.hideSizeLabel2 = true;
				$scope.sizePlaceholder1 = "Duration (hours)";
				$scope.descriptionPlaceholder = "Service Description";
        		$scope.photoHeaderLabel = "You Have to Upload 1 Photo";
        		$scope.frontPhotoLabel = "Required";
        		$scope.backPhotoLabel = "Optional";
        		$scope.sidePhotoLabel = "Optional";
				return;
        	}
        	else {
        		$scope.hideSport = true;
				$scope.hideMakeModel = false;
				$scope.hideYear = false;
				$scope.hideDefects = false;
				$scope.hideRates = false;
				$scope.hideRateService = true;
				$scope.hideSwap = false;
        		$scope.hideAuthAmount = false;
				$scope.descriptionPlaceholder = "Product Description";
        		$scope.photoHeaderLabel = "You Have to Upload 3 Product Photos (front, back & side)";
        		$scope.frontPhotoLabel = "Front";
        		$scope.backPhotoLabel = "Back";
        		$scope.sidePhotoLabel = "Side";
			}
        	var item;
        	var found = false;
        	for (var i = 0; i < $scope.itemTypes.length && !found; i++) {
        		if ($scope.itemTypes[i].item == $scope.card.item) {
        			item = $scope.itemTypes[i];
        			found = true;
        		}
        	}
        	if (found) {
        		var hideType = item.hideType == "true";
        		$scope.hideType = hideType;
        		$scope.hideOneSize = true;
        		$scope.hideTwoSizes = true;
        		if (hideType) {
        			//Because we hide type we must have a default type to get the size details from
        			var defaultType = item.types[0];
        			if (defaultType.sizeCount == 1) {
        				$scope.hideOneSize = false;
        				$scope.hideTwoSizes = true;
        				$scope.hideSizeLabel1 = true;
        				$scope.hideSizeLabel2 = true;
						$scope.sizePlaceholder1 = "Size";
						$scope.sizePlaceholder2 = "";
        				if (defaultType.sizeLabels.length > 0) {
        					$scope.sizeLabel1 = defaultType.sizeLabels[0];
        					$scope.hideSizeLabel1 = false;
        				}
        				if (defaultType.sizePlaceholders.length > 0) {
        					$scope.sizePlaceholder1 = defaultType.sizePlaceholders[0];
        				}
        			}
        			else if (defaultType.sizeCount == 2) {
        				$scope.hideOneSize = true;
        				$scope.hideTwoSizes = false;
        				$scope.hideSizeLabel1 = true;
        				$scope.hideSizeLabel2 = true;
						$scope.sizePlaceholder1 = "Width";
						$scope.sizePlaceholder2 = "Height";
        				if (defaultType.sizeLabels.length == 1) {
        					$scope.sizeLabel1 = defaultType.sizeLabels[0];
        					$scope.hideSizeLabel1 = false;
        				}
        				else if (defaultType.sizeLabels.length == 2) {
        					$scope.sizeLabel1 = defaultType.sizeLabels[0];
        					$scope.sizeLabel2 = defaultType.sizeLabels[1];
        					$scope.hideSizeLabel1 = false;
        					$scope.hideSizeLabel2 = false;
        				}
        				if (defaultType.sizePlaceholders.length == 1) {
        					$scope.sizePlaceholder1 = defaultType.sizePlaceholders[0];
        				}
        				else if (defaultType.sizePlaceholders.length == 2) {
        					$scope.sizePlaceholder1 = defaultType.sizePlaceholders[0];
        					$scope.sizePlaceholder2 = defaultType.sizePlaceholders[1];
        				}
        			}
        		}
        	}
        	else {
        		$scope.hideType = false;
        		$scope.hideOneSize = true;
        		$scope.hideTwoSizes = true;
        		$scope.hideSizeLabel1 = true;
        		$scope.hideSizeLabel2 = true;
				$scope.sizePlaceholder1 = "Size";
				$scope.sizePlaceholder2 = "";
        	}
        }
        
        function typeChanged() {
        	var item;
        	var found = false;
        	if ($scope.card.item == "Service") {
        		if ($scope.card.type.name == "Gear setup") {
        			$scope.hideOneSize = true;
        		}
        		return;
        	}
        	for (var i = 0; i < $scope.itemTypes.length && !found; i++) {
        		if ($scope.itemTypes[i].item == $scope.card.item) {
        			item = $scope.itemTypes[i];
        			found = true;
        		}
        	}
        	if (found) {
        		var type;
        		found = false
				for (var i = 0; i < item.types.length && !found; i++) {
					if (item.types[i].type == $scope.card.type.name) {
						type = item.types[i];
						found = true;
					}
				}
        		$scope.hideOneSize = true;
        		$scope.hideTwoSizes = true;
        		if (type.sizeCount == 1) {
        			$scope.hideOneSize = false;
					$scope.hideTwoSizes = true;
					$scope.hideSizeLabel1 = true;
					$scope.hideSizeLabel2 = true;
					$scope.sizePlaceholder1 = "Size";
					$scope.sizePlaceholder2 = "";
					if (type.sizeLabels.length > 0) {
						$scope.sizeLabel1 = type.sizeLabels[0];
						$scope.hideSizeLabel1 = false;
					}
					if (type.sizePlaceholders.length > 0) {
						$scope.sizePlaceholder1 = type.sizePlaceholders[0];
					}
				}
				else if (type.sizeCount == 2) {
					$scope.hideOneSize = true;
					$scope.hideTwoSizes = false;
					$scope.hideSizeLabel1 = true;
					$scope.hideSizeLabel2 = true;
					$scope.sizePlaceholder1 = "Width";
					$scope.sizePlaceholder2 = "Height";
					if (type.sizeLabels.length == 1) {
						$scope.sizeLabel1 = type.sizeLabels[0];
						$scope.hideSizeLabel1 = false;
					}
					else if (type.sizeLabels.length == 2) {
						$scope.sizeLabel1 = type.sizeLabels[0];
						$scope.sizeLabel2 = type.sizeLabels[1];
						$scope.hideSizeLabel1 = false;
						$scope.hideSizeLabel2 = false;
					}
					if (type.sizePlaceholders.length == 1) {
						$scope.sizePlaceholder1 = type.sizePlaceholders[0];
					}
					else if (type.sizePlaceholders.length == 2) {
						$scope.sizePlaceholder1 = type.sizePlaceholders[0];
						$scope.sizePlaceholder2 = type.sizePlaceholders[1];
					}
				}
        	}
        }
        
        function removeItem() {
			var confirmPopup = $ionicPopup.confirm({
		   		template: 'Are you sure you want to remove this item?',
		   		buttons: [
					{ 
						text: 'Cancel'
					},
				    { 
				    	text: 'Remove', 
						type: 'button-positive',
				    	onTap: function(e) {
           					var cardId = $scope.card.cardId;
							cardService.removeCard(cardId, function (result) {
								if (result) {
									if (result.errorCode != 0) {
										//Handle error
										var alertPopup = $ionicPopup.alert({
											template: result.errorMessage
										});
										alertPopup.then();
										return;
									}
									$state.go('app.my_profile');
								}
							});
         				}
       				}
				]
		 	});
		 	confirmPopup.then();
        }
        
        // #endregion
    }
}());