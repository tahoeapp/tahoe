// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register controller
    angular.module('tahoeApp.controllers').controller("RentingCtrl", RentingCtrl);

    // 2. Inject dependencies
    RentingCtrl.inject = [];

    // 3. Define public interface and private implementation
    function RentingCtrl($scope, localStorageService) {

        // #region 3.1 Public Interface
        
        $scope.$root.hideMenuIcon = true;
        $scope.$root.hideBackIcon = false;
        
        var rates = localStorageService.readRates();
        
        $scope.rentCost = 90;
        $scope.autherizedAmount = 400;
        
        $scope.showConfirm = showConfirm;
        
        $scope.tahoeFee = $scope.rentCost * (rates.feeRate / 100);
        if ($scope.tahoeFee < rates.minimumFee) {
        	$scope.tahoeFee = parseInt(rates.minimumFee);
        }
        
        $scope.totalCost = $scope.rentCost + $scope.tahoeFee;
        
        function showConfirm() {
        	// paypalService.getPaypalAuth(function (result) {
//         		paypalService.startPaypalPayment(result, $scope.rentCost, $scope.tahoeFee, $scope.autherizedAmount, "jpgale@gmail.com", function (result) {
//         			console.log(result);
//         		});
//         	});
			PayPalApp.buy();
		}
    }
}());