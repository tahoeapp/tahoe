// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register service
    angular.module('tahoeApp.services').factory("paypalService", paypalService);

    // 2. Inject dependencies
    paypalService.inject = [];

    // 3. Define public interface and private implementation
    function paypalService(PaypalRestangular, basicAuthService) {

        // #region 3.1 Public Interface
       
        // #endregion

        // #region  3.2. Private implementations
        var getPaypalAuth = function (callback) {
        	PaypalRestangular.all("oauth2/token").post("grant_type=client_credentials", {}, basicAuthService.createPaypalHeader()).then(function (result) {
        		if (typeof (callback) === 'function') {
        			callback(result.access_token);
        		}
        	});
        }
        
        var startPaypalPayment = function (accessToken, rate, fee, autherizedAmount, owner, callback) {
        	PaypalRestangular.all("payments/payment").post(
        	{'intent': 'sale', 
        	 'payer': {'payment_method': 'paypal'}, 
        	 'transactions':[{'amount': {'total': '7.47', 'currency': 'USD'}}]},
        		{}, {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + accessToken}).then(function (result) {
        		if (typeof (callback) === 'function') {
        			callback(result);
        		}
        	}); 
        }

        var service = {
            getPaypalAuth: getPaypalAuth,
            startPaypalPayment: startPaypalPayment
        }

        return service;
    }
    // #endregion
}());