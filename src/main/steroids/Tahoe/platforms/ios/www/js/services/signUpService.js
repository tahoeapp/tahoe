// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register service
    angular.module('tahoeApp.services').factory("signUpService", signUpService);

    // 2. Inject dependencies
    signUpService.inject = [];

    // 3. Define public interface and private implementation
    function signUpService(Restangular, uploadService, baseUrl) {

        // #region 3.1 Public Interface

        // #endregion

        // #region  3.2. Private implementations
        var signUp = function (model, callback) {
            Restangular.all('users').post(model).then(function (result) {
                if ('function' === typeof (callback)) {
                    callback(result);
                }
            });
        }
        
        var uploadAvatar = function (id, picture, callback) {
        	var url = baseUrl + "/users/" + id + "/avatarupload";
        	uploadService.uploadPicture(url, false, picture, callback);
		};

        var service = {
            signUp: signUp,
            uploadAvatar: uploadAvatar
        };

        return service;
        // #endregion
    }
}());