// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register service
    angular.module('tahoeApp.services').factory("locationService", locationService);

    // 2. Inject dependencies
    locationService.inject = [];

    // 3. Define public interface and private implementation
    function locationService($http) {

        // #region 3.1 Public Interface

        // #endregion

        // #region  3.2. Private implementations
        var getLocationByAddress = function (address, callback) {
        	//Create the parameter string
        	var paramStr = "?address=";
        	var minParamSize = paramStr.length;
        	if (address) {
        		paramStr = paramStr + address.replace(/ /g, "+");
        	}
        	if (paramStr.length == minParamSize) {
        		console.log("No valid location data");
        		return;
        	}
        	var locationApiCall = "http://maps.google.com/maps/api/geocode/json" + paramStr;
        	
        	$http.get(locationApiCall).success(function(result) {
      			if (typeof (callback) === 'function') {
        			callback(result.results[0].geometry.location);
        		}
    		});
        }
        
        var getCityByLatLong = function (latitude, longitude, callback) {
        	var paramStr = "?latlng=" + latitude + ","  + longitude;
        	var locationApiCall = "http://maps.google.com/maps/api/geocode/json" + paramStr;
        	
        	$http.get(locationApiCall).success(function(result) {
        		var location = null;
        		var results = result.results;
				for (var i = 0; i < results.length; i++) {
					location = results[i].formatted_address;
        			var types = results[i].types;
        			for (var j = 0; j < types.length; j++) {
        				if (types[j] == "locality") {
							if (typeof (callback) === 'function') {
								callback(location);
								return;
							}
						}
        			}
        		}
        		if (typeof (callback) === 'function') {
					callback("");
				}
    		});
        }

        var service = {
            getLocationByAddress: getLocationByAddress,
            getCityByLatLong: getCityByLatLong
        };

        return service;
        // #endregion
    }
}());