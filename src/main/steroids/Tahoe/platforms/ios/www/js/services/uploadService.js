// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register service
    angular.module('tahoeApp.services').factory("uploadService", uploadService);

    // 2. Inject dependencies
    uploadService.inject = [];

    // 3. Define public interface and private implementation
    function uploadService($http, Restangular, basicAuthService, localStorageService, baseUrl) {

        // #region 3.1 Public Interface
       
        // #endregion

        // #region  3.2. Private implementations
        function writeToFile(picture, callback) {
	        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
	        	fileSystem.root.getFile("upload.jpg", {create: true, exclusive: false}, function (fileEntry) {
	        		var fileLocation = fileEntry.fullPath;
	        		fileEntry.createWriter(function (writer) {
	        			writer.write(picture);
	        			writer.onwriteend = function(evt) {
	        				if (typeof (callback) === 'function') {
                 			   callback(fileEntry.toURL());
                			}
                		}
	        		}, function (error) {
	        			console.log(error.code);
	        		});
	        	}, function (error) {
	        		console.log(error.code);
	        	});
	        }, function (error) {
	        	console.log(error.code);
	        });
    	};
        
        var uploadPicture = function (url, auth, picture, callback) {
        	writeToFile(picture, function (fileLocation) {
				var options = new FileUploadOptions();
				options.fileKey = "file";
				options.fileName = "upload.jpg"
				options.mimetype = "image/jpg"
				if (auth) {
					var loginInfo = localStorageService.readLoginInfo();
					var authHeader = basicAuthService.createHeader(loginInfo);
					var authValue = authHeader.Authorization;
					options.headers = {'Authorization' : authValue}
				}
				var ft = new FileTransfer();
				ft.upload(fileLocation, url, function(result) {
					if (typeof (callback) === 'function') {
						callback(result);
					}
				}, function(err) {	
					if (typeof (callback) === 'function') {
						callback(err);
					}
				}, options);
			});
		};

        var service = {
            uploadPicture: uploadPicture
        }

        return service;
    }
    // #endregion
}());