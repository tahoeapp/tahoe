// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register service
    angular.module('tahoeApp.services').factory("itemTypesService", itemTypesService);

    // 2. Inject dependencies
    itemTypesService.inject = [];

    // 3. Define public interface and private implementation
    function itemTypesService(Restangular) {

        // #region 3.1 Public Interface

        // #endregion

        // #region  3.2. Private implementations
        var getItemTypes = function (callback) {
           	Restangular.all("itemtypes").getList().then(function (result) {
                if (typeof (callback) === 'function') {
                    callback(result);
                }
            });
        }

        var service = {
            getItemTypes: getItemTypes
        };

        return service;
        // #endregion
    }
}());