﻿// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register controller
    angular.module('tahoeApp.controllers').controller("DatepickerDemoCtrl", DatepickerDemoCtrl);

    // 2. Inject dependencies
    DatepickerDemoCtrl.inject = [];

    // 3. Define public interface and private implementation
    function DatepickerDemoCtrl($scope) {

        // #region 3.1 Public Interface
        $scope.clear = clear;

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };

        // Disable weekend selection
        $scope.disabled = disabled;

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];

        $scope.open = open;
        $scope.open1 = open1;
        $scope.open2 = open2;

        $scope.today = today;
        $scope.today();

        $scope.toggleMin = toggleMin;
        $scope.toggleMin();
        // #endregion

        // #region  3.2. Private implementations

        function disabled(date, mode) {
            return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
        };

        function today() {
            $scope.dt1 = new Date();
        };

        function clear() {
            $scope.dt1 = null;
        };

        function toggleMin() {
            $scope.minDate = $scope.minDate ? null : new Date();
        };

        function open($event) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.opened = true;
        };

        function open1($event) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.opened1 = true;
        };

        function open2($event) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.opened2 = true;
        };
        // #endregion
    }
}());