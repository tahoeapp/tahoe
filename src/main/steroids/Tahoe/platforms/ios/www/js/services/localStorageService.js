// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register service
    angular.module('tahoeApp.services').factory("localStorageService", localStorageService);

    // 2. Inject dependencies
    localStorageService.inject = [];

    // 3. Define public interface and private implementation
    function localStorageService() {

        // #region 3.1 Public Interface

        // #endregion

        // #region  3.2. Private implementations
        var clearLocalStorage = function () {
            window.localStorage.clear();
        };

        var saveLoginInfo = function (loginInfo) {
            window.localStorage['loginInfo.id'] = loginInfo.id;
            window.localStorage['loginInfo.email'] = loginInfo.email;
            window.localStorage['loginInfo.password'] = loginInfo.password;
        };

        var readLoginInfo = function () {
            return {
                id: window.localStorage['loginInfo.id'],
                email: window.localStorage['loginInfo.email'],
                password: window.localStorage['loginInfo.password']
            }
        };

        var saveUserNameInfo = function (user) {
            window.localStorage['user.firstName'] = user.firstName;
            window.localStorage['user.lastName'] = user.lastName;
        };

        var readUserNameInfo = function () {
            return {
                firstName: window.localStorage['user.firstName'],
                lastName: window.localStorage['user.lastName']
            }
        };
        
        var saveItemTypes = function (itemTypes) {
        	window.localStorage['itemTypes.count'] = itemTypes.length;
        	for (var i = 0; i < itemTypes.length; i++) {
        		window.localStorage['itemTypes.'+i+'.item'] = itemTypes[i].item;
        		window.localStorage['itemTypes.'+i+'.hideType'] = itemTypes[i].hideType;
        		window.localStorage['itemTypes.'+i+'.serviceSport'] = itemTypes[i].serviceSport;
        		window.localStorage['itemTypes.'+i+'.useSport'] = itemTypes[i].useSport;
        		window.localStorage['itemTypes.'+i+'.typesCount'] = itemTypes[i].types.length;
        		for (var j = 0; j < itemTypes[i].types.length; j++) {
        			window.localStorage['itemTypes.'+i+'.types.'+j+'.type'] = itemTypes[i].types[j].type;
        			window.localStorage['itemTypes.'+i+'.types.'+j+'.typeDisplay'] = itemTypes[i].types[j].typeDisplay;
        			window.localStorage['itemTypes.'+i+'.types.'+j+'.sizeCount'] = itemTypes[i].types[j].sizeCount;
        			window.localStorage['itemTypes.'+i+'.types.'+j+'.sizeLabelCount'] = itemTypes[i].types[j].sizeLabels.length;
        			for (var k = 0; k < itemTypes[i].types[j].sizeLabels.length; k++) {
        				window.localStorage['itemTypes.'+i+'.types.'+j+'.sizeLabel.'+k+'.label'] = itemTypes[i].types[j].sizeLabels[k].sizeLabel;
        			}
        			window.localStorage['itemTypes.'+i+'.types.'+j+'.sizePlaceHoldersCount'] = itemTypes[i].types[j].sizePlaceholders.length;
        			for (var k = 0; k < itemTypes[i].types[j].sizePlaceholders.length; k++) {
        				window.localStorage['itemTypes.'+i+'.types.'+j+'.sizePlaceholder.'+k+'.placeholder'] = itemTypes[i].types[j].sizePlaceholders[k].sizePlaceholder;
        			}
        		}
        	}
        };

        var readItemTypes = function () {
        	var items = [];
        	var count = window.localStorage['itemTypes.count'];
        	for (var i = 0; i < count; i++) {
        		var types = [];
        		var typesCount = window.localStorage['itemTypes.'+i+'.typesCount'];
        		for (var j = 0; j < typesCount; j++) {
        			var sizeLabelsCount = window.localStorage['itemTypes.'+i+'.types.'+j+'.sizeLabelCount'];
        			var sizeLabels = [];
        			for (var k = 0; k < sizeLabelsCount; k++) {
        				sizeLabels.push(window.localStorage['itemTypes.'+i+'.types.'+j+'.sizeLabel.'+k+'.label']);
        			}
        			var sizePlaceholdersCount = window.localStorage['itemTypes.'+i+'.types.'+j+'.sizePlaceHoldersCount'];
        			var sizePlaceholders = [];
        			for (var k = 0; k < sizePlaceholdersCount; k++) {
        				sizePlaceholders.push(window.localStorage['itemTypes.'+i+'.types.'+j+'.sizePlaceholder.'+k+'.placeholder']);
        			}
        			types.push({type: window.localStorage['itemTypes.'+i+'.types.'+j+'.type'],
        						typeDisplay: window.localStorage['itemTypes.'+i+'.types.'+j+'.typeDisplay'],
        						sizeCount: window.localStorage['itemTypes.'+i+'.types.'+j+'.sizeCount'],
        						sizeLabels: sizeLabels,
        						sizePlaceholders: sizePlaceholders});
        		}
        		items.push({item: window.localStorage['itemTypes.'+i+'.item'],
        					hideType: window.localStorage['itemTypes.'+i+'.hideType'],
        					types: types,
        					serviceSport: window.localStorage['itemTypes.'+i+'.serviceSport'],
        					useSport: window.localStorage['itemTypes.'+i+'.useSport']});
        	}
        	return items;
        };
        
        var saveRates = function (rates) {
        	window.localStorage['rates.feeRate'] = rates.feeRate;
            window.localStorage['rates.minimumFee'] = rates.minimumFee;
        };
        
        var readRates = function () {
        	return {
                feeRate: window.localStorage['rates.feeRate'],
                minimumFee: window.localStorage['rates.minimumFee']
            }
        };

        var service = {
            clearLocalStorage: clearLocalStorage,
            saveLoginInfo: saveLoginInfo,
            readLoginInfo: readLoginInfo,
            saveUserNameInfo: saveUserNameInfo,
            readUserNameInfo: readUserNameInfo,
            saveItemTypes: saveItemTypes,
            readItemTypes: readItemTypes,
            saveRates: saveRates,
            readRates: readRates
        }

        return service;
        // #endregion
    }
}());