﻿// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register controller
    angular.module('tahoeApp.controllers').controller("paymentHistoryListCtrl", paymentHistoryListCtrl);

    // 2. Inject dependencies
    paymentHistoryListCtrl.inject = [];

    // 3. Define public interface and private implementation
    function paymentHistoryListCtrl($scope, paymentHistoryService) {
        // #region 3.1 Public Interface
        $scope.paymentHistoryList = [];
       
        // #endregion


        // #region  3.2. Private implementations
        paymentHistoryService.getPaymentHistoryList(function (err, data) {
            if (!err) {
                $scope.paymentHistoryList = data;
            }
        });
        // #endregion
    }
}());