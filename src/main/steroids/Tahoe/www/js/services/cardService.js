// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register service
    angular.module('tahoeApp.services').factory("cardService", cardService);

    // 2. Inject dependencies
    cardService.inject = [];

    // 3. Define public interface and private implementation
    function cardService(Restangular, basicAuthService, localStorageService, uploadService, baseUrl) {

        // #region 3.1 Public Interface
       
        // #endregion

        // #region  3.2. Private implementations
        var readCard = function (cardId, callback) {
            Restangular.one("cards", cardId).get().then(function (result) {
                if (typeof (callback) === 'function') {
                    callback(result);
                }
            });
        };
        var readNearbyCards = function (page, callback) {
        	return Restangular.all('cards/nearby').getList({"page":page}).then(function (result) {
        		if (typeof (callback) === 'function') {
        			callback(result);
        		}
        	});
        };
        
        var addCard = function (card, callback) {
            var loginInfo = localStorageService.readLoginInfo();
            Restangular.all("me/cards").post(card, {}, basicAuthService.createHeader(loginInfo)).then(function (result) {
                if (typeof (callback) === 'function') {
                    callback(result);
                }
            });
        };
        
        var updateCard = function (cardId, card, callback) {
            var loginInfo = localStorageService.readLoginInfo();
            Restangular.all("me/cards/" + cardId).post(card, {}, basicAuthService.createHeader(loginInfo)).then(function (result) {
                if (typeof (callback) === 'function') {
                    callback(result);
                }
            });
        };
        
        var uploadPictureToCard = function (cardId, pictureType, picture, callback) {
        	var url = baseUrl + "/me/cards/" + cardId + "/" + pictureType;
			uploadService.uploadPicture(url, true, picture, callback);
		};
		
        var removeCard = function (cardId, callback) {
            var loginInfo = localStorageService.readLoginInfo();
            Restangular.all("me/cards/" + cardId + "/remove").post({}, {}, basicAuthService.createHeader(loginInfo)).then(function (result) {
                if (typeof (callback) === 'function') {
                    callback(result);
                }
            });
        };

        var service = {
            readCard: readCard,
            readNearbyCards: readNearbyCards,
            addCard: addCard,
            updateCard: updateCard,
            uploadPictureToCard: uploadPictureToCard,
            removeCard: removeCard
        }

        return service;
    }
    // #endregion
}());