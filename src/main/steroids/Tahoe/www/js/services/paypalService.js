// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register service
    angular.module('tahoeApp.services').factory("paypalService", paypalService);

    // 2. Inject dependencies
    paypalService.inject = [];

    // 3. Define public interface and private implementation
    function paypalService(PaypalRestangular, basicAuthService) {

        // #region 3.1 Public Interface
       
        // #endregion

        // #region  3.2. Private implementations
        var getPaypalAuth = function (callback) {
        	PaypalRestangular.all("oauth2/token").post("grant_type=client_credentials", {}, basicAuthService.createPaypalHeader()).then(function (result) {
        		if (typeof (callback) === 'function') {
        			callback(result.access_token);
        		}
        	});
        }
        
        var saveCardDetails = function(accessToken, cardDetails, callback) {
        	PaypalRestangular.all("vault/credit-cards").post(
        	{'payer_id': cardDetails.payer, 
        	 'type': cardDetails.type, 
        	 'number': cardDetails.number,
        	 'first_name': cardDetails.firstName,
        	 'last_name': cardDetails.lastName,
        	 'expire_month': cardDetails.expirationMonth,
        	 'expire_year': cardDetails.expirationYear,
        	 'cvv2': cardDetails.cvv,
        	 'billing_address': {'line1': cardDetails.line1,
        	 					 'line2': cardDetails.line2,
        	 					 'city': cardDetails.city,
        	 					 'state': cardDetails.state,
        	 					 'postal_code': cardDetails.postalCode,
        	 					 'country_code': cardDetails.country}},
        		{}, {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + accessToken}).then(function (result) {
        		if (typeof (callback) === 'function') {
        			callback(result.id);
        		}
        	}); 
        }
        
        var authPayment = function (accessToken, rentalDetails, callback) {
        	PaypalRestangular.all("payments/payment").post(
        	{'intent': 'authorize', 
        	 'payer': {'payment_method': 'credit_card',
        	 		   'funding_instruments': [{"credit_card_token": {
        	 		   		'credit_card_id': rentalDetails.creditCardId,
        	 		   		'payer_id': rentalDetails.creditCard.payer,
        	 		   		'last4': rentalDetails.creditCard.last4,
        	 		   		'type': rentalDetails.creditCard.type,
        	 		   		'expire_year': rentalDetails.creditCard.expirationYear,
        	 		   		'expire_month': rentalDetails.creditCard.expirationMonth
        	 		   }}]}, 
        	 'transactions':[{'amount': {'total': rentalDetails.total.toFixed(2), 'currency': 'USD'}}]},
        		{}, {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + accessToken}).then(function (result) {
        		if (typeof (callback) === 'function') {
        			callback(result);
        		}
        	}); 
        }

        var service = {
            getPaypalAuth: getPaypalAuth,
            saveCardDetails: saveCardDetails,
            authPayment: authPayment
        }

        return service;
    }
    // #endregion
}());