// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register service
    angular.module('tahoeApp.services').factory("myProfileService", myProfileService);

    // 2. Inject dependencies
    myProfileService.inject = [];

    // 3. Define public interface and private implementation
    function myProfileService(Restangular, basicAuthService, localStorageService) {

        // #region 3.1 Public Interface

        // #endregion

        // #region  3.2. Private implementations
        var createAuth = function () {
            var loginInfo = localStorageService.readLoginInfo();
            return basicAuthService.createHeader(loginInfo);
        };
        var readMyCards = function (callback) {
            Restangular.all("me/cards").getList({}, createAuth()).then(function (result) {
                if (typeof (callback) === 'function') {
                    callback(result);
                }
            });
        };

        var readMyInfo = function (callback) {
            Restangular.one("me/user").get({}, createAuth()).then(function (result) {
                callback(result);
            });
        };

        var updateMyInfo = function (model, callback) {
        	var loginInfo = localStorageService.readLoginInfo();
            Restangular.all("me/user").post(model, {}, createAuth()).then(function (result) {
                callback(result);
            });
        };

        var service = {
            readMyCards: readMyCards,
            readMyInfo: readMyInfo,
            updateMyInfo: updateMyInfo
        };

        return service;
        // #endregion
    }
}());