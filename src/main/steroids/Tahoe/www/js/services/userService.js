// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register service
    angular.module('tahoeApp.services').factory("userService", userService);

    // 2. Inject dependencies
    userService.inject = [];

    // 3. Define public interface and private implementation
    function userService(Restangular, baseUrl) {

        // #region 3.1 Public Interface
       
        // #endregion

        // #region  3.2. Private implementations
        var getUser = function (id, callback) {
            Restangular.one("users/"+id).get().then(function (result) {
                if (typeof (callback) === 'function') {
                    callback(result);
                }
            });
        };

        var service = {
            getUser: getUser
        }

        return service;
    }
    // #endregion
}());