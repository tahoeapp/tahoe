// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register service
    angular.module('tahoeApp.services').factory("cardDisplayService", cardDisplayService);

    // 2. Inject dependencies
    cardDisplayService.inject = [];

    // 3. Define public interface and private implementation
    function cardDisplayService() {

        // #region 3.1 Public Interface
       
        // #endregion
		var singleCardDisplay = function (card, itemTypes) {
			//Find the item and type information
			var sizeCount = 1;
			var found = false;
			var typeDisplay = card.type;
			for (var j = 0; j < itemTypes.length && !found; j++) {
				if (itemTypes[j].item == card.item) {
					//If this item has no type then use the default type
					if (itemTypes[j].hideType == "true") {
						typeDisplay = itemTypes[j].types[0].typeDisplay;
						sizeCount = itemTypes[j].types[0].sizeCount;
					}
					else {
						for (var k = 0; k < itemTypes[j].types.length && !found; k++) {
							if (itemTypes[j].types[k].type == card.type) {
								typeDisplay = itemTypes[j].types[k].typeDisplay;
								sizeCount = itemTypes[j].types[k].sizeCount;
								found = true;
							}
						}
					}
					found = true;
				}
			}
			if (card.item == "Service") {
				card.typeDisplay = card.sport + " " + typeDisplay;
				//For a service use the size display to display the first 3 words of the description
				var words = card.description.split(" ");
				if (words.length <= 3) {
					card.sizeDisplay = card.description;
				}
				else {
					var description = "";
					for (var j = 0; j < 3; j++) {
						description = description.concat(words[j]) + " ";
					}
					card.sizeDisplay = description + "...";
				}
				//Display the number of hours in the price
				card.priceDisplay = "$ " + card.pricePerDay + " | " + card.size + " hours";
			}
			else {
				card.typeDisplay = typeDisplay;
				card.sizeDisplay = "Size: " + card.size;
				if (sizeCount == 2) {
					card.sizeDisplay = "Size: " + card.width + " x " + card.height;
				}
				card.yearDisplay = card.year;
				card.priceDisplay = "$ " + card.pricePerDay + "/day | $ " + card.pricePerWeek + "/week";
			}
			return card	
		}

        // #region  3.2. Private implementations
        var setupCardDisplay = function (cards, itemTypes) {
			for (var i = 0; i < cards.length; i++) {
				cards[i] = singleCardDisplay(cards[i], itemTypes);
				
			}
			return cards;
        }

        var service = {
        	singleCardDisplay: singleCardDisplay,
            setupCardDisplay: setupCardDisplay
        }

        return service;
    }
    // #endregion
}());