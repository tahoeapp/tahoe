// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register service
    angular.module('tahoeApp.services').factory("imageCacheService", imageCacheService);

    // 2. Inject dependencies
    imageCacheService.inject = [];

    // 3. Define public interface and private implementation
    function imageCacheService(base64EncodeService) {

        // #region 3.1 Public Interface

        // #endregion

        // #region  3.2. Private implementations
        
        function readForConvert(filePath, callback) {
        	window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
	        	fileSystem.root.getFile(filePath, {}, function (fileEntry) {
	        		//The cached file does exist return the data
	        		fileEntry.file(function(file) {
	        			var fileReader = new FileReader();
	        			fileReader.onloadend = function(evt) {
	        				if (typeof (callback) === 'function') {
								callback(base64EncodeService.base64Encode(evt.target.result));
							}
	        			}
	        			fileReader.readAsBinaryString(file);
	        		}, function (error) {
	        			console.log(error.code);
	        		});
	        	}, function (error) {
	        		console.log(error.code); 
	        	});
	        }, function (error) {
	        	console.log(error.code);
	        });
        };
        
        function convertToBase64(filePath, callback) {
        	readForConvert(filePath, function (data) {
				window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
					fileSystem.root.getFile(filePath, {}, function (fileEntry) {
						//The cached file does exist return the data
						fileEntry.createWriter(function (writer) {
							writer.write(data);
							writer.onwriteend = function(evt) {
								if (typeof (callback) === 'function') {
								   callback();
								}
							};
						}, function (error) {
							console.log(error.code);
						});
					}, function (error) {
						console.log(error.code); 
					});
				}, function (error) {
					console.log(error.code);
				});
			});
        };
        
        function createDir(rootDirEntry, folders, callback) {
        	if (folders[0] == null) {
        		if (typeof (callback) === 'function') {
					callback(rootDirEntry.toURL());
				}
				return;
			}
        	rootDirEntry.getDirectory("" + folders[0], {create: true, exclusive: false}, function(dirEntry) {
        		if (folders.length) {
        			createDir(dirEntry, folders.slice(1), callback);
        		}
        	}, function(error) {
        		console.log(error);
        	});
        };
        
        function downloadFileToCache (url, index, callback) {
        	//Make the url from the id type, the id and the image type
        	//e.g. user 2 avatar is /user/2/avatar or card 5 front is /card/5/front
        	var pathComponents = url.split('/');
        	var type = pathComponents.pop();
        	var id = pathComponents.pop();
        	var idType = pathComponents.pop();
        	var pathToCacheFolder = "cache/" + idType + "/" + id + "/" + type;
        	var pathToCacheFile = "image.jpg";
        	//Get the file system object
        	window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
	        	createDir(fileSystem.root, pathToCacheFolder.split('/'), function(path) {
        			var downloadPath = path + pathToCacheFile;
        			console.log(downloadPath);
					var fileTransfer = new FileTransfer();
					fileTransfer.download(url, downloadPath, function (entry) {
						convertToBase64(pathToCacheFolder + "/" + pathToCacheFile, function() {
							loadImage(url, index, callback);
						});
					}, function(error) {
						console.log("Unable to download picture " + url);
						if (typeof (callback) === 'function') {
							callback();
						}
					});  
				});      		
	        }, function (error) {
	        	console.log(error.code);
	        	callback();
	        });
        };
        
        var loadImage = function (url, index, callback) {
        	//Make the url from the id type, the id and the image type
        	//e.g. user 2 avatar is /user/2/avatar or card 5 front is /card/5/front
        	var pathComponents = url.split('/');
        	var type = pathComponents.pop();
        	var id = pathComponents.pop();
        	var idType = pathComponents.pop();
        	var pathToCacheFolder = "cache/" + idType + "/" + id + "/" + type;
        	var pathToCacheFile = pathToCacheFolder + "/image.jpg";
        	//Get the file system object
        	window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
	        	fileSystem.root.getFile(pathToCacheFile, {}, function (fileEntry) {
	        		//The cached file does exist return the data
	        		fileEntry.file(function(file) {
	        			var fileReader = new FileReader();
	        			fileReader.onloadend = function(evt) {
	        				if (typeof (callback) === 'function') {
	        					callback(evt.target.result, index);
	        				}
	        			}
	        			fileReader.readAsBinaryString(file);
	        		}, function (error) {
	        			//If we are unable to read the file then delete and redownload
	        			deleteImage(idType, id, type, function() {
		        			loadImage(url);
		        		});
	        		});
	        	}, function (error) {
	        		//The cached file does not exist download and save the file
	        		downloadFileToCache(url, index, callback);     		
	        	});
	        }, function (error) {
	        	console.log(error.code);
	        	callback();
	        });
        };
        
        var deleteImage = function (idType, id, type, callback) {	
        	var pathToCacheFile = "/cache/" + idType + "/" + id + "/" + type + "/image.jpg";
        	window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
        		fileSystem.root.getFile(pathToCacheFile, {}, function(fileEntry) {
        			fileEntry.remove(function() {
        				if (typeof(callback) === 'function') {
        					callback();
        				}
        			}, function(error) {
        				if (typeof(callback) === 'function') {
        					callback();
        				}
        			});
        		}, function(error) {
					if (typeof(callback) === 'function') {
						callback();
					}
        		});
        	}, function(error) {
				if (typeof(callback) === 'function') {
					callback();
				}
        	});
        };
        
        var deleteAllCache = function (callback) {
        	var pathToCache = "/cache";
        	window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
        		fileSystem.root.getDirectory(pathToCache, {}, function(dirEntry) {
        			dirEntry.removeRecursively(function() {
        				if (typeof(callback) === 'function') {
        					callback();
        				}
        			}, function(error) {
        				if (typeof(callback) === 'function') {
        					callback();
        				}
        			});
        		}, function(error) {
					if (typeof(callback) === 'function') {
						callback();
					}
        		});
        	}, function(error) {
				if (typeof(callback) === 'function') {
					callback();
				}
        	});
        };

        var service = {
            loadImage: loadImage,
            deleteImage: deleteImage,
            deleteAllCache: deleteAllCache
        };

        return service;
        // #endregion
    }
}());