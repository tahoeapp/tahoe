﻿// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register controller
    angular.module('tahoeApp.controllers').controller("LoginCtrl", LoginCtrl);

    // 2. Inject dependencies
    LoginCtrl.inject = [];

    // 3. Define public interface and private implementation
    function LoginCtrl($scope, $state, $ionicPopup, $ionicSideMenuDelegate, $ionicHistory, loginService, localStorageService, validationService, appHomeUrl) {

        // #region 3.1 Public Interface
        $ionicSideMenuDelegate.canDragContent(false);
        
        $scope.loginInfo = {
            id: '',
            email: '',
            password: ''
        };

        $scope.login = login;
        $scope.resetPassword = resetPassword;
        
        $scope.resetPasswordError = true;
        $scope.passwordError = true;
        // #endregion

        // #region  3.2. Private implementations
        function login() {
        	//Ensure that the reset password error is cancelled
        	$scope.resetPasswordError = true;
        
        	//Get the fields used for validation
        	var emailField = angular.element(document.getElementById("email-field"));
        	var passwordField = angular.element(document.getElementById("password-field"));
        	var isValid = true;
        	
        	//Ensure that we have an email and password
        	if (!$scope.loginInfo.email) {
        		emailField.addClass("red-border");
        		isValid = false;
            }
            else {
            	emailField.removeClass("red-border");
            }
            
        	if (!$scope.loginInfo.password || !validationService.validatePassword($scope.loginInfo.password)) {
        		passwordField.addClass("red-border");
        		$scope.passwordError = false;
        		isValid = false;
            }
            else {
            	passwordField.removeClass("red-border");
            	$scope.passwordError = true;
            }
            
            if (isValid) {
				loginService.validateLogin($scope.loginInfo, function (result) {
					if (result) {
						if (result.status) {
							//Handle error
							var alertPopup = $ionicPopup.alert({
								template: 'Unable to find email address or password.'
							});
							alertPopup.then();
							return;
						}
						$scope.loginInfo.id = result.id; // add the id that was retrieved from the server
						localStorageService.saveLoginInfo($scope.loginInfo);
						localStorageService.saveUserNameInfo(result);
						$ionicSideMenuDelegate.canDragContent(true);
				
						$state.go('app.my_profile');
						$ionicHistory.nextViewOptions({
							historyRoot: true
						});
					}
				});
			}
        }
        
        function resetPassword() {
        	//Ensure that the errors for login are cancelled
        	var passwordField = angular.element(document.getElementById("password-field"));
            passwordField.removeClass("red-border");
            $scope.passwordError = true;
        
        	//Ensure that we have an email
        	var emailField = angular.element(document.getElementById("email-field"));
        	var isValid = true;
        	
        	if (!$scope.loginInfo.email) {
        		emailField.addClass("red-border");
        		$scope.resetPasswordError = false;
        		isValid = false;
            }
            else {
            	emailField.removeClass("red-border");
            	$scope.resetPasswordError = true;
            }
            
            if (isValid) {
	            loginService.resetPassword($scope.loginInfo);
	            //Show a popup to say that the reset password email has been send
	            var alertPopup = $ionicPopup.alert({
					template: 'An email to reset your password has been sent.'
				});
				alertPopup.then();
	        }
        }
        // #endregion
    }
}());