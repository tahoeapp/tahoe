// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register controller
    angular.module('tahoeApp.controllers').controller("RentingCtrl", RentingCtrl);

    // 2. Inject dependencies
    RentingCtrl.inject = [];

    // 3. Define public interface and private implementation
    function RentingCtrl($state, $scope, localStorageService, cardDisplayService) {

        // #region 3.1 Public Interface
        
        $scope.$root.hideMenuIcon = true;
        $scope.$root.hideBackIcon = false;
        
        $scope.datepickers = {
        	startDateOpened: false,
        	endDateOpened: false
      	}

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };

        $scope.format = 'dd-MMMM-yyyy';
        
        $scope.minDate = new Date();
        $scope.maxDate = '2040-06-30';
        
        $scope.note = "";

        // #endregion

        // #region  3.2. Private implementations
        
        var rates = localStorageService.readRates();
        
        $scope.itemTypes = localStorageService.readItemTypes();
        
        $scope.rentCostPerDay = 0;
        $scope.rentCostPerWeek = 0;
        $scope.authorizedAmount = 0;
        
        $scope.validDateRange = false;
        
        var cards = $scope.$root.cart;
        
        if (cards.length > 0) {
        	var cardDisplay = cardDisplayService.singleCardDisplay(cards[0], $scope.itemTypes);
        	$scope.yearDisplay = cardDisplay.yearDisplay;
        	$scope.makeAndModel = cardDisplay.makeAndModel;
			$scope.typeDisplay = cardDisplay.typeDisplay;
        	$scope.sizeDisplay = cardDisplay.sizeDisplay;
        	$scope.description = cardDisplay.description;
        	
        	//Get the total cost of the rental
        	for (var i = 0; i < cards.length; i++) {
        		$scope.rentCostPerDay = $scope.rentCostPerDay + cards[i].pricePerDay;
        		$scope.rentCostPerWeek = $scope.rentCostPerWeek + cards[i].pricePerWeek;
        		$scope.authorizedAmount = $scope.authorizedAmount + cards[i].authorizedAmount;
        	}
        }
        
        $scope.showPayment = showPayment;
        $scope.dateChanged = dateChanged;
        $scope.open = open;
        
        function open($event, date) {
            $event.preventDefault();
            $event.stopPropagation();
            
        	$scope.datepickers[date] = true;
        }
        
        function dateChanged() {
        	if (this.startDate && this.endDate) {
        		$scope.validDateRange = true;
        		$scope.rentLength = ((this.endDate.getTime() - this.startDate.getTime()) / (1000 * 60 * 60 * 24)) + 1;
        		$scope.rentCost = $scope.rentCostPerDay * $scope.rentLength;
        		if ($scope.rentLength > 6) {
        			var numWeeks = parseInt($scope.rentLength / 7);
        			var numDays = $scope.rentLength % 7;
        			$scope.rentCost = ($scope.rentCostPerWeek * numWeeks) +
        					   ($scope.rentCostPerDay * numDays);
        		}
        		
        		$scope.tahoeFee = $scope.rentCost * (rates.feeRate / 100);
        		if ($scope.tahoeFee < rates.minimumFee) {
        			$scope.tahoeFee = parseInt(rates.minimumFee);
        		}	
        
        		$scope.totalCost = $scope.rentCost + $scope.tahoeFee;
        	}
        	else {
        		$scope.validDateRange = false;
        	}
        }
        
        function showPayment() {
        	var userId = localStorageService.readLoginInfo();
        	var rentalItems = [];
        	for (var i = 0; i < cards.length; i++) {
        		rentalItems.push(cards[i]);
        	}
        	var rentalDetails = {
        		user: userId.id,
        		userEmail: userId.email,
        		items: rentalItems,
        		cost: $scope.rentCost,
        		fee: $scope.tahoeFee,
        		total: $scope.totalCost,
        		authAmount: $scope.authorizedAmount,
        		startDate: this.startDate,
        		endDate: this.endDate,
        		note: this.note
        	}
			$state.go('app.payment_entry', {rentalDetails: angular.toJson(rentalDetails)});
		}
    }
}());