﻿// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register controller
    angular.module('tahoeApp.controllers').controller("ThreadsCtrl", ThreadsCtrl);

    // 2. Inject dependencies
    ThreadsCtrl.inject = [];

    // 3. Define public interface and private implementation
    function ThreadsCtrl($scope, threadService, baseUrl) {

        // #region 3.1 Public Interface
        $scope.search = '';
        $scope.displayNames = displayNames;
        $scope.buildLastMsgAvatarUrl = buildLastMsgAvatarUrl;
        $scope.displayLastMsgDate = displayLastMsgDate;

        // #endregion

        // #region  3.2. Private implementations
        threadService.readMyThreads(function (result) {
            $scope.threads = result;
        });

        function displayNames(thread) {
            var names = {};
            thread.messages.forEach(function (message) {
                names[message.sender.id] = message.sender.firstName + ' ' + message.sender.lastName;
            });

            var asList = Object.keys(names).map(function (key) {
                return names[key];
            });

            return asList.join(', ');
        };

        function buildLastMsgAvatarUrl(thread) {
            var len = thread.messages.length;
            var id = thread.messages[len - 1].sender.id;

            return baseUrl + '/users/' + id + '/avatar';
        };

        function displayLastMsgDate(thread) {
            var len = thread.messages.length;
            return len ? thread.messages[len - 1].createDate : 0;
        };

        // #endregion
    }
}());