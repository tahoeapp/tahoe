﻿// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register controller
    angular.module('tahoeApp.controllers').controller("SearchCtrl", SearchCtrl);

    // 2. Inject dependencies
    SearchCtrl.inject = [];

    // 3. Define public interface and private implementation
    function SearchCtrl($scope, searchService, localStorageService) {

        // #region 3.1 Public Interface

        // #endregion

        // #region  3.2. Private implementations

        // #endregion
    }
	// function closeSearchWhenClickingElsewhere(event, callbackOnClose) {
	// 
	// 	            var clickedElement = event.target;
	// 	            if (!clickedElement) return;
	// 
	// 	            var elementClasses = clickedElement.classList;
	// 	            var clickedOnSearchDrawer = elementClasses.contains('search-wrap') || (clickedElement.parentElement !== null && clickedElement.parentElement.classList.contains('search-wrap'));
	// 
	// 	            if (!clickedOnSearchDrawer) {
	// 	                callbackOnClose();
	// 	                return;
	// 	            }
	// 
	// 	        }
	}());