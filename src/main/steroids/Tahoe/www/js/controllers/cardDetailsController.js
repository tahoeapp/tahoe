﻿// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register controller
    angular.module('tahoeApp.controllers').controller("CardDetailsCtrl", CardDetailsCtrl);

    // 2. Inject dependencies
    CardDetailsCtrl.inject = [];

    // 3. Define public interface and private implementation
    function CardDetailsCtrl($scope, $state, $stateParams, cardService, cardDisplayService, localStorageService, userService, baseUrl, $ionicSlideBoxDelegate) {

        // #region 3.1 Public Interface

        $scope.$root.hideMenuIcon = true;
        
        $scope.rankingStar1 = false;
        $scope.rankingStar2 = false;
        $scope.rankingStar3 = false;
        $scope.rankingStar4 = false;
        $scope.rankingStar5 = false;
        
        $scope.itemTypes = localStorageService.readItemTypes();

        // #endregion

        // #region  3.2. Private implementations
        cardService.readCard($stateParams.cardId, function (result) {
            $scope.card = cardDisplayService.singleCardDisplay(result, $scope.itemTypes);
            $scope.ownerAvatarUrl = baseUrl + '/users/' + $scope.card.ownerId + '/avatar';
            userService.getUser($scope.card.ownerId, function (result) {
            	$scope.ownerName = result.firstName + " " + result.lastName;
            	$scope.rankingStar1 = result.totalRanking >= 1;
            	$scope.rankingStar2 = result.totalRanking >= 2;
            	$scope.rankingStar3 = result.totalRanking >= 3;
            	$scope.rankingStar4 = result.totalRanking >= 4;
            	$scope.rankingStar5 = result.totalRanking >= 5;
            });
            $scope.$apply();
        });
        // #endregion
        
        $scope.buildOwnerPictureUrl = function () {
			return $scope.ownerAvatarUrl;
        };
        
        var photos = [
            { slideThumbnail: baseUrl + '/cards/' + $stateParams.cardId + '/front' },
            { slideThumbnail: baseUrl + '/cards/' + $stateParams.cardId + '/back' },
            { slideThumbnail: baseUrl + '/cards/' + $stateParams.cardId + '/side' }
        ];

        $scope.cardsSlide = photos;

        $scope.slideHasChanged = function (index) {
            cardService.readCard(index, function (result) {
                $scope.card = result;
            });
        };

        $scope.previousSlide = function (index)
        {
            cardService.readCard(index - 1, function (result) {
                $scope.card = result;
            });
        }

        $scope.nextSlide = function (index) {
            cardService.readCard(index + 1, function (result) {
                $scope.card = result;
            });
        }
        
        $scope.rentClicked = function() {
        	$scope.$root.cart.push($scope.card);
        	$state.go('app.renting');
        }

		$scope.nextSlide = function() {
			$ionicSlideBoxDelegate.next();
		}

		$scope.previousSlide = function() {
			$ionicSlideBoxDelegate.previous();
		}
		
/* 		alert(angular.element('.card_bg').height()); */
    }
}());