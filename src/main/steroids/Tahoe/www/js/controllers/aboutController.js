// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register controller
    angular.module('tahoeApp.controllers').controller("AboutCtrl", AboutCtrl);

    // 2. Inject dependencies
    AboutCtrl.inject = [];

    // 3. Define public interface and private implementation
    function AboutCtrl($scope) {

        // #region 3.1 Public Interface
        
        $scope.$root.hideMenuIcon = true;
        $scope.$root.hideBackIcon = false;

    }
}());