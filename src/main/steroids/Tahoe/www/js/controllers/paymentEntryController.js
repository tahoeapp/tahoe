﻿// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register controller
    angular.module('tahoeApp.controllers').controller("paymentEntryCtrl", paymentEntryCtrl);

    // 2. Inject dependencies
    paymentEntryCtrl.inject = [];

    // 3. Define public interface and private implementation
    function paymentEntryCtrl($scope, $stateParams, locationService, paypalService) {
        // #region 3.1 Public Interface
        
        $scope.country = "";
        
        $scope.pay = pay;
        
        $scope.rentalDetails = angular.fromJson($stateParams.rentalDetails);
        
        $scope.rentalCost = $scope.rentalDetails.total;
       
        // #endregion

        // #region  3.2. Private implementations
        
        // user: userId.id,
//         		items: rentalItems,
//         		cost: $scope.rentCost,
//         		fee: $scope.tahoeFee,
//         		total: $scope.totalCost,
//         		authAmount: $scope.authorizedAmount,
//         		startDate: this.startDate,
//         		endDate: this.endDate,
//         		note: this.note
        
        function pay() {
        	var last4 = this.cardNumber.toString().substr(12, 15);
        	var cardDetails = {
				payer: $scope.rentalDetails.userEmail,
				type: this.cardType.toLowerCase(), 
				number: this.cardNumber,
				last4: last4,
				firstName: this.firstName,
				lastName: this.lastName,
				expirationMonth: this.expireMonth,
				expirationYear: this.expireYear,
				cvv: this.cvvCode,
				line1: this.addressLine1,
				line2: this.addressLine2,
				city: this.city,
				state: this.state,
				postalCode: this.postalCode,
				country: this.country
			};
        	locationService.get2LetterCountryCode(this.country, function(countryCode) {
        		paypalService.getPaypalAuth(function (auth) {
        			cardDetails.country = countryCode;
					paypalService.saveCardDetails(auth, cardDetails, function (creditCardId) {
						var rentalDetails = $scope.rentalDetails;
						rentalDetails.creditCardId = creditCardId;
						rentalDetails.creditCard = cardDetails;
						paypalService.authPayment(auth, rentalDetails, function (result) {
							console.log(result);
						});
					});
				});
        		//Make the paypal call to save the payment info
        		//Make the payment auth
        		//Save to our server
        	});
        };
        
        // #endregion
    }
}());