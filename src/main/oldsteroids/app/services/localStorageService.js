// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register service
    angular.module('tahoeApp.services').factory("localStorageService", localStorageService);

    // 2. Inject dependencies
    localStorageService.inject = [];

    // 3. Define public interface and private implementation
    function localStorageService() {

        // #region 3.1 Public Interface

        // #endregion

        // #region  3.2. Private implementations
        var clearLocalStorage = function () {
            window.localStorage.clear();
        };

        var saveLoginInfo = function (loginInfo) {
            window.localStorage['loginInfo.id'] = loginInfo.id;
            window.localStorage['loginInfo.email'] = loginInfo.email;
            window.localStorage['loginInfo.password'] = loginInfo.password;
        };

        var readLoginInfo = function () {
            return {
                id: window.localStorage['loginInfo.id'],
                email: window.localStorage['loginInfo.email'],
                password: window.localStorage['loginInfo.password']
            }
        };

        var service = {
            clearLocalStorage: clearLocalStorage,

            saveLoginInfo: saveLoginInfo,

            readLoginInfo: readLoginInfo
        }

        return service;
        // #endregion
    }
}());