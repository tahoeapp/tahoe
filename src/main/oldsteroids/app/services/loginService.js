// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register service
    angular.module('tahoeApp.services').factory("loginService", loginService);

    // 2. Inject dependencies
    loginService.inject = [];

    // 3. Define public interface and private implementation
    function loginService(Restangular, basicAuthService) {

        // #region 3.1 Public Interface

        // #endregion

        // #region  3.2. Private implementations
        var validateLogin = function (loginInfo, callback) {
            var authHeaders = basicAuthService.createHeader(loginInfo);
            Restangular.all("me/session").post(loginInfo, {}, authHeaders).then(function (result) {
                if (typeof (callback) === 'function') {
                    callback(result);
                }
            });
        }

        var service = {
            validateLogin: validateLogin
        };

        return service;
        // #endregion
    }
}());