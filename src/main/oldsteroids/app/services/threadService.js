// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register service
    angular.module('tahoeApp.services').factory("threadService", threadService);

    // 2. Inject dependencies
    threadService.inject = [];

    // 3. Define public interface and private implementation
    function threadService(Restangular, basicAuthService, localStorageService) {

        // #region 3.1 Public Interface

        // #endregion

        // #region  3.2. Private implementations
        var createAuth = function () {
            var loginInfo = localStorageService.readLoginInfo();
            return basicAuthService.createHeader(loginInfo);
        };

        var readMyThreads = function (callback) {
            Restangular.all('me/threads').getList({}, createAuth()).then(function (result) {
                if (typeof (callback) === 'function') {
                    callback(result);
                }
            });
        };

        var readMyThread = function (threadId, callback) {
            Restangular.one("me/threads", threadId).get({}, createAuth()).then(function (result) {
                if (typeof (callback) === 'function') {
                    callback(result);
                }
            });
        };

        var service = {
            readMyThreads: readMyThreads,
            readMyThread: readMyThread
        };

        return service;
        // #endregion
    }
}());