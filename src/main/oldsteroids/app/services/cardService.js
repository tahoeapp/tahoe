// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register service
    angular.module('tahoeApp.services').factory("cardService", cardService);

    // 2. Inject dependencies
    cardService.inject = [];

    // 3. Define public interface and private implementation
    function cardService(Restangular) {

        // #region 3.1 Public Interface
       
        // #endregion

        // #region  3.2. Private implementations
        // TODO comment out for fake data
        var readCard = function (cardId, callback) {
            Restangular.one("cards", cardId).get().then(function (result) {
                if (typeof (callback) === 'function') {
                    callback(result);
                }
            });
        };
        var readNearbyCards = function (page, callback) {
        	return Restangular.all('cards/nearby').getList({"page":page}).then(function (result) {
        		if (typeof (callback) === 'function') {
        			callback(result);
        		}
        	});
        };

        var service = {
            readCard: readCard,
            readNearbyCards: readNearbyCards
        }

        return service;
    }
    // #endregion
}());