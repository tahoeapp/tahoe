﻿// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register controller
    angular.module('tahoeApp.controllers').controller("MyProfileCtrl", MyProfileCtrl);

    // 2. Inject dependencies
    MyProfileCtrl.inject = [];

    // 3. Define public interface and private implementation
    function MyProfileCtrl($scope, localStorageService, myProfileService, appHomeUrl) {

        // #region 3.1 Public Interface
        $scope.logOut = logOut;
        // #endregion

        // TODO: Get profile from server
        var profile = [
          {
              fullName: "Jannie",
              state: "California",
              country: "USA",
              avatar: "../../images/ava1.jpg",
              profileImage: "../../images/img-profile-edit.jpg"
          }
        ];

        $scope.myInfo = profile[0];

        // TODO: Get product list from server
        var cardList = [
        {
            boardImage: "../../images/product1.jpg", 
            name: "2012 K2 Apache Skis",
            price: "$ 12/day | $ 85/week",
            size: "11",
            like:"14", 
            view:"234", 
            bootSize:"11"},
        {
            boardImage: "../../images/product1.jpg", 
            name: "2012 K2 Apache Skis",
            price: "$ 12/day | $ 85/week",
            size: "11",
            like:"14", 
            view:"234", 
            bootSize:"11"
        }
        ];

        $scope.cards = cardList;

        // #region  3.2. Private implementations
        // TODO: Comment out for fake data
        //myProfileService.readMyCards(function (result) {
        //    $scope.cards = result;
        //});

        //myProfileService.readMyInfo(function (result) {
        //    $scope.myInfo = result;
        //});

        function logOut() {
            localStorageService.clearLocalStorage();
            document.location.replace(appHomeUrl);
        }
        // #endregion
    }
}());