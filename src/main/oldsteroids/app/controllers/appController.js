﻿// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register controller
    angular.module('tahoeApp.controllers').controller("AppCtrl", AppCtrl);

    // 2. Inject dependencies
    AppCtrl.inject = [];

    // 3. Define public interface and private implementation
    function AppCtrl($scope, localStorageService, loginService, appHomeUrl) {

        // #region 3.1 Public Interface
        $scope.isLoggedIn = false;
        $scope.isSearching = false;
        
        // Hide menu icon in the header
        $scope.$root.hideMenuIcon = false;

        var storedLoginInfo = localStorageService.readLoginInfo();
        if (storedLoginInfo.email && storedLoginInfo.password) {
            loginService.validateLogin(storedLoginInfo, function () {
                $scope.isLoggedIn = true;
            });
        }

        $scope.logOut = logOut;
        $scope.toogleSearching = toogleSearching;
        // #endregion

        // #region #region  3.2. Private implementations
        function logOut() {
            localStorageService.clearLocalStorage();
            document.location.replace(appHomeUrl);
        };

        function toogleSearching() {
            $scope.isSearching = !$scope.isSearching;
        }
        // #endregion
    }
}());