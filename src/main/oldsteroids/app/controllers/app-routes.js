'use-strict';

var app = angular.module('tahoeApp');

app.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider

        .state('app', {
            url: "/app",
            abstract: true,
            templateUrl: "menu.html",
            controller: 'AppCtrl'
        })

        .state('app.home', {
            url: "/home",
            views: {
                'menuContent': {
                    templateUrl: "home.html",
                    controller: 'HomeCtrl'
                }
            }
        })

        .state('app.card_details', {
            url: "/card_details/:cardId",
            views: {
                'menuContent': {
                    templateUrl: "card_details.html",
                    controller: 'CardDetailsCtrl'
                }
            }
        })

        .state('app.my_profile', {
            url: "/my_profile",
            views: {
                'menuContent': {
                    templateUrl: "my_profile.html",
                    controller: 'MyProfileCtrl'
                }
            }
        })

        .state('app.add_card', {
            url: "/add_card",
            views: {
                'menuContent': {
                    templateUrl: "add_card.html",
                    controller: 'AddCardCtrl'
                }
            }
        })

        .state('app.sign_up', {
            url: "/sign_up",
            views: {
                'menuContent': {
                    templateUrl: "sign_up.html",
                    controller: 'SignUpCtrl'
                }
            }
        })

        .state('app.login', {
            url: "/login",
            views: {
                'menuContent': {
                    templateUrl: "login.html",
                    controller: 'LoginCtrl'
                }
            }
        })

        .state('app.search', {
            url: "/search",
            views: {
                'menuContent': {
                    templateUrl: "search.html"
                }
            }
        })

        .state('app.renting', {
            url: "/renting",
            views: {
                'menuContent': {
                    templateUrl: "renting.html"
                }
            }
        })

        .state('app.threads', {
            url: "/threads",
            views: {
                'menuContent': {
                    templateUrl: "threads.html",
                    controller: 'ThreadsCtrl'

                }
            }
        })

        .state('app.thread', {
            url: "/threads/:id",
            views: {
                'menuContent': {
                    templateUrl: "thread.html",
                    controller: 'ThreadCtrl'

                }
            }
        })

        .state('app.offer_list', {
            url: "/offer_list",
            views: {
                'menuContent': {
                    templateUrl: "offer_list.html"
                }
            }
        })
        .state('app.payment_history_list', {
            url: "/payment_history_list",
            views: {
                'menuContent': {
                    templateUrl: "payment_history_list.html",
                    controller: 'paymentHistoryListCtrl'
                }
            }
        })
        .state('app.payment_history_details', {
            url: "/payment_history_details/:id",
            views: {
                'menuContent': {
                    templateUrl: "payment_history_details.html",
                    controller: 'paymentHistoryDetailsCtrl'
                }
            }
        })
        .state('app.rent_offer_sent', {
            url: "/rent_offer_sent",
            views: {
                'menuContent': {
                    templateUrl: "rent_offer_sent.html"
                }
            }
        })
		.state('app.profile_edit', {
            url: "/profile_edit",
            views: {
                'menuContent': {
                    templateUrl: "profile_edit.html"
                }
            }
        })
                .state('app.setting', {
            url: "/setting",
            views: {
                'menuContent': {
                    templateUrl: "setting.html"
                }
            }
        })
                .state('app.swap_offer_new', {
            url: "/swap_offer_new",
            views: {
                'menuContent': {
                    templateUrl: "swap_offer_new.html"
                }
            }
        })
        .state('app.swap_offer_sent', {
            url: "/swap_offer_sent",
            views: {
                'menuContent': {
                    templateUrl: "swap_offer_sent.html"
                }
            }
        })
        .state('app.swapping', {
            url: "/swapping",
            views: {
                'menuContent': {
                    templateUrl: "swapping.html"
                }
            }
        })
        .state('app.message_list', {
            url: "/message_list",
            views: {
                'menuContent': {
                    templateUrl: "message_list.html"
                }
            }
        })
        .state('app.message', {
            url: "/message",
            views: {
                'menuContent': {
                    templateUrl: "message.html"
                }
            }
        })
        .state('app.counter_received', {
            url: "/counter_received",
            views: {
                'menuContent': {
                    templateUrl: "counter_received.html"
                }
            }
        })
        .state('app.add_item', {
            url: "/add_item",
            views: {
                'menuContent': {
                    templateUrl: "add_item.html"
                }
            }
        })
        .state('app.about', {
            url: "/about",
            views: {
                'menuContent': {
                    templateUrl: "about.html"
                }
            }
        });

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/home');
});
