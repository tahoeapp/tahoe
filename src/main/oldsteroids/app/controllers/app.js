'use-strict';

angular.module('tahoeApp', ['ionic', 'angularMoment', 'tahoeApp.controllers'])

    .run(function ($ionicPlatform) {
        $ionicPlatform.ready(function () {
            if (window.StatusBar) {
                StatusBar.styleDefault();
            }
        });
    })

//    .constant('baseUrl', 'http://localhost:8080/rest')

    // uncomment for production use
    //.constant('baseUrl', 'http://162.243.145.237:8080/rest')
    .constant('baseUrl', 'http://104.236.139.183:8080/rest')

//    .constant('appHomeUrl', 'http://localhost:4567/views/app/index.html')

    // uncomment for device deployments
    .constant('appHomeUrl', 'http://localhost/views/app/index.html')

    .config(function (RestangularProvider, baseUrl) {
        RestangularProvider.setDefaultHeaders({ "Content-Type": "application/json" });
        RestangularProvider.setBaseUrl(baseUrl);
    });

