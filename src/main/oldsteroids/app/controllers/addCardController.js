﻿// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register controller
    angular.module('tahoeApp.controllers').controller("AddCardCtrl", AddCardCtrl);

    // 2. Inject dependencies
    AddCardCtrl.inject = [];

    // 3. Define public interface and private implementation
    function AddCardCtrl($scope, localStorageService) {

        // #region 3.1 Public Interface
        $scope.getPhoto = getPhoto;
        // #endregion

        // #region  3.2. Private implementations
        function getPhoto() {
            navigator.camera.getPicture(onSuccess, onFail, {
                quality: 20,
                destinationType: Camera.DestinationType.DATA_URL
            });
        };

        function onSuccess(imageData) {
            $scope.imageData = "data:image/jpeg;base64," + imageData;
        }

        function onFail(message) {
            alert('Failed because: ' + message);
        }
        // #endregion
    }
}());