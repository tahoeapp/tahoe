﻿// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register controller
    angular.module('tahoeApp.controllers').controller("SignUpCtrl", SignUpCtrl);

    // 2. Inject dependencies
    SignUpCtrl.inject = [];

    // 3. Define public interface and private implementation
    function SignUpCtrl($scope, signUpService, localStorageService, appHomeUrl) {

        // #region 3.1 Public Interface
        $scope.model = {};

        $scope.signUp = signUp;
        // #endregion

        // #region  3.2. Private implementations
        function signUp() {
           
            var m = $scope.model;
            if (!(m.fullName && m.email && m.password)) {
                console.log("error in details");
                return;
            }

            signUpService.signUp($scope.model, function (result) {
                debugger;
                if (result) {
                    debugger;
                    localStorageService.saveLoginInfo({
                       
                        id: result.id,
                        email: m.email,
                        password: m.password
                    });
                    document.location.replace(appHomeUrl);
                }
            });
        };
        // #endregion
    }
}());