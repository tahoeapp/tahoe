﻿// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register controller
    angular.module('tahoeApp.controllers').controller("LoginCtrl", LoginCtrl);

    // 2. Inject dependencies
    LoginCtrl.inject = [];

    // 3. Define public interface and private implementation
    function LoginCtrl($scope, loginService, localStorageService, appHomeUrl) {

        // #region 3.1 Public Interface
        $scope.loginInfo = {
            id: '',
            email: '',
            password: ''
        };

        $scope.login = login;
        // #endregion

        // #region  3.2. Private implementations
        function login() {
            loginService.validateLogin($scope.loginInfo, function (result) {
                $scope.loginInfo.id = result.id; // add the id that was retrieved from the server
                localStorageService.saveLoginInfo($scope.loginInfo);
                document.location.replace(appHomeUrl);
            });
        }
        // #endregion
    }
}());