// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register service
    angular.module('tahoeApp.services').factory("basicAuthService", basicAuthService);

    // 2. Inject dependencies
    basicAuthService.inject = [];

    // 3. Define public interface and private implementation
    function basicAuthService(base64EncodeService) {

        // #region 3.1 Public Interface
        var keyStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';

        // #endregion

        // #region  3.2. Private implementations
        var base64Encode = function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;

            do {
                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);

                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;

                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }

                output = output +
                keyStr.charAt(enc1) +
                keyStr.charAt(enc2) +
                keyStr.charAt(enc3) +
                keyStr.charAt(enc4);
                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";
            } while (i < input.length);

            return output;
        };

        var createHeader = function (loginInfo) {
            return { 'Authorization': 'Basic ' + base64EncodeService.base64Encode(loginInfo.email + ':' + loginInfo.password) };
        }

        var service = {
            createHeader: createHeader
        }

        return service;
        // #endregion
    }
}());