// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register service
    angular.module('tahoeApp.services').factory("validationService", validationService);

    // 2. Inject dependencies
    validationService.inject = [];

    // 3. Define public interface and private implementation
    function validationService(Restangular) {

        // #region 3.1 Public Interface

        // #endregion

        // #region  3.2. Private implementations
        var validatePassword = function (password) {
           return password.match(/(?=.*\d)(?=.*[a-z]).{6,}/) && password.length <= 20;
        }
        
        var validateMakeAndModel = function (makeAndModel) {
        	return makeAndModel.match(/.+,.+/);
        }

        var service = {
            validatePassword: validatePassword,
            validateMakeAndModel: validateMakeAndModel
        };

        return service;
        // #endregion
    }
}());