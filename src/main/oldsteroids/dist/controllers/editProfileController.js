﻿// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register controller
    angular.module('tahoeApp.controllers').controller("EditProfileCtrl", EditProfileCtrl);

    // 2. Inject dependencies
    EditProfileCtrl.inject = [];

    // 3. Define public interface and private implementation
    function EditProfileCtrl($scope, $state, $window, $ionicPopup, $ionicActionSheet, signUpService, localStorageService, myProfileService, locationService, validationService, baseUrl) {

        // #region 3.1 Public Interface
        
        $scope.$root.hideMenuIcon = true;
        $scope.$root.hideBackIcon = false;
        
        $scope.model = {};
        $scope.address = {};
        $scope.confirmation = {};
        
        $scope.updatedAvatar = false;
        
        $scope.autocompleteOptions = {types: ['(cities)']};
        
        $scope.passwordError = true;
        $scope.confirmPasswordError = true;

        $scope.update = update;
        $scope.getAvatarPhoto = getAvatarPhoto;
        
        myProfileService.readMyInfo(function (result) {
        	$scope.userId = result.id;
            $scope.model.firstName = result.firstName;
            $scope.model.lastName = result.lastName;
            $scope.model.email = result.email;
            $scope.model.latitude = result.latitude;
            $scope.model.longitude = result.longitude;
            $scope.avatarImage = baseUrl + "/users/" + result.id + "/avatar";
            locationService.getCityByLatLong(result.latitude, result.longitude, function(result) {
            	if (result != "") {
            		$scope.address = result;
            	}
            });
        });
        
        // #endregion

        // #region  3.2. Private implementations
        function update() {
           
            var model = $scope.model;
            var confirmation = $scope.confirmation;
            
            var firstNameField = angular.element(document.getElementById("first-name-field"));
            var lastNameField = angular.element(document.getElementById("last-name-field"));
            var locationField = angular.element(document.getElementById("location-field"));
            var passwordField = angular.element(document.getElementById("password-field"));
            var confirmPasswordField = angular.element(document.getElementById("confirm-password-field"));
        	var isValid = true;
        	
        	if (!model.firstName) {
        		firstNameField.addClass("red-border");
        		isValid = false;
        	}
        	else {
        		firstNameField.removeClass("red-border");
        	}
        	
        	if (!model.lastName) {
        		lastNameField.addClass("red-border");
        		isValid = false;
        	}
        	else {
        		lastNameField.removeClass("red-border");
        	}
        	
        	if (!this.address) {
        		locationField.addClass("red-border");
        		isValid = false;
        	}
        	else {
        		locationField.removeClass("red-border");
        	}
        	
        	if (!model.password && !confirmation.password) {	
				passwordField.removeClass("red-border");
				confirmPasswordField.removeClass("red-border");
				$scope.passwordError = true;
				$scope.confirmPasswordError = true;
        	}
        	else {
				if (!model.password && confirmation.password) {
					passwordField.addClass("red-border");
					isValid = false;
				}
				else {
					passwordField.removeClass("red-border");
					$scope.passwordError = true;
				}
			
				if (model.password && !confirmation.password) {
					confirmPasswordField.addClass("red-border");
					isValid = false;
				}
				else {
					confirmPasswordField.removeClass("red-border");
				}
				if (model.password && confirmation.password) {
					if (!validationService.validatePassword(model.password)) {
						passwordField.addClass("red-border");
        				$scope.passwordError = false;
        				isValid = false;
					}
					else if (model.password !== confirmation.password) {	
						passwordField.addClass("red-border");
						confirmPasswordField.addClass("red-border");
						$scope.confirmPasswordError = false;
						isValid = false;
					}
					else {	
						passwordField.removeClass("red-border");
						confirmPasswordField.removeClass("red-border");
						$scope.passwordError = true;
						$scope.confirmPasswordError = true;
					}
				}
			}
			
			if (isValid) {
				if (this.address.geometry) {
					model.latitude = this.address.geometry.location.k;
					model.longitude = this.address.geometry.location.D;
            	}
				myProfileService.updateMyInfo(model, function (result) {
					if (result) {
						if (result.errorCode) {
							//Handle error
							var alertPopup = $ionicPopup.alert({
								template: result.errorMessage
							});
							alertPopup.then();
							return;
						}
						var storedLoginInfo = localStorageService.readLoginInfo();
						storedLoginInfo.password = model.password;
						localStorageService.saveLoginInfo(storedLoginInfo);
						localStorageService.saveUserNameInfo(model);
						if ($scope.updatedAvatar) {
							signUpService.uploadAvatar(result.id, $scope.avatarData, function (result) {
								if (result) {
									if (result.responseCode == 200) {
										$state.go('app.my_profile');
									}
								}
							});
						}
						else {
							$state.go('app.my_profile');
						}
					}
				});
			}
        };
        
        function getAvatarPhoto() {
        	$ionicActionSheet.show({buttons: [{text: 'Photo Library'},
        									  {text: 'Take Photo'}],
        									  cancelText: 'Cancel',
        									  buttonClicked: function(index) {
					navigator.camera.getPicture(onSuccess, onFail, {
						quality: 20,
						sourceType: index,
						destinationType: Camera.DestinationType.DATA_URL
					});
					return true;
				}
			});
        };

        function onSuccess(imageData) {
        	$scope.updatedAvatar = true;
            $scope.avatarImage = "data:image/jpeg;base64," + imageData;
            $scope.avatarData = imageData;
            $scope.$apply();
        }

        function onFail(message) {
        }
        // #endregion
    }
}());