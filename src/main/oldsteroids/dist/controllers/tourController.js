﻿// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register controller
    angular.module('tahoeApp.controllers').controller("TourCtrl", TourCtrl);

    // 2. Inject dependencies
    TourCtrl.inject = [];

    // 3. Define public interface and private implementation
    function TourCtrl($scope, $state, $ionicSideMenuDelegate) {

        // #region 3.1 Public Interface
        
        $ionicSideMenuDelegate.canDragContent(false);

		//Changes to frameheight needed
		//iPhone 5s + 1, iPhone 6 -18 iPhone 6+ -18
		var innerHeight = window.innerHeight;
		var innerWidth = window.innerWidth;
		if (innerHeight == 568) { //iPhone 5s
			innerHeight = innerHeight - 18;
		}
		else if (innerHeight == 667) { //iPhone 6
			innerHeight = innerHeight - 18;
		}
		else if (innerHeight == 736) { //iPhone 6+
			innerHeight = innerHeight - 18;
		}
		
		//Calculate the button positions
		var buttonHeight = innerHeight / 13.62;
		$scope.bottomButtonTop = innerHeight - buttonHeight;
		
		$scope.middleButtonTop = innerHeight / 1.705;
		$scope.middleButtonBottom = $scope.middleButtonTop + buttonHeight;
		
		var middleButtonWidth = innerWidth / 1.205;
		$scope.middleButtonLeft = innerWidth / 13.39;
		$scope.middleButtonRight = $scope.middleButtonLeft + middleButtonWidth;

        $scope.frameheight = innerHeight;
        $scope.framewidth = innerWidth;
		
		$scope.endTour = endTour;
        // #endregion
		
        function endTour() {
        	$ionicSideMenuDelegate.canDragContent(true);
        	$state.go('app.my_profile');
        }
    }
}());