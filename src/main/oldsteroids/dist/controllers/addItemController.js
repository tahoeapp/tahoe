﻿// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register controller
    angular.module('tahoeApp.controllers').controller("AddItemCtrl", AddItemCtrl);

    // 2. Inject dependencies
    AddItemCtrl.inject = [];

    // 3. Define public interface and private implementation
    function AddItemCtrl($scope, $state, $stateParams, $ionicActionSheet, $ionicPopup, cardService, localStorageService, locationService, validationService, baseUrl) {

        // #region 3.1 Public Interface
        
        angular.element('select').change(function() {
        	angular.element(this).css('color','black');
        });
        
        $scope.$root.hideMenuIcon = true;
        $scope.$root.hideBackIcon = false;
        
        $scope.isEditing = false;
        
        $scope.card = {
        	swappable: false
        };
        $scope.address = {};
        
        $scope.getFrontPhoto = getFrontPhoto;
        $scope.getBackPhoto = getBackPhoto;
        $scope.getSidePhoto = getSidePhoto;
        $scope.submitCard = submitCard;
        $scope.saveCard = saveCard;
        $scope.itemChanged = itemChanged;
        $scope.typeChanged = typeChanged;

        $scope.hideFront = true;
        $scope.hideBack = true;
        $scope.hideSide = true;
        
        $scope.hideType = false;
        $scope.hideLitreSize = false;
        $scope.hideSqMetersSize = true;
        $scope.hideCMSize = true;
        $scope.hideWidthHeightSize = true;
        
        $scope.disableInput = true;
        
        $scope.types = [];
        
        $scope.years = [];
        
        $scope.selectedFrontPhoto = false;
        $scope.selectedBackPhoto = false;
        $scope.selectedSidePhoto = false;
        
        $scope.imageError = true;
        $scope.makeModelError = true;
        
        $scope.autocompleteOptions = {types: ['(cities)']};
        
        var year = new Date().getFullYear();
        for (var i = 0; i < 11; i++) {
        	$scope.years.push(year - i);
        }
        
        if ($stateParams.cardId) {
        	//If we are in editing mode then show the correct button and get the data
        	$scope.isEditing = true;
        	$scope.hideFront = false;
        	$scope.hideBack = false;
        	$scope.hideSide = false;
        	$scope.frontImage = baseUrl + '/cards/' + $stateParams.cardId + '/front';
            $scope.backImage = baseUrl + '/cards/' + $stateParams.cardId + '/back';
        	$scope.sideImage = baseUrl + '/cards/' + $stateParams.cardId + '/side';
        	cardService.readCard($stateParams.cardId, function (result) {
            	$scope.card = result;
            	$scope.card.year = parseInt(result.year);
            	itemChanged();
            	typeChanged();
            	locationService.getCityByLatLong(result.latitude, result.longitude, function(result) {
					if (result != "") {
						$scope.address = result;
					}
				});
        	});
        }
        
        // #endregion

        // #region  3.2. Private implementations
        function submitCard() {
			//Validate entries
			var card = $scope.card;
			
			var itemField = angular.element(document.getElementById("item-field"));
            var typeField = angular.element(document.getElementById("type-field"));
            var makeModelField = angular.element(document.getElementById("make-model-field"));
            var yearField = angular.element(document.getElementById("year-field"));
            var sizeLField = angular.element(document.getElementById("size-l-field"));
            var sizeM2Field = angular.element(document.getElementById("size-m2-field"));
            var sizeCMField = angular.element(document.getElementById("size-cm-field"));
            var widthField = angular.element(document.getElementById("width-field"));
            var heightField = angular.element(document.getElementById("height-field"));
            var locationField = angular.element(document.getElementById("location-field"));
            var descriptionField = angular.element(document.getElementById("descripition-field"));
            var defectField = angular.element(document.getElementById("defect-field"));
            var pricePerDayField = angular.element(document.getElementById("price-per-day-field"));
            var pricePerWeekField = angular.element(document.getElementById("price-per-week-field"));
            var authAmountField = angular.element(document.getElementById("auth-amount-field"));
        	var isValid = true;
        	
        	if (!card.item || card.item === "Item") {
        		itemField.addClass("red-border");
        		isValid = false;
        	}
        	else {
        		itemField.removeClass("red-border");
        	}
        	
        	if (!card.type || card.type === "Type") {
        		typeField.addClass("red-border");
        		isValid = false;
        	}
        	else {
        		typeField.removeClass("red-border");
        	}
        	
        	if (!card.makeAndModel) {
        		makeModelField.addClass("red-border");
        		$scope.makeModelError = true;
        		isValid = false;
        	}
			else if (!validationService.validateMakeAndModel(card.makeAndModel)) {
        		makeModelField.addClass("red-border");
        		$scope.makeModelError = false;
        		isValid = false;
			}
        	else {
        		makeModelField.removeClass("red-border");
        		$scope.makeModelError = true;
        	}
        	
        	if (!card.year || card.year === "Year") {
        		yearField.addClass("red-border");
        		isValid = false;
        	}
        	else {
        		yearField.removeClass("red-border");
        	}
        	
        	if (!card.size && card.type !== "Twin Tip") {
        		sizeLField.addClass("red-border");
        		sizeM2Field.addClass("red-border");
        		sizeCMField.addClass("red-border");
        		isValid = false;
        	}
        	else {
        		sizeLField.removeClass("red-border");
        		sizeM2Field.removeClass("red-border");
        		sizeCMField.removeClass("red-border");
        	}
        	
        	if (!card.width && card.type === "Twin Tip") {
        		widthField.addClass("red-border");
        		isValid = false;
        	}
        	else {
        		widthField.removeClass("red-border");
        	}
        	
        	if (!card.height && card.type === "Twin Tip") {
        		heightField.addClass("red-border");
        		isValid = false;
        	}
        	else {
        		heightField.removeClass("red-border");
        	}
        	
        	if (!card.description) {
        		descriptionField.addClass("red-border");
        		isValid = false;
        	}
        	else {
        		descriptionField.removeClass("red-border");
        	}
        	
        	if (!card.defects) {
        		defectField.addClass("red-border");
        		isValid = false;
        	}
        	else {
        		defectField.removeClass("red-border");
        	}
        	
        	if (!card.pricePerDay) {
        		pricePerDayField.addClass("red-border");
        		isValid = false;
        	}
        	else {
        		pricePerDayField.removeClass("red-border");
        	}
        	
        	if (!card.pricePerWeek) {
        		pricePerWeekField.addClass("red-border");
        		isValid = false;
        	}
        	else {
        		pricePerWeekField.removeClass("red-border");
        	}
        	
        	if (!card.authorizedAmount) {
        		authAmountField.addClass("red-border");
        		isValid = false;
        	}
        	else {
        		authAmountField.removeClass("red-border");
        	}
        	
        	if (!this.address.geometry) {
        		locationField.addClass("red-border");
        		isValid = false;
        	}
        	else {
        		locationField.removeClass("red-border");
        	}
			
			if (!$scope.selectedFrontPhoto || !$scope.selectedBackPhoto || !$scope.selectedSidePhoto) {
				$scope.imageError = false;
        		isValid = false;
			}
			else {
				$scope.imageError = true;
			}
			
			if (isValid) {
				//Obtain the users longitude and latitude
				card.latitude = this.address.geometry.location.k;
				card.longitude = this.address.geometry.location.D;
				cardService.addCard($scope.card, function (result) {
					if (result) {
						if (result.errorCode) {
						//Handle error
							var alertPopup = $ionicPopup.alert({
								template: result.errorMessage
							});
							alertPopup.then();
							return;
						}
						//Upload the pictures
						var cardId = result.cardId;
						cardService.uploadPictureToCard(cardId, "front", $scope.frontImageData, function (result) {
							if (result) {
								if (result.responseCode == 200) {
									cardService.uploadPictureToCard(cardId, "back", $scope.backImageData, function (result) {
										if (result) {
											if (result.responseCode == 200) {
												cardService.uploadPictureToCard(cardId, "side", $scope.sideImageData, function (result) {
													if (result.responseCode == 200) {
														$state.go('app.my_profile');
													}
												});
											}
										}
									});
								}
							}
						});
					}
				});
			}
        };
        
        function saveCard() {
			//Validate entries
			var card = $scope.card;
			
			var itemField = angular.element(document.getElementById("item-field"));
            var typeField = angular.element(document.getElementById("type-field"));
            var makeModelField = angular.element(document.getElementById("make-model-field"));
            var yearField = angular.element(document.getElementById("year-field"));
            var sizeLField = angular.element(document.getElementById("size-l-field"));
            var sizeM2Field = angular.element(document.getElementById("size-m2-field"));
            var sizeCMField = angular.element(document.getElementById("size-cm-field"));
            var widthField = angular.element(document.getElementById("width-field"));
            var heightField = angular.element(document.getElementById("height-field"));
            var locationField = angular.element(document.getElementById("location-field"));
            var descriptionField = angular.element(document.getElementById("descripition-field"));
            var defectField = angular.element(document.getElementById("defect-field"));
            var pricePerDayField = angular.element(document.getElementById("price-per-day-field"));
            var pricePerWeekField = angular.element(document.getElementById("price-per-week-field"));
            var authAmountField = angular.element(document.getElementById("auth-amount-field"));
        	var isValid = true;
        	
        	if (!card.item || card.item === "Item") {
        		itemField.addClass("red-border");
        		isValid = false;
        	}
        	else {
        		itemField.removeClass("red-border");
        	}
        	
        	if (!card.type || card.type === "Type") {
        		typeField.addClass("red-border");
        		isValid = false;
        	}
        	else {
        		typeField.removeClass("red-border");
        	}
        	
        	if (!card.makeAndModel) {
        		makeModelField.addClass("red-border");
        		$scope.makeModelError = true;
        		isValid = false;
        	}
			else if (!validationService.validateMakeAndModel(card.makeAndModel)) {
        		makeModelField.addClass("red-border");
        		$scope.makeModelError = false;
        		isValid = false;
			}
        	else {
        		makeModelField.removeClass("red-border");
        		$scope.makeModelError = true;
        	}
        	
        	if (!card.year || card.year === "Year") {
        		yearField.addClass("red-border");
        		isValid = false;
        	}
        	else {
        		yearField.removeClass("red-border");
        	}
        	
        	if (!card.size && card.type !== "Twin Tip") {
        		sizeLField.addClass("red-border");
        		sizeM2Field.addClass("red-border");
        		sizeCMField.addClass("red-border");
        		isValid = false;
        	}
        	else {
        		sizeLField.removeClass("red-border");
        		sizeM2Field.removeClass("red-border");
        		sizeCMField.removeClass("red-border");
        	}
        	
        	if (!card.width && card.type === "Twin Tip") {
        		widthField.addClass("red-border");
        		isValid = false;
        	}
        	else {
        		widthField.removeClass("red-border");
        	}
        	
        	if (!card.height && card.type === "Twin Tip") {
        		heightField.addClass("red-border");
        		isValid = false;
        	}
        	else {
        		heightField.removeClass("red-border");
        	}
        	
        	if (!card.description) {
        		descriptionField.addClass("red-border");
        		isValid = false;
        	}
        	else {
        		descriptionField.removeClass("red-border");
        	}
        	
        	if (!card.defects) {
        		defectField.addClass("red-border");
        		isValid = false;
        	}
        	else {
        		defectField.removeClass("red-border");
        	}
        	
        	if (!card.pricePerDay) {
        		pricePerDayField.addClass("red-border");
        		isValid = false;
        	}
        	else {
        		pricePerDayField.removeClass("red-border");
        	}
        	
        	if (!card.pricePerWeek) {
        		pricePerWeekField.addClass("red-border");
        		isValid = false;
        	}
        	else {
        		pricePerWeekField.removeClass("red-border");
        	}
        	
        	if (!card.authorizedAmount) {
        		authAmountField.addClass("red-border");
        		isValid = false;
        	}
        	else {
        		authAmountField.removeClass("red-border");
        	}
        	
        	if (!this.address.geometry) {
        		locationField.addClass("red-border");
        		isValid = false;
        	}
        	else {
        		locationField.removeClass("red-border");
        	}
			
			if (isValid) {
				card.year = "" + card.year
			
				var cardId = card.cardId;
				delete card.cardId;
				delete card.ownerId;
				delete card.averageOwnerRanking;
				delete card.likeCount;
				delete card.viewCount;
			
				cardService.updateCard(cardId, card, function (result) {
					if (result) {
						if (result.errorCode) {
						//Handle error
								var alertPopup = $ionicPopup.alert({
									template: result.errorMessage
								});
								alertPopup.then();
							return;
						}
						//Upload the pictures
						if ($scope.selectedFrontPhoto) {
							cardService.uploadPictureToCard(cardId, "front", $scope.frontImageData, function (result) {
								if (result) {
									if (result.responseCode == 200) {
										onUpdateFrontSuccess(cardId);
									}
								}
							});
						}
						else {
							onUpdateFrontSuccess(cardId);
						}
					}
				});
			}
        };
        
        function onUpdateFrontSuccess(cardId) {
        	if ($scope.selectedBackPhoto) {
				cardService.uploadPictureToCard(cardId, "back", $scope.backImageData, function (result) {
					if (result) {
						if (result.responseCode == 200) {
							onUpdateBackSuccess(cardId);
						}
					}
				});
			}
			else {
				onUpdateBackSuccess(cardId);
			}
        }
        
        function onUpdateBackSuccess(cardId) {
        	if ($scope.selectedSidePhoto) {
        		cardService.uploadPictureToCard(cardId, "side", $scope.sideImageData, function (result) {
					if (result.responseCode == 200) {
						$state.go('app.my_profile');
					}
				});
			}
			else {
				$state.go('app.my_profile');
			}
        }
        
        function getFrontPhoto() {
        	if (!$scope.disableInput) {
				$ionicActionSheet.show({buttons: [{text: 'Photo Library'},
												  {text: 'Take Photo'}],
												  cancelText: 'Cancel',
												  buttonClicked: function(index) {
						navigator.camera.getPicture(onFrontSuccess, onFail, {
							quality: 20,
							sourceType: index,
							destinationType: Camera.DestinationType.DATA_URL
						});
						return true;
					}
				});
            }
        };

        function onFrontSuccess(imageData) {
        	$scope.selectedFrontPhoto = true;
        	$scope.frontImage = "data:image/jpeg;base64," + imageData;
        	$scope.frontImageData = imageData;
            $scope.hideFront = false;
            $scope.$apply();
        };
        
        function getBackPhoto() {
            if (!$scope.disableInput) {
            	$ionicActionSheet.show({buttons: [{text: 'Photo Library'},
												  {text: 'Take Photo'}],
												  cancelText: 'Cancel',
												  buttonClicked: function(index) {
						navigator.camera.getPicture(onBackSuccess, onFail, {
							quality: 20,
							sourceType: index,
							destinationType: Camera.DestinationType.DATA_URL
						});
						return true;
					}
				});
			}
        };

        function onBackSuccess(imageData) {
       		$scope.selectedBackPhoto = true;
            $scope.backImage = "data:image/jpeg;base64," + imageData;
            $scope.backImageData = imageData;
            $scope.hideBack = false;
            $scope.$apply();
        }
        
        function getSidePhoto() {
            if (!$scope.disableInput) {
            	$ionicActionSheet.show({buttons: [{text: 'Photo Library'},
												  {text: 'Take Photo'}],
												  cancelText: 'Cancel',
												  buttonClicked: function(index) {
						navigator.camera.getPicture(onSideSuccess, onFail, {
							quality: 20,
							sourceType: index,
							destinationType: Camera.DestinationType.DATA_URL
						});
						return true;
					}
				});
			}
        };

        function onSideSuccess(imageData) {
        	$scope.selectedSidePhoto = true;
            $scope.sideImage = "data:image/jpeg;base64," + imageData;
            $scope.sideImageData = imageData;
            $scope.hideSide = false;
            $scope.$apply();
        }

        function onFail(message) {
        }
        
        function itemChanged() {
        	$scope.disableInput = false;
        	if ($scope.card.item === "Windsurf Board") {
        		$scope.hideType = false;
        		$scope.types = ["Beginner", "Intermediate", "Advanced"];				
				$scope.hideLitreSize = false;
				$scope.hideSqMetersSize = true;
				$scope.hideCMSize = true;
				$scope.hideWidthHeightSize = true;
        	}
        	else if ($scope.card.item === "Windsurf Sail") {
        		$scope.hideType = true;
        		$scope.types.length = 0;			
				$scope.hideLitreSize = true;
				$scope.hideSqMetersSize = false;
				$scope.hideCMSize = true;
				$scope.hideWidthHeightSize = true;
        	}
        	else if ($scope.card.item === "Kite Board") {
        		$scope.hideType = false;
        		$scope.types = ["Twin Tip", 
        						"Strapless Kite Surfboard",
							    "Straps Kite Surfboard",
							    "Skim Board"];			
				$scope.hideLitreSize = true;
				$scope.hideSqMetersSize = true;
				$scope.hideCMSize = false;
				$scope.hideWidthHeightSize = true;
        	}
        	else if ($scope.card.item === "Kite") {
        		$scope.hideType = false;
        		$scope.types = ["Bow/Delta kite", "C kite", "Hybrid kite"];			
				$scope.hideLitreSize = true;
				$scope.hideSqMetersSize = false;
				$scope.hideCMSize = true;
				$scope.hideWidthHeightSize = true;
        	}
        	else {
        		$scope.disableInput = true;
        		$scope.hideLitreSize = false;
				$scope.hideSqMetersSize = true;
				$scope.hideCMSize = true;
				$scope.hideWidthHeightSize = true;
        	}
        }
        
        function typeChanged() {
        	
        	if ($scope.card.item === "Windsurf Board") {
        		$scope.hideLitreSize = false;
				$scope.hideSqMetersSize = true;
				$scope.hideCMSize = true;
				$scope.hideWidthHeightSize = true;
        	}
        	else if ($scope.card.item === "Windsurf Sail") {
        		$scope.hideLitreSize = true;
				$scope.hideSqMetersSize = false;
				$scope.hideCMSize = true;
				$scope.hideWidthHeightSize = true;
        	}
        	else if ($scope.card.item === "Kite Board") {
        		$scope.hideLitreSize = true;
				$scope.hideSqMetersSize = true;
				if ($scope.card.type === "Twin Tip") {
					$scope.hideCMSize = true;
					$scope.hideWidthHeightSize = false;
				}
				else {
					$scope.hideCMSize = false;
					$scope.hideWidthHeightSize = true;
				}
        	}
        	else if ($scope.card.item === "Kite") {
        		$scope.hideLitreSize = true;
				$scope.hideSqMetersSize = false;
				$scope.hideCMSize = true;
				$scope.hideWidthHeightSize = true;
        	}
        	else {
        		$scope.hideLitreSize = false;
				$scope.hideSqMetersSize = true;
				$scope.hideCMSize = true;
				$scope.hideWidthHeightSize = true;
        	}
        }
        
        // #endregion
    }
}());