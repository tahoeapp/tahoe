﻿// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register controller
    angular.module('tahoeApp.controllers').controller("SignUpCtrl", SignUpCtrl);

    // 2. Inject dependencies
    SignUpCtrl.inject = [];

    // 3. Define public interface and private implementation
    function SignUpCtrl($scope, $ionicPopup, $state, $ionicActionSheet, $ionicSideMenuDelegate, signUpService, localStorageService, validationService, appHomeUrl) {

        // #region 3.1 Public Interface
        $ionicSideMenuDelegate.canDragContent(false);
        
        $scope.model = {};
        $scope.confirmation = {};
        
        $scope.selectedAvatar = false;
        
        $scope.hideAvatar = true;
        
        $scope.autocompleteOptions = {types: ['(cities)']};

        $scope.signUp = signUp;
        $scope.getAvatarPhoto = getAvatarPhoto;
        
        $scope.avatarError = true;
        $scope.passwordError = true;
        $scope.confirmPasswordError = true;
        $scope.tosError = true;
        
        // #endregion

        // #region  3.2. Private implementations
        function signUp() {
           
            var model = $scope.model;
            var confirmation = $scope.confirmation;
            
            var firstNameField = angular.element(document.getElementById("first-name-field"));
            var lastNameField = angular.element(document.getElementById("last-name-field"));
            var emailField = angular.element(document.getElementById("email-field"));
            var passwordField = angular.element(document.getElementById("password-field"));
            var confirmPasswordField = angular.element(document.getElementById("confirm-password-field"));
            var locationField = angular.element(document.getElementById("location-field"));
            var tosField = angular.element(document.getElementById("tos-field"));
        	var isValid = true;
        	
        	if (!model.firstName) {
        		firstNameField.addClass("red-border");
        		isValid = false;
        	}
        	else {
        		firstNameField.removeClass("red-border");
        	}
        	
        	if (!model.lastName) {
        		lastNameField.addClass("red-border");
        		isValid = false;
        	}
        	else {
        		lastNameField.removeClass("red-border");
        	}
        	
        	if (!model.email) {
        		emailField.addClass("red-border");
        		isValid = false;
        	}
        	else {
        		emailField.removeClass("red-border");
        	}
        	
        	if (!model.password || !validationService.validatePassword(model.password)) {
        		passwordField.addClass("red-border");
        		$scope.passwordError = false;
        		isValid = false;
        	}
        	else {
        		passwordField.removeClass("red-border");
        		$scope.passwordError = true;
        	}
        	
        	if (!confirmation.password) {
        		confirmPasswordField.addClass("red-border");
        		isValid = false;
        	}
        	else {
        		confirmPasswordField.removeClass("red-border");
        	}
        	
        	if (!this.address) {
        		locationField.addClass("red-border");
        		isValid = false;
        	}
        	else {
        		locationField.removeClass("red-border");
        	}
        	
        	if (!confirmation.agreeTOS) {
        		tosField.addClass("red-border");
        		$scope.tosError = false;
        		isValid = false;
        	}
        	else {
        		tosField.removeClass("red-border");
        		$scope.tosError = true;
        	}
        	
            if (!($scope.selectedAvatar)) {
				$scope.avatarError = false
			}
			else {
				$scope.avatarError = true;
			}
        	
        	if (isValid) {
				if (model.password !== confirmation.password) {
					passwordField.addClass("red-border");
					confirmPasswordField.addClass("red-border");
					$scope.confirmPasswordError = false;
					isValid = false;
				}
				else {
					passwordField.removeClass("red-border");
					confirmPasswordField.removeClass("red-border");
					$scope.confirmPasswordError = true;
				}
			}
            
            if (isValid) {
				model.latitude = this.address.geometry.location.k;
				model.longitude = this.address.geometry.location.D;
            
				signUpService.signUp($scope.model, function (result) {
					if (result) {
						if (result.errorCode) {
							//Handle error
							var alertPopup = $ionicPopup.alert({
								template: result.errorMessage
							});
							alertPopup.then();
							return;
						}
						localStorageService.saveLoginInfo({
							id: result.id,
							email: model.email,
							password: model.password
						});
						localStorageService.saveUserNameInfo(model);
						signUpService.uploadAvatar(result.id, $scope.avatarData, function (result) {
							if (result) {
								if (result.responseCode == 200) {
									$ionicSideMenuDelegate.canDragContent(true);
									$state.go('app.tour');
								}
							}
						});
					}
				});
			}
        };
        
        function getAvatarPhoto() {
        	$ionicActionSheet.show({buttons: [{text: 'Photo Library'},
        									  {text: 'Take Photo'}],
        									  cancelText: 'Cancel',
        									  buttonClicked: function(index) {
					navigator.camera.getPicture(onSuccess, onFail, {
						quality: 20,
						sourceType: index,
						destinationType: Camera.DestinationType.DATA_URL
					});
					return true;
				}
			});
        };

        function onSuccess(imageData) {
        	$scope.selectedAvatar = true;
            $scope.avatarImage = "data:image/jpeg;base64," + imageData;
            $scope.avatarData = imageData;
            $scope.hideAvatar = false;
            $scope.$apply();
        }

        function onFail(message) {
        }
        // #endregion
    }
}());