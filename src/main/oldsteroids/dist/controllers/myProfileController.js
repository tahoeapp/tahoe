// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register controller
    angular.module('tahoeApp.controllers').controller("MyProfileCtrl", MyProfileCtrl);

    // 2. Inject dependencies
    MyProfileCtrl.inject = [];

    // 3. Define public interface and private implementation
    function MyProfileCtrl($scope, $state, localStorageService, myProfileService, locationService, appHomeUrl, baseUrl) {

        angular.element('.col').on('click', function() {hprlnk(angular.element(this).attr('id'));});
        		
        // #region 3.1 Public Interface
        $scope.$root.hideMenuIcon = false;
        $scope.$root.hideBackIcon = true;
        
        $scope.buildProfilePictureUrl = buildProfilePictureUrl;
        $scope.buildOwnerPictureUrl = buildOwnerPictureUrl;
        $scope.buildBannerUrl = buildBannerUrl;
        $scope.logOut = logOut;
        $scope.hprlnk = hprlnk;
        
        $scope.noCards = true;
        // #endregion

        // #region  3.2. Private implementations
        myProfileService.readMyCards(function (result) {
            $scope.cards = result;
            $scope.noCards = result.length == 0;
            
            if (!$scope.noCards) {
            	for (var i = 0; i < $scope.cards.length; i++) {
            		$scope.cards[i].sizeDisplay = $scope.cards[i].size;
            		if ($scope.cards[i].type == "Twin Tip") {
            			$scope.cards[i].sizeDisplay = $scope.cards[i].width + " x " + $scope.cards[i].height;
            		}
            	}
            }
        });

        myProfileService.readMyInfo(function (result) {
            $scope.myInfo = result;
            locationService.getCityByLatLong(result.latitude, result.longitude, function(result) {
            	if (location != "") {
            		$scope.myInfo.location = result;
            	}
            });
        });

		function hprlnk(lnk) {
			$state.go('app.'+lnk);
		}

		function buildProfilePictureUrl(id) {
			if (id) {
            	return baseUrl + '/users/' + id + '/avatar';
            }
        };
        
        function buildOwnerPictureUrl(card) {
        	if (card.ownerId) {
            	return baseUrl + '/users/' + card.ownerId + '/avatar';
            }
        };

        function buildBannerUrl(card) {
        	if (card.cardId) {
            	return baseUrl + '/cards/' + card.cardId + '/banner';
            }
        };

        function logOut() {
            localStorageService.clearLocalStorage();
            document.location.replace(appHomeUrl);
        }
        // #endregion
    }
}());