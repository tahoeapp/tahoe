﻿// 0. IIFE (Immediately-invoked function expression)
(function () {
    'use strict';

    // 1. Register controller
    angular.module('tahoeApp.controllers').controller("AppCtrl", AppCtrl);

    // 2. Inject dependencies
    AppCtrl.inject = [];

    // 3. Define public interface and private implementation
    function AppCtrl($scope, $state, localStorageService, loginService, appHomeUrl, baseUrl) {

        // #region 3.1 Public Interface
        $scope.isLoggedIn = false;
        //$scope.isSearching = false;
        
        // Hide menu icon in the header
        $scope.$root.hideMenuIcon = false;

        var storedLoginInfo = localStorageService.readLoginInfo();
        if (storedLoginInfo.email && storedLoginInfo.password) {
            loginService.validateLogin(storedLoginInfo, function (result) {
            	if (result) {
            		if (!result.status) {
	            		$scope.isLoggedIn = true;
						$state.go('app.my_profile');
					}
				}
            });
        }

        $scope.logOut = logOut;
        $scope.getProfileName = getProfileName;
        $scope.buildProfilePictureUrl = buildProfilePictureUrl;
        //$scope.toogleSearching = toogleSearching;
        
        // #endregion

        // #region #region  3.2. Private implementations
        function logOut() {
            localStorageService.clearLocalStorage();
            //document.location.replace(appHomeUrl);
            $state.go('app.login');
        };
        
        function getProfileName() {
        	var storedUserName = localStorageService.readUserNameInfo();
        	return storedUserName.firstName + " " + storedUserName.lastName;
        }

		function buildProfilePictureUrl() {
			var storedLoginInfo = localStorageService.readLoginInfo();
			if (storedLoginInfo.id) {
            	return baseUrl + '/users/' + storedLoginInfo.id + '/avatar';
            }
        };

        //function toogleSearching() {
        //    $scope.isSearching = !$scope.isSearching;
        //}
        // #endregion
    }
}());