'use-strict';

var app = angular.module('tahoeApp');

app.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider

        .state('app', {
            url: "/app",
            abstract: true,
            templateUrl: "/views/app/menu.html",
            controller: 'AppCtrl'
        })

        .state('app.card_details', {
            url: "/card_details/:cardId",
            views: {
                'menuContent': {
                    templateUrl: "/views/app/card_details.html",
                    controller: 'CardDetailsCtrl'
                }
            }
        })

        .state('app.my_profile', {
            url: "/my_profile",
            views: {
                'menuContent': {
                    templateUrl: "/views/app/my_profile.html",
                    controller: 'MyProfileCtrl'
                }
            }
        })

        .state('app.sign_up', {
            url: "/sign_up",
            views: {
                'menuContent': {
                    templateUrl: "/views/app/sign_up.html",
                    controller: 'SignUpCtrl'
                }
            }
        })

        .state('app.login', {
            url: "/login",
            views: {
                'menuContent': {
                    templateUrl: "/views/app/login.html",
                    controller: 'LoginCtrl'
                }
            }
        })

		.state('app.edit_profile', {
            url: "/edit_profile",
            views: {
                'menuContent': {
                    templateUrl: "/views/app/edit_profile.html",
                    controller: 'EditProfileCtrl'
                }
            }
        })
         
        .state('app.add_item', {
            url: "/add_item",
            views: {
                'menuContent': {
                    templateUrl: "/views/app/add_item.html",
                    controller: 'AddItemCtrl'
                }
            }
        })
        
        .state('app.edit_item', {
            url: "/edit_item/:cardId",
            views: {
                'menuContent': {
                    templateUrl: "/views/app/add_item.html",
                    controller: 'AddItemCtrl'
                }
            }
        })
        
        .state('app.about', {
            url: "/about",
            views: {
                'menuContent': {
                    templateUrl: "/views/app/about.html",
                    controller: 'AboutCtrl'
                }
            }
        })
        
        .state('app.tour', {
            url: "/tour",
            views: {
                'menuContent': {
                    templateUrl: "/views/app/tour.html",
                    controller: 'TourCtrl'
                }
            }
        });

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/login');
});
