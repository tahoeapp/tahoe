# App development phases and relevant API docs

## Phase 1

_Graphics are in `<Dropbox home>/Graphics/Latest PNGs`_  
_HTML is in `<Dropbox home>/HTML pages`_

#### Screens needed to complete
* Home (Product.png, product.html)
* Menu (Menu.png)
* Card Details (Product-details.png, product_details.html)

#### Relevant APIs

GET /rest/cards/nearby?longitude=XXX&latitude=YYY

```javascript
[  
   {  
      "ownerId": "5466e7e03004c9111650e366",
      "averageOwnerRanking": 0,
      "pricePerDay": 12,
      "pricePerWeek": 60,
      "authorizedAmount": 400,
      "title": "Burton 360",
      "description": "Burton 360",
      "defects": "No defects",
      "longitude": 122.42,
      "latitude": 37.76,
      "cardId": "5466e7e03004c9111650e367",
      "likeCount": 0,
      "viewCount": 21
   },
   {  
      "ownerId": "5466e7e03004c9111650e368",
      "averageOwnerRanking": 0,
      "pricePerDay": 12,
      "pricePerWeek": 60,
      "authorizedAmount": 400,
      "title": "Burton 720",
      "description": "Burton 720",
      "defects": "No defects",
      "longitude": 122.4319,
      "latitude": 37.7514,
      "cardId": "5466e7e03004c9111650e369",
      "likeCount": 0,
      "viewCount": 21
   }
]
````

GET /rest/cards/{id}


```javascript
{  
   "ownerId": "5466e7e03004c9111650e366",
   "averageOwnerRanking": 0,
   "pricePerDay": 12,
   "pricePerWeek": 60,
   "authorizedAmount": 400,
   "title": "Burton 360",
   "description": "Burton 360",
   "defects": "No defects",
   "longitude": 122.42,
   "latitude": 37.76,
   "cardId": "5466e7e03004c9111650e367",
   "likeCount": 0,
   "viewCount": 22
}
```