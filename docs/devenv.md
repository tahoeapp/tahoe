## Development Environment Setup

#### Requirements

* Install [Java 1.7 JDK]
* Install [AppGyver Steroids]. This will also install Git.
* Install latest [MongoDB]
* Download the AppGyver scanner app to your Android or iOS
* (Optional) Install [Robomongo] to access MongoDB
* Your favorite IDE

#### How to run server and app

* Download the sources from BitBucket using `git clone https://bitbucket.org/tahoeapp/tahoe-web.git`
* From the root directory, run `$ gradlew clean build`
* Make sure MongoDB is running
* Create a database called `tahoe`
* If this is your first time, you would need to put demo data in the system. Note that DATE depends on when you built the server.
* First time run: `$ java -jar build/libs/tahoe-web-DATE.jar --spring.profiles.active=bootstrap,demo`
* Not first run: `$ java -jar build/libs/tahoe-web-DATE.jar`
* Configure the app to work against your local server by setting the BaseUrl in `app.js` with your machine IP
* Now, `$ cd src/main/steroids` and `$ steroids connect`
* Use the Scanner app to scan the QR code that appeared in your browser
* The app should now appear on your phone
* To update the app on your phone, hit enter in the Steroids console


[Java 1.7 JDK]: https://academy.appgyver.com/installwizard/steps#/home
[AppGyver Steroids]: https://academy.appgyver.com/installwizard/steps#/home
[MongoDB]: https://www.mongodb.org/downloads
[Robomongo]: http://robomongo.org/
