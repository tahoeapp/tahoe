## Prod configuration

* Create tahoe user and group
* MongoDB needs to be set up for authentication
* Java 1.7 needs to be installed
* Product must be uploaded
* Product to auto-launch by tahoe user

### Create tahoe user and group

* `sudo adduser tahoe`
* Password: xw4uDFn!7Q78
* Disable tahoe remote login by changing their shell in `/etc/passwd` to `/bin/false`

### MongoDB config

* Install MongoDB using `sudo apt-get install mongodb`
* Configure `/etc/mongodb.conf` to allow auth
* Connect using `mongo`, `use tahoe` and add a user:
```
db.createUser(
{ user: "dbadmin",
  pwd: "@hn67KJB9$5h",
  roles: [ { role: "dbOwner", db: "tahoe" } ]
}
)
```

### Java config

https://www.digitalocean.com/community/tutorials/how-to-install-java-on-ubuntu-with-apt-get

### Product upload

* SCP the file into ~ by running `scp filename.jar root@162.243.145.237:~/`

### Requires params

`java -Djava.security.egd=file:/dev/./urandom -jar tahoe-web-1.0.jar --spring.profiles.active=bootstrap,demo --spring.data.mongodb.host=localhost`
